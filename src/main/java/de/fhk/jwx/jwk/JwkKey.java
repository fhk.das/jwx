/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwk;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.fhk.jwx.JoseHeaders;
import de.fhk.jwx.jwa.JwaECTypes;

/**
 * this class represents the json web key defined by jose.
 *
 */
public final class JwkKey {
    public static final String KEY_TYPE = "kty";
    public static final String PUBLIC_KEY_USE = "use";
    public static final String KEY_OPERATIONS = "key_ops";
    public static final String KEY_ALGO = JoseHeaders.HEADER_ALGORITHM;
    public static final String KEY_ID = JoseHeaders.HEADER_KEY_ID;
    public static final String X509_URL = JoseHeaders.HEADER_X509_URL;
    public static final String X509_CHAIN = JoseHeaders.HEADER_X509_CHAIN;
    public static final String X509_THUMBPRINT = JoseHeaders.HEADER_X509_THUMBPRINT;
    public static final String X509_THUMBPRINT_SHA256 = JoseHeaders.HEADER_X509_THUMBPRINT_SHA256;

    public static final String KEY_TYPE_RSA = "RSA";
    public static final String RSA_MODULUS = "n";
    public static final String RSA_PUBLIC_EXP = "e";
    public static final String RSA_PRIVATE_EXP = "d";
    public static final String RSA_FIRST_PRIME_FACTOR = "p";
    public static final String RSA_SECOND_PRIME_FACTOR = "q";
    public static final String RSA_FIRST_PRIME_CRT = "dp";
    public static final String RSA_SECOND_PRIME_CRT = "dq";
    public static final String RSA_FIRST_CRT_COEFFICIENT = "qi";

    public static final String KEY_TYPE_OCTET = "oct";
    public static final String OCTET_KEY_VALUE = "k";

    public static final String KEY_TYPE_ELLIPTIC = "EC";
    public static final String EC_CURVE = "crv";
    public static final String EC_CURVE_P256 = JwaECTypes.P256.getName();
    public static final String EC_CURVE_P384 = JwaECTypes.P384.getName();
    public static final String EC_CURVE_P521 = JwaECTypes.P521.getName();
    public static final String EC_X_COORDINATE = "x";
    public static final String EC_Y_COORDINATE = "y";
    public static final String EC_PRIVATE_KEY = "d";

    public static final String PUBLIC_KEY_USE_SIGN = "sig";
    public static final String PUBLIC_KEY_USE_ENCRYPT = "enc";

    public static final String KEY_OPER_SIGN = "sign";
    public static final String KEY_OPER_VERIFY = "verify";
    public static final String KEY_OPER_ENCRYPT = "encrypt";
    public static final String KEY_OPER_DECRYPT = "decrypt";

    private ObjectMapper mapper = new ObjectMapper();
    private Map<String, Object> values = new LinkedHashMap<String, Object>();

    protected JwkKey() {
    }

    public JwkKey(Map<String, Object> values) {
        this.values.putAll(values);
    }

    public JwkKey(File jwk) {
        try {
            this.values = mapper.readValue(jwk,
                    new TypeReference<Map<String, Object>>() {
                    });
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public JwkKey(String jwk) {
        try {
            this.values = mapper.readValue(jwk,
                    new TypeReference<Map<String, Object>>() {
                    });
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    protected void setKeyType(String keyType) {
        this.values.put(KEY_TYPE, keyType);
    }

    public String getKeyType() {
        return String.valueOf(this.values.get(KEY_TYPE));
    }

    protected void setPublicKeyUse(String use) {
        this.values.put(PUBLIC_KEY_USE, use);
    }

    public String getPublicKeyUse() {
        return (String) this.values.get(PUBLIC_KEY_USE);
    }

    protected void setKeyOperation(List<String> keyOperation) {
        this.values.put(KEY_OPERATIONS, keyOperation);
    }

    @SuppressWarnings("unchecked")
    public List<String> getKeyOperation() {
        return (List<String>) this.values.get(KEY_OPERATIONS);
    }

    protected void setAlgorithm(String algorithm) {
        this.values.put(KEY_ALGO, algorithm);
    }

    public String getAlgorithm() {
        return (String) this.values.get(KEY_ALGO);
    }

    public void setKid(String kid) {
        this.values.put(KEY_ID, kid);
    }

    public String getKid() {
        return (String) this.values.get(KEY_ID);
    }

    protected void setX509Url(String x509Url) {
        this.values.put(X509_URL, x509Url);
    }

    public String getX509Url() {
        return (String) this.values.get(X509_URL);
    }

    protected void setX509Chain(List<String> x509Chain) {
        this.values.put(X509_CHAIN, x509Chain);
    }

    @SuppressWarnings("unchecked")
    public List<String> getX509Chain() {
        return (List<String>) this.values.get(X509_CHAIN);
    }

    protected void setX509Thumbprint(String x509Thumbprint) {
        this.values.put(X509_THUMBPRINT, x509Thumbprint);
    }

    public String getX509Thumbprint() {
        return (String) this.values.get(X509_THUMBPRINT);
    }

    protected void setX509ThumbprintSHA256(String x509Thumbprint) {
        this.values.put(X509_THUMBPRINT_SHA256, x509Thumbprint);
    }

    public String getX509ThumbprintSHA256() {
        return (String) this.values.get(X509_THUMBPRINT_SHA256);
    }

    protected JwkKey setProperty(String name, Object value) {
        this.values.put(name, value);
        return this;
    }

    public Object getProperty(String name) {
        return this.values.get(name);
    }

    public String getCurveName(){
    	return (String) this.values.get(EC_CURVE);
    }
    
    public String toJSON() {
        return this.toJSON(false);
    }

    public String toJSON(boolean prettyPrint) {
        try {
            if (prettyPrint)

                return mapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(this.values);

            else
                return mapper.writeValueAsString(this.values);
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    public int sizeOfProperties() {
        return values.size();
    }

    public Map<String, Object> getAllParameters() {
        return this.values;
    }

}

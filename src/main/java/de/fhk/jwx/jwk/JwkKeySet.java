/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwk;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * this class represents the json web key set defined by jose.
 *
 */
public final class JwkKeySet {

    public static final String KEYS_PROPERTY = "keys";
    private ObjectMapper mapper = new ObjectMapper();
    private List<JwkKey> keys = new ArrayList<JwkKey>();

    public JwkKeySet() {

    }

    @SuppressWarnings("unchecked")
    public JwkKeySet(File jwks) {
        Map<String, Object> wohleKeys;
        try {
            wohleKeys = mapper.readValue(jwks,
                    new TypeReference<Map<String, Object>>() {
                    });

            List<Map<String, Object>> listOfKeys = (List<Map<String, Object>>) wohleKeys
                    .get(KEYS_PROPERTY);
            for (Map<String, Object> key : listOfKeys) {
                this.keys.add(new JwkKey(key));
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    public JwkKeySet(String jwks) {
        Map<String, Object> wohleKeys;
        try {
            wohleKeys = mapper.readValue(jwks,
                    new TypeReference<Map<String, Object>>() {
                    });

            List<Map<String, Object>> listOfKeys = (List<Map<String, Object>>) wohleKeys
                    .get(KEYS_PROPERTY);
            for (Map<String, Object> key : listOfKeys) {
                this.keys.add(new JwkKey(key));
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public List<JwkKey> getKeys() {
        return keys;
    }

    public void setKeys(List<JwkKey> keys) {
        this.keys = keys;
    }

    /**
     * 
     * @return keyIdMap contains only keys which includes keyId-parameter
     */
    public Map<String, JwkKey> getKeyIdMap() {
        List<JwkKey> keys = getKeys();
        if (keys == null) {
            return Collections.emptyMap();
        }
        Map<String, JwkKey> map = new LinkedHashMap<String, JwkKey>();
        for (JwkKey key : this.keys) {
            String kid = key.getKid();
            if (kid != null) {
                map.put(kid, key);
            }
        }
        return map;
    }

    public JwkKey getKey(String kid) {
        List<JwkKey> keys = getKeys();
        if (keys == null) {
            return null;
        }
        for (JwkKey key : this.keys) {

            if (key.getKid().equals(kid)) {
                return key;
            }
        }
        return null;
    }

    public Map<String, List<JwkKey>> getKeyTypeMap() {
        return getKeyPropertyMap(JwkKey.KEY_TYPE);
    }

    public Map<String, List<JwkKey>> getKeyUseMap() {
        return getKeyPropertyMap(JwkKey.PUBLIC_KEY_USE);
    }

    private Map<String, List<JwkKey>> getKeyPropertyMap(String propertyName) {
        List<JwkKey> keys = getKeys();
        if (keys == null) {
            return Collections.emptyMap();
        }
        Map<String, List<JwkKey>> map = new LinkedHashMap<String, List<JwkKey>>();
        for (JwkKey key : keys) {
            String propValue = (String) key.getProperty(propertyName);
            if (propValue != null) {
                List<JwkKey> list = map.get(propValue);
                if (list == null) {
                    list = new LinkedList<JwkKey>();
                    map.put(propValue, list);
                }
                list.add(key);
            }
        }
        return map;
    }

    public Map<String, List<JwkKey>> getKeyOperationMap() {
        List<JwkKey> keys = getKeys();
        if (keys == null) {
            return Collections.emptyMap();
        }
        Map<String, List<JwkKey>> map = new LinkedHashMap<String, List<JwkKey>>();
        for (JwkKey key : keys) {
            List<String> ops = key.getKeyOperation();
            if (ops != null) {
                for (String op : ops) {
                    List<JwkKey> list = map.get(op);
                    if (list == null) {
                        list = new LinkedList<JwkKey>();
                        map.put(op, list);
                    }
                    list.add(key);
                }
            }
        }
        return map;
    }

    public List<JwkKey> getKeys(String keyType) {
        return getKeyTypeMap().get(keyType);
    }

    public List<JwkKey> getRsaKeys() {
        return getKeyTypeMap().get(JwkKey.KEY_TYPE_RSA);
    }

    public List<JwkKey> getEllipticKeys() {
        return getKeyTypeMap().get(JwkKey.KEY_TYPE_ELLIPTIC);
    }

    public List<JwkKey> getSecretKeys() {
        return getKeyTypeMap().get(JwkKey.KEY_TYPE_OCTET);
    }

    public String toJSON() {
        return this.toJSON(false);
    }

    public String toJSON(boolean prettyPrint) {
        try {
            Map<String, Object> keys = new LinkedHashMap<String, Object>();
            keys.put(KEYS_PROPERTY, this.keys);
            if (prettyPrint)

                return mapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(keys);

            else
                return mapper.writeValueAsString(keys);
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

}

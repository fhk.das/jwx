/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwk;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URI;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.SecretKey;

import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jwa.JwaECTypes;
import de.fhk.jwx.util.IOUtils;
import de.fhk.jwx.util.JoseUtils;
/**
 * this class generates JWK from JWK parameters.
 *
 */
public class JwkMaker {
    public static JwkKey generateJWKFromSecretString(String kid, boolean signer, boolean verfiyer, String alg, String secret){
        JwkKey jwk = new JwkKey();
        jwk.setKeyType(JwkKey.KEY_TYPE_OCTET);
        jwk.setKid(kid);
        List<String> keyOperation = new ArrayList<String>();
        if (signer) keyOperation.add(JwkKey.KEY_OPER_SIGN);
        if (verfiyer) keyOperation.add(JwkKey.KEY_OPER_VERIFY);
        if (keyOperation.size()>0) jwk.setKeyOperation(keyOperation);
        if (alg!=null) jwk.setAlgorithm(alg);
        if (secret != null) jwk.setProperty("k", JoseUtils.stringToBase64URL(secret));
        else throw new SecurityException("secret is null");
        return jwk;
    }
    public static JwkKey generateJWKFromBase64URLSecret(String kid, boolean signer, boolean verifier, String alg, String encodedSecret){
        JwkKey jwk = new JwkKey();
        jwk.setKeyType(JwkKey.KEY_TYPE_OCTET);
        if (kid != null) jwk.setKid(kid);
        List<String> keyOperation = new ArrayList<String>();
        if (signer) keyOperation.add(JwkKey.KEY_OPER_SIGN);
        if (verifier) keyOperation.add(JwkKey.KEY_OPER_VERIFY);
        if (keyOperation.size()>0) jwk.setKeyOperation(keyOperation);
        if (alg != null) jwk.setAlgorithm(alg);
        if (encodedSecret != null) jwk.setProperty("k", encodedSecret);
        else throw new SecurityException("encoded secret is null");
        return jwk;
    }
    public static JwkKey generateJWKFromByteSecret(String kid, boolean signer, boolean verifier, String alg, byte[] secret){
        JwkKey jwk = new JwkKey();
        jwk.setKeyType(JwkKey.KEY_TYPE_OCTET);
        if (kid != null) jwk.setKid(kid);
        List<String> keyOperation = new ArrayList<String>();
        if (signer) keyOperation.add(JwkKey.KEY_OPER_SIGN);
        if (verifier) keyOperation.add(JwkKey.KEY_OPER_VERIFY);
        if (keyOperation.size()>0) jwk.setKeyOperation(keyOperation);
        if (alg != null) jwk.setAlgorithm(alg);
        if (secret != null) jwk.setProperty("k", JoseUtils.bytesToBase64URLString(secret));
        else throw new SecurityException("encoded secret is null");
        return jwk;
    }
    
    public static JwkKey generateJWKAsRSAPublicKey(String kid, boolean verifier, boolean encrypter, String alg, byte[] n, byte[] e){
        JwkKey jwk = new JwkKey();
        jwk.setKeyType(JwkKey.KEY_TYPE_RSA);
        if (kid != null) jwk.setKid(kid);
        List<String> keyOperation = new ArrayList<String>();
        if (verifier) keyOperation.add(JwkKey.KEY_OPER_VERIFY);
        if (encrypter) keyOperation.add(JwkKey.KEY_OPER_ENCRYPT);
        if (keyOperation.size()>0) jwk.setKeyOperation(keyOperation);
        if (alg != null) jwk.setAlgorithm(alg);
        if ((n != null) && (e != null)) {
            jwk.setProperty("n", JoseUtils.bytesToBase64URLString(n));
            jwk.setProperty("e", JoseUtils.bytesToBase64URLString(e));
        }
        else throw new SecurityException("encoded secret is null");
        
        return jwk;
    }
    public static JwkKey generateJWKAsRSAPrivateKey(String kid, boolean signer, boolean verifier, boolean encrypter, boolean decrypter, String alg, byte[] n, byte[] e, byte[] d, byte[] p, byte[] q, byte[] dp, byte[] dq, byte[] qi){
        JwkKey jwk = JwkMaker.generateJWKAsRSAPublicKey(kid, verifier, encrypter, alg, n, e);
        List<String> keyOperation = jwk.getKeyOperation();
        if (signer) keyOperation.add(JwkKey.KEY_OPER_SIGN);
        if (decrypter) keyOperation.add(JwkKey.KEY_OPER_DECRYPT);
        jwk.setKeyOperation(keyOperation);
        if ((d != null) && (p != null) && (q != null)){
            jwk.setProperty("d", JoseUtils.bytesToBase64URL(d));
            jwk.setProperty("d", JoseUtils.bytesToBase64URL(p));
            jwk.setProperty("d", JoseUtils.bytesToBase64URL(q));
            if (dp != null )jwk.setProperty("dp", JoseUtils.bytesToBase64URL(dp));
            if (dq != null )jwk.setProperty("dq", JoseUtils.bytesToBase64URL(dq));
            if (qi != null )jwk.setProperty("qi", JoseUtils.bytesToBase64URL(qi));
        }    
        else throw new SecurityException("value of d is missing!");
        return jwk;
    }
    
    public static JwkKey generateJWKAsECPublicKey(String kid, boolean verifier, boolean encrypter, String alg, JwaECTypes crv, String representationOfX, String representationOfY){
        JwkKey jwk = new JwkKey();
        jwk.setKeyType(JwkKey.KEY_TYPE_ELLIPTIC);
        if (kid != null) jwk.setKid(kid);
        List<String> keyOperation = new ArrayList<String>();
        if (verifier) keyOperation.add(JwkKey.KEY_OPER_VERIFY);
        if (encrypter) keyOperation.add(JwkKey.KEY_OPER_ENCRYPT);
        if (keyOperation.size()>0) jwk.setKeyOperation(keyOperation);
        if (alg != null) jwk.setAlgorithm(alg);
        if ((crv != null) && (representationOfX!=null) && (representationOfY!=null)){
            jwk.setProperty("crv", crv.getName());
            jwk.setProperty("x", JoseUtils.stringToBase64URL(representationOfX));
            jwk.setProperty("y", JoseUtils.stringToBase64URL(representationOfY));
        } else throw new SecurityException("one or more of the mandatory parameters are missing!");
        return jwk;
    }

    public static JwkKey generateJWKAsECPrivateKey(String kid, boolean signer, boolean verifier, boolean encrypter, boolean decrypter, String alg, JwaECTypes crv, String representationOfX, String representationOfY, String representationOfD){
        JwkKey jwk = JwkMaker.generateJWKAsECPublicKey(kid, verifier, encrypter, alg, crv, representationOfX, representationOfY);
        List<String> keyOperation = jwk.getKeyOperation();
        if (signer) keyOperation.add(JwkKey.KEY_OPER_SIGN);
        if (decrypter) keyOperation.add(JwkKey.KEY_OPER_DECRYPT);
        jwk.setKeyOperation(keyOperation);
        if (representationOfD != null)
            jwk.setProperty("d", JoseUtils.stringToBase64URL(representationOfD));
        else throw new SecurityException("value of d is missing!");
        return jwk;
    }
    public static JwkKey fromECPublicKey(ECPublicKey pk, String curve) {
        JwkKey jwk = new JwkKey();
        jwk.setKeyType(JwkKey.KEY_TYPE_ELLIPTIC);
        jwk.setProperty(JwkKey.EC_CURVE, curve);
        jwk.setProperty(JwkKey.EC_X_COORDINATE, 
                        JoseUtils.bytesToBase64URL(pk.getW().getAffineX().toByteArray()));
        jwk.setProperty(JwkKey.EC_Y_COORDINATE, 
                JoseUtils.bytesToBase64URL(pk.getW().getAffineY().toByteArray()));
        return jwk;
    }
    public static JwkKey fromECPrivateKey(ECPrivateKey pk, String curve) {
        JwkKey jwk = new JwkKey();
        jwk.setKeyType(JwkKey.KEY_TYPE_ELLIPTIC);
        jwk.setProperty(JwkKey.EC_CURVE, curve);
        jwk.setProperty(JwkKey.EC_PRIVATE_KEY, 
                JoseUtils.bytesToBase64URL(pk.getS().toByteArray()));
        return jwk;
    }
    public static JwkKey fromRSAPublicKey(RSAPublicKey pk, String algo){
        JwkKey jwk = prepareRSAJwk(pk.getModulus(), algo);
        String encodedPublicExponent;
        try {
            encodedPublicExponent = new String(JoseUtils.bytesToBase64URL(pk.getPublicExponent().toByteArray()), "UTF-8");

        jwk.setProperty(JwkKey.RSA_PUBLIC_EXP, encodedPublicExponent);
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jwk;
    }
    
    public static JwkKey fromRSAPrivateKey(RSAPrivateKey pk, String algo) {
        JwkKey jwk = prepareRSAJwk(pk.getModulus(), algo);
        String encodedPrivateExponent = JoseUtils.bytesToBase64URLString(pk.getPrivateExponent().toByteArray());
        jwk.setProperty(JwkKey.RSA_PRIVATE_EXP, encodedPrivateExponent);
        return jwk;
    }

    public static JwkKey fromSecretKey(SecretKey secretKey, String algo) {
        if (!JwaAlgorithms.isHmacSign(algo)) {
            throw new SecurityException("Invalid algorithm");
        }
        JwkKey jwk = new JwkKey();
        jwk.setKeyType(JwkKey.KEY_TYPE_OCTET);
        jwk.setAlgorithm(algo);
        String encodedSecretKey = JoseUtils.bytesToBase64URLString(secretKey.getEncoded());
        jwk.setProperty(JwkKey.OCTET_KEY_VALUE, encodedSecretKey);
        return jwk;
    }
    
    private static JwkKey prepareRSAJwk(BigInteger modulus, String algo) {
        if (!JwaAlgorithms.isRsaSign(algo)) {
            throw new SecurityException("Invalid algorithm");
        }
        JwkKey jwk = new JwkKey();
        jwk.setKeyType(JwkKey.KEY_TYPE_RSA);
        jwk.setAlgorithm(algo);
        String encodedModulus = JoseUtils.bytesToBase64URLString(modulus.toByteArray());
        jwk.setProperty(JwkKey.RSA_MODULUS, encodedModulus);
        return jwk;
    }
    public static JwkKey readJwkKey(URI uri){
        try {
            return readJwkKey(uri.toURL().openStream());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }
    public static JwkKeySet readJwkSet(URI uri){
        try {
            return readJwkSet(uri.toURL().openStream());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }
    public static JwkKey readJwkKey(InputStream is){
        try {
            return readJwkKey(IOUtils.streamToString(is));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }
    public static JwkKeySet readJwkSet(InputStream is){
        try {
            return readJwkSet(IOUtils.streamToString(is));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }
    public static JwkKey readJwkKey(String jwkJson){
        return new JwkKey(jwkJson);
    }
    public static JwkKeySet readJwkSet(String jwksJson){
        return new JwkKeySet(jwksJson);
    }
    public static JwkKey decodeJwkKey(String jwkJson){
        JwkKey jwk = new JwkKey();
    	jwk = readJwkKey(JoseUtils.base64URLToString(jwkJson));
    	return jwk;
    }
    public static JwkKeySet decodeJwkSet(String jwksJson){
        JwkKeySet set = new JwkKeySet();
    	set = readJwkSet(JoseUtils.base64URLToString(jwksJson));
    	return set;
    }

}

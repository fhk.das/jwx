/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwa;

/**
 * this enum class provides the specific length of signatures.
 *
 */
public enum JwaLengthOfSignature {
        LengthOfES256(64), LengthOfES384(96), LengthOfES512(132);
        private int length;
        private JwaLengthOfSignature(int length){
            this.length = length;
        }
        private int getLength(){
            return this.length;
        }
        public static int lengthOf(final JwaAlgorithms type){
            if (type.getSpecName().startsWith("ES")){
                if (type.getSpecName().equals(JwaAlgorithms.ES_SHA_256.getSpecName())) return LengthOfES256.getLength();
                else if (type.getSpecName().equals(JwaAlgorithms.ES_SHA_384.getSpecName())) return LengthOfES384.getLength();
                else if (type.getSpecName().equals(JwaAlgorithms.ES_SHA_512.getSpecName())) return LengthOfES512.getLength(); 
            }
            return 0;
        
            
        }

}
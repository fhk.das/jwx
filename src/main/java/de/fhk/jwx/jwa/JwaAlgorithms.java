/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwa;

import de.fhk.jwx.jwe.JoseException;
import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.jwk.JwkKeySet;


/**
 * this enum class provides defined algorithms by jose and their specific names.
 *
 */
public enum JwaAlgorithms {
    PLAIN_TEXT("none", "none"), HMAC_SHA_256("HS256", "HmacSHA256"), HMAC_SHA_384(
            "HS384", "HmacSHA384"), HMAC_SHA_512("HS512", "HmacSHA512"), RS_SHA_256(
            "RS256", "SHA256withRSA"), RS_SHA_384("RS384", "SHA384withRSA"), RS_SHA_512(
            "RS512", "SHA512withRSA"), PS_SHA_256("PS256",
            "SHA256withRSAandMGF1"), PS_SHA_384("PS384", "SHA384withRSAandMGF1"), PS_SHA_512(
            "PS512", "SHA512withRSAandMGF1"), ES_SHA_256("ES256",
            "SHA256withECDSA"), ES_SHA_384("ES384", "SHA384withECDSA"), ES_SHA_512(
            "ES512", "SHA512withECDSA"),

    RSA1_5("RSA1_5", "RSA/ECB/PKCS1Padding"), RSA_OAEP("RSA-OAEP",
            "RSA/ECB/OAEPWithSHA-1AndMGF1Padding"), RSA_OAEP_256(
            "RSA-OAEP-256", "RSA/ECB/OAEPWithSHA-256AndMGF1Padding"), AES_128_KW(
            "A128KW", "AESWrap"), AES_192_KW("A192KW", "AESWrap"), AES_256_KW(
            "A256KW", "AESWrap"), DIRECT("dir", ""), ECDH_ES("ECDH-ES",
            "AESWrap"), ECDH_ES_AES128KW("ECDH-ES+A128KW", "AESWrap"), ECDH_ES_AES192KW(
            "ECDH-ES+A192KW", "AESWrap"), ECDH_ES_AES256KW("ECDH-ES+A256KW",
            "AESWrap"), AES_128_GCMKW("A128GCMKW", "AES/GCM/NoPadding"), AES_192_GCMKW(
            "A192GCMKW", "AES/GCM/NoPadding"), AES_256_GCMKW("A256GCMKW",
            "AES/GCM/NoPadding"), PBES2_HS256_A128KW("PBES2-HS256+A128KW", "AESWrap"), PBES2_HS384_A192KW(
            "PBES2-HS384+A192KW", "AESWrap"), PBES2_HS512_A256KW("PBES2-HS512+A256KW",
            "AESWrap"),

    
    AES_128_CBC_HMAC_SHA_256(
    		"A128CBC-HS256", "AES/CBC/PKCS7Padding"), AES_192_CBC_HMAC_SHA_384(
            "A192CBC-HS384", "AES/CBC/PKCS7Padding"), AES_256_CBC_HMAC_SHA_512(
            "A256CBC-HS512", "AES/CBC/PKCS7Padding"), AES_128_GCM("A128GCM",
            "AES/GCM/NoPadding"), AES_192_GCM("A192GCM", "AES/GCM/NoPadding"), AES_256_GCM(
            "A256GCM", "AES/GCM/NoPadding");

    private String specName;
    private String javaName;

    private JwaAlgorithms(String specName, String javaName) {
        this.specName = specName;
        this.javaName = javaName;
    }

    public String getSpecName() {
        return this.specName;
    }

    public String getJavaName() {
        return this.javaName;
    }
    
    public static JwaAlgorithms getAlgorithm(String specName) {

        if (specName.startsWith("A")) {
            if (specName.equals(JwaAlgorithms.AES_128_KW.getSpecName()))
                return JwaAlgorithms.AES_128_KW;
            else if (specName.equals(JwaAlgorithms.AES_192_KW.getSpecName()))
                return JwaAlgorithms.AES_192_KW;
            else if (specName.equals(JwaAlgorithms.AES_256_KW.getSpecName()))
                return JwaAlgorithms.AES_256_KW;
            else if (specName.equals(JwaAlgorithms.AES_128_GCMKW.getSpecName()))
                return JwaAlgorithms.AES_128_GCMKW;
            else if (specName.equals(JwaAlgorithms.AES_192_GCMKW.getSpecName()))
                return JwaAlgorithms.AES_192_GCMKW;
            else if (specName.equals(JwaAlgorithms.AES_256_GCMKW.getSpecName()))
                return JwaAlgorithms.AES_256_GCMKW;
            else if (specName.equals(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256
                    .getSpecName()))
                return JwaAlgorithms.AES_128_CBC_HMAC_SHA_256;
            else if (specName.equals(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384
                    .getSpecName()))
                return JwaAlgorithms.AES_192_CBC_HMAC_SHA_384;
            else if (specName.equals(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512
                    .getSpecName()))
                return JwaAlgorithms.AES_256_CBC_HMAC_SHA_512;
            else if (specName.equals(JwaAlgorithms.AES_128_GCM.getSpecName()))
                return JwaAlgorithms.AES_128_GCM;
            else if (specName.equals(JwaAlgorithms.AES_192_GCM.getSpecName()))
                return JwaAlgorithms.AES_192_GCM;
            else if (specName.equals(JwaAlgorithms.AES_256_GCM.getSpecName()))
                return JwaAlgorithms.AES_256_GCM;
        } else if (specName.startsWith("dir"))
            return JwaAlgorithms.DIRECT;
        else if (specName.startsWith("EC")) {
            if (specName.equals(JwaAlgorithms.ECDH_ES.getSpecName()))
                return JwaAlgorithms.ECDH_ES;
            else if (specName.equals(JwaAlgorithms.ECDH_ES_AES128KW
                    .getSpecName()))
                return JwaAlgorithms.ECDH_ES_AES128KW;
            else if (specName.equals(JwaAlgorithms.ECDH_ES_AES192KW
                    .getSpecName()))
                return JwaAlgorithms.ECDH_ES_AES192KW;
            else if (specName.equals(JwaAlgorithms.ECDH_ES_AES256KW
                    .getSpecName()))
                return JwaAlgorithms.ECDH_ES_AES256KW;
        } else if (specName.startsWith("ES")) {
            if (specName.equals(JwaAlgorithms.ES_SHA_256.getSpecName()))
                return JwaAlgorithms.ES_SHA_256;
            else if (specName.equals(JwaAlgorithms.ES_SHA_384.getSpecName()))
                return JwaAlgorithms.ES_SHA_384;
            else if (specName.equals(JwaAlgorithms.ES_SHA_512.getSpecName()))
                return JwaAlgorithms.ES_SHA_512;
        } else if (specName.startsWith("HS")) {
            if (specName.equals(JwaAlgorithms.HMAC_SHA_256.getSpecName()))
                return JwaAlgorithms.HMAC_SHA_256;
            else if (specName.equals(JwaAlgorithms.HMAC_SHA_384.getSpecName()))
                return JwaAlgorithms.HMAC_SHA_384;
            else if (specName.equals(JwaAlgorithms.HMAC_SHA_512.getSpecName()))
                return JwaAlgorithms.HMAC_SHA_512;
        } else if (specName.startsWith("none"))
            return JwaAlgorithms.PLAIN_TEXT;
        else if (specName.startsWith("PB")) {
            if (specName.equals(JwaAlgorithms.PBES2_HS256_A128KW.getSpecName()))
                return JwaAlgorithms.PBES2_HS256_A128KW;
            else if (specName.equals(JwaAlgorithms.PBES2_HS384_A192KW
                    .getSpecName()))
                return JwaAlgorithms.PBES2_HS384_A192KW;
            else if (specName.equals(JwaAlgorithms.PBES2_HS512_A256KW
                    .getSpecName()))
                return JwaAlgorithms.PBES2_HS512_A256KW;

        } else if (specName.startsWith("PS")) {
            if (specName.equals(JwaAlgorithms.PS_SHA_256.getSpecName()))
                return JwaAlgorithms.PS_SHA_256;
            else if (specName.equals(JwaAlgorithms.PS_SHA_384.getSpecName()))
                return JwaAlgorithms.PS_SHA_384;
            else if (specName.equals(JwaAlgorithms.PS_SHA_512.getSpecName()))
                return JwaAlgorithms.PS_SHA_512;

        } else if (specName.startsWith("RS")) {
            if (specName.equals(JwaAlgorithms.RS_SHA_256.getSpecName()))
                return JwaAlgorithms.RS_SHA_256;
            else if (specName.equals(JwaAlgorithms.RS_SHA_384.getSpecName()))
                return JwaAlgorithms.RS_SHA_384;
            else if (specName.equals(JwaAlgorithms.RS_SHA_512.getSpecName()))
                return JwaAlgorithms.RS_SHA_512;
            else if (specName.equals(JwaAlgorithms.RSA1_5.getSpecName()))
                return JwaAlgorithms.RSA1_5;
            else if (specName.equals(JwaAlgorithms.RSA_OAEP.getSpecName()))
                return JwaAlgorithms.RSA_OAEP;
            else if (specName.equals(JwaAlgorithms.RSA_OAEP_256.getSpecName()))
                return JwaAlgorithms.RSA_OAEP_256;
        }
        return null;
    }

    public static boolean isEcdsaSign(String name) {
        if (name.equals(JwaAlgorithms.ES_SHA_256.getSpecName())
                || name.equals(JwaAlgorithms.ES_SHA_384.getSpecName())
                || name.equals(JwaAlgorithms.ES_SHA_512.getSpecName()))
            return true;
        return false;
    }

    public static boolean isHmacSign(String name) {
        if (name.equals(JwaAlgorithms.HMAC_SHA_256.getSpecName())
                || name.equals(JwaAlgorithms.HMAC_SHA_384.getSpecName())
                || name.equals(JwaAlgorithms.HMAC_SHA_512.getSpecName()))
            return true;
        return false;
    }

    public static boolean isRsaSign(String name) {
        if (name.equals(JwaAlgorithms.RS_SHA_256.getSpecName())
                || name.equals(JwaAlgorithms.RS_SHA_384.getSpecName())
                || name.equals(JwaAlgorithms.RS_SHA_512.getSpecName()))
            return true;
        return false;
    }

    public static boolean isNoneAlg(String name) {
        if (name.equals(JwaAlgorithms.PLAIN_TEXT.getSpecName()))
            return true;
        return false;
    }

    public static boolean isRsassapssSign(String name) {
        if (name.equals(JwaAlgorithms.PS_SHA_256.getSpecName())
                || name.equals(JwaAlgorithms.PS_SHA_384.getSpecName())
                || name.equals(JwaAlgorithms.PS_SHA_512.getSpecName()))
            return true;
        return false;
    }

    public static boolean isContentEncAlg(String name) {
        if (name.equals(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256.getSpecName())
                || name.equals(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384
                        .getSpecName())
                || name.equals(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512
                        .getSpecName())
                || name.equals(JwaAlgorithms.AES_128_GCM.getSpecName())
                || name.equals(JwaAlgorithms.AES_192_GCM.getSpecName())
                || name.equals(JwaAlgorithms.AES_256_GCM.getSpecName()))
            return true;
        return false;
    }

    public static boolean isContentEncAlgCBC(String name) {
        if (name.equals(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256.getSpecName())
                || name.equals(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384
                        .getSpecName())
                || name.equals(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512
                        .getSpecName()))
            return true;
        return false;
    }

    public static boolean isContentEncAlgGCM(String name) {
        if (name.equals(JwaAlgorithms.AES_128_GCM.getSpecName())
                || name.equals(JwaAlgorithms.AES_192_GCM.getSpecName())
                || name.equals(JwaAlgorithms.AES_256_GCM.getSpecName()))
            return true;
        return false;
    }

    public static boolean isKeyManagementAlg(String name) {
        if (name.equals(JwaAlgorithms.AES_128_KW.getSpecName())
                || name.equals(JwaAlgorithms.AES_192_KW.getSpecName())
                || name.equals(JwaAlgorithms.AES_256_KW.getSpecName())
                || name.equals(JwaAlgorithms.AES_128_GCMKW.getSpecName())
                || name.equals(JwaAlgorithms.AES_192_GCMKW.getSpecName())
                || name.equals(JwaAlgorithms.AES_256_GCMKW.getSpecName())
                || name.equals(JwaAlgorithms.DIRECT.getSpecName())
                || name.equals(JwaAlgorithms.ECDH_ES.getSpecName())
                || name.equals(JwaAlgorithms.ECDH_ES_AES128KW.getSpecName())
                || name.equals(JwaAlgorithms.ECDH_ES_AES192KW.getSpecName())
                || name.equals(JwaAlgorithms.ECDH_ES_AES256KW.getSpecName())
                || name.equals(JwaAlgorithms.PBES2_HS256_A128KW.getSpecName())
                || name.equals(JwaAlgorithms.PBES2_HS384_A192KW.getSpecName())
                || name.equals(JwaAlgorithms.PBES2_HS512_A256KW.getSpecName())
                || name.equals(JwaAlgorithms.RSA1_5.getSpecName())
                || name.equals(JwaAlgorithms.RSA_OAEP.getSpecName())
                || name.equals(JwaAlgorithms.RSA_OAEP_256.getSpecName()))
            return true;
        return false;
    }

    public static boolean isKeyManagementDir(String name) {
        if (name.equals(JwaAlgorithms.DIRECT.getSpecName()))
            return true;
        return false;
    }

    public static boolean isKeyManagementAesWrap(String name) {
        if (name.equals(JwaAlgorithms.AES_128_KW.getSpecName())
                || name.equals(JwaAlgorithms.AES_192_KW.getSpecName())
                || name.equals(JwaAlgorithms.AES_256_KW.getSpecName()))
            return true;
        return false;
    }

    public static boolean isKeyManagementRsa(String name) {
        if (name.equals(JwaAlgorithms.RSA1_5.getSpecName())
                || name.equals(JwaAlgorithms.RSA_OAEP.getSpecName())
                || name.equals(JwaAlgorithms.RSA_OAEP_256.getSpecName()))
            return true;
        return false;
    }

    public static boolean isKeyManagementPbes(String name) {
  
            if (   name.equals(JwaAlgorithms.PBES2_HS256_A128KW.getSpecName())
                || name.equals(JwaAlgorithms.PBES2_HS384_A192KW.getSpecName())
                || name.equals(JwaAlgorithms.PBES2_HS512_A256KW.getSpecName())
               ){
            	return true;
            }
            else{
            	return false;
            }
    }
    
    public static boolean isKeyManagementEcdh(String name) {
        if (
                name.equals(JwaAlgorithms.ECDH_ES_AES128KW.getSpecName())
                || name.equals(JwaAlgorithms.ECDH_ES_AES192KW.getSpecName())
                || name.equals(JwaAlgorithms.ECDH_ES_AES256KW.getSpecName()))
            return true;
        return false;
    }
    
    public static boolean isKeyManagementGcmWrap(String name) {
        if (name.equals(JwaAlgorithms.AES_128_GCMKW.getSpecName())
                || name.equals(JwaAlgorithms.AES_192_GCMKW.getSpecName())
                || name.equals(JwaAlgorithms.AES_256_GCMKW.getSpecName()))
            return true;
        return false;
    }
    

}

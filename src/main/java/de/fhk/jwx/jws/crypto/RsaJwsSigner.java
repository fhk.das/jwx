/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jws.crypto;

import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;

import de.fhk.jwx.jwa.JwaAlgorithms;

/**
 * this class provides the signer for RS-* algorithms.
 *
 */
public class RsaJwsSigner extends AbstractPrivateKeyJwsSigner{

    public RsaJwsSigner(PrivateKey key, AlgorithmParameterSpec spec) {
        super(key, spec);
    }
    
    
    public RsaJwsSigner(PrivateKey key) {
        super(key, null, null);
    }
    public RsaJwsSigner(PrivateKey key, SecureRandom random, 
                                          AlgorithmParameterSpec spec) {
        super(key, random, spec);
    }

    @Override
    boolean isProvidedAlg(String alg) {
        return JwaAlgorithms.isRsaSign(alg);
    }

}

/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jws.crypto;

import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.AlgorithmParameterSpec;

import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jws.JwsHeaders;

/**
 * this class provides the verifier for RS-*, ES-* and PS-*.
 *
 */
abstract class AbstractPublicKeyJwsVerifier implements JwsVerifier {
    private PublicKey key;
    private AlgorithmParameterSpec spec;
    public AbstractPublicKeyJwsVerifier(PublicKey key) {
        this(key, null);
    }

    public AbstractPublicKeyJwsVerifier(PublicKey key, AlgorithmParameterSpec spec) {
        this.key = key;
        this.spec = spec;
    }
    abstract boolean isProvidedAlg(String alg);

    public boolean verify(JwsHeaders headers, byte[] content, byte[] signature) {
        String alg = headers.getAlgorithm();
        if (!isProvidedAlg(alg)) return false;
        try {
            Signature s = Signature.getInstance(JwaAlgorithms.getAlgorithm(alg).getJavaName());
            s.initVerify(key);
            if (this.spec != null) {
                s.setParameter(this.spec);
            }
            s.update(content);
            return s.verify(signature);
        } catch (Exception ex) {
            throw new SecurityException(ex);
        }
    }


}

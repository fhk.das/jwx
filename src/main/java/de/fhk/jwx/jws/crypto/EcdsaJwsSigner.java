/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jws.crypto;

import java.security.SecureRandom;
import java.security.interfaces.ECPrivateKey;
import java.security.spec.AlgorithmParameterSpec;

import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jwa.JwaLengthOfSignature;
import de.fhk.jwx.jws.JwsHeaders;

/**
 * this class provides the signer for ES-* algorithms.
 *
 */
public class EcdsaJwsSigner extends AbstractPrivateKeyJwsSigner {
    public EcdsaJwsSigner(ECPrivateKey key) {
        this(key, null, null);
    }

    public EcdsaJwsSigner(ECPrivateKey key, AlgorithmParameterSpec spec) {
        this(key, null, spec);
    }

    public EcdsaJwsSigner(ECPrivateKey key, SecureRandom random,
            AlgorithmParameterSpec spec) {
        super(key, random, spec);
    }

    @Override
    public byte[] sign(JwsHeaders headers, byte[] content) {
        byte[] sig = super.sign(headers, content);
        int length = JwaLengthOfSignature.lengthOf(JwaAlgorithms.getAlgorithm(headers.getAlgorithm()));
        int resultLength =Integer.MAX_VALUE;
        byte[] result=null;
        while(resultLength>length){     
            int startR = 4;
            int lengthOfR = 3;
            if (sig[1] == -127) {
                startR++;
                lengthOfR++;
            }
            byte[] r = new byte[sig[lengthOfR]];
            System.arraycopy(sig, startR, r, 0, sig[lengthOfR]);
            int lengthOfS = startR + sig[lengthOfR] + 1;
            byte[] s = new byte[sig[lengthOfS]];
            System.arraycopy(sig, lengthOfS + 1, s, 0, sig[lengthOfS]);
            int tempLength = r.length;
            if (s.length > tempLength)
                tempLength = s.length;
            if (tempLength % 2 != 0)
                tempLength++;

            result = new byte[tempLength * 2];
            int startPosition = tempLength - r.length;
            if (startPosition < 0)
                startPosition = 0;
            System.arraycopy(r, 0, result, startPosition, result.length / 2
                    - startPosition);

            startPosition = tempLength - s.length;
            System.arraycopy(s, 0, result, result.length / 2 + startPosition,
                    result.length / 2 - startPosition);
        
            if (length-result.length>4 || length-result.length<-4) throw new SecurityException("algorithm dosen't match to key!");
            sig = super.sign(headers, content);
            resultLength = result.length;
        }
        return result;
    }

    @Override
    boolean isProvidedAlg(String alg) {
        return JwaAlgorithms.isEcdsaSign(alg);
    }
}

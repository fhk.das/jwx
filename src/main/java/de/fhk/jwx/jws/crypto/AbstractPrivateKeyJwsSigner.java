/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jws.crypto;

import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.spec.AlgorithmParameterSpec;

import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jws.JwsHeaders;

/**
 * this class provides the signer for RS-*, ES-* and PS-*.
 *
 */
abstract class AbstractPrivateKeyJwsSigner implements JwsSigner {

    private PrivateKey key;
    private SecureRandom random;
    private AlgorithmParameterSpec spec;

    public AbstractPrivateKeyJwsSigner(PrivateKey key) {
        this(key, null, null);
    }

    public AbstractPrivateKeyJwsSigner(PrivateKey key,
            AlgorithmParameterSpec spec) {
        this(key, null, spec);
    }

    public AbstractPrivateKeyJwsSigner(PrivateKey key, SecureRandom random,
            AlgorithmParameterSpec spec) {
        super();
        this.key = key;
        this.random = random;
        this.spec = spec;
    }

    abstract boolean isProvidedAlg(String alg);

    protected PrivateKey getKey() {
        return this.key;
    }

    public byte[] sign(JwsHeaders headers, byte[] data) {
        if (!isProvidedAlg(headers.getAlgorithm()))
            throw new SecurityException("alg not provided!");
        try {
            Signature s = Signature.getInstance(JwaAlgorithms.getAlgorithm(
                    headers.getAlgorithm()).getJavaName());
            if (random == null) {
                s.initSign(key);
            } else {
                s.initSign(key, random);
            }
            if (spec != null) {
                s.setParameter(spec);
            }
            s.update(data);

            return s.sign();
        } catch (Exception ex) {
            throw new SecurityException(ex);
        }
    }
}

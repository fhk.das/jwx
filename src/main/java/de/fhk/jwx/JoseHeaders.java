/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhk.jwx;

import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;

import de.fhk.jwx.jwk.JwkKey;

/**
 * This interface provides the header constants and the methods for jose headers.
 */
public interface JoseHeaders {
    
    //all Jose Header Parameters understood by this implementation
    public static final String HEADER_ALGORITHM = "alg";
    public static final String JWE_HEADER_CONTENT_ENC_ALGORITHM = "enc";
    public static final String JWE_HEADER_ZIP_ALGORITHM = "zip";    
    
    public static final String HEADER_JSON_WEB_KEY_SET_URL = "jku";
    public static final String HEADER_JSON_WEB_KEY = "jwk";
    public static final String HEADER_KEY_ID = "kid";
    public static final String HEADER_X509_URL = "x5u";
    public static final String HEADER_X509_CHAIN = "x5c";
    public static final String HEADER_X509_THUMBPRINT = "x5t";
    public static final String HEADER_X509_THUMBPRINT_SHA256 = "x5t#S256";
    
    public static final String HEADER_TYPE = "typ";
    public static final String HEADER_CONTENT_TYPE = "cty";
    public static final String HEADER_CRITICAL = "crit";
    
    public static final String JWE_HEADER_PBES_SALT = "p2s";
    public static final String JWE_HEADER_PBES_ITERATION_COUNT = "p2c";
    
    public static final String JWE_HEADER_ECDHES_EPHEMERAL_PUBLIC_KEY = "epk";
    public static final String JWE_HEADER_ECDHES_AGREEMENT_PARTYUINFO = "epu";
    public static final String JWE_HEADER_ECDHES_AGREEMENT_PARTYVINFO = "epv";
    
    public static final String JWE_HEADER_GCMKW_KEY_WRAP_INITIALIZATION_VECTOR = "iv";
    public static final String JWE_HEADER_GCMKW_KEY_WRAP_AUTHENTICATION_TAG = "tag";
    Map<String, Object> headers = new LinkedHashMap<String, Object>();

    /**
     * set Public or Private Header Parameter Names
     * @param key Public/Private header
     * @param value Public/Private header value
     */
    public void setHeader(String key, Object value);

    /**
     * get Public or Private Header Parameter Names
     * @param key Public/Private header name
     * @return Public/Private header
     */
    public default Object getHeader(String key) {
        return JoseHeaders.headers.get(key);
    }
    
    public void setHeaders(Map<String, Object> headers);
    
    public String getHeaders() throws JsonProcessingException;
    
    public String getHeaders(boolean prettyPrint) throws JsonProcessingException;

    public String getBase64URLEncodedHeaders() throws JsonProcessingException;

    public Map<String, Object> getAllHeaders();

    public void setAlgorithm(String algo);

    public String getAlgorithm();

    public void setJSONWebKeySet(URL url);
    
    public URL getJSONWebKeySet();
    
    public void setJSONWebKey(JwkKey key);
    
    public JwkKey getJSONWebKey();
    
    public void setKeyId(String kid);

    public String getKeyId();
    
    public void setX509Url(URL x509Url);

    public URL getX509Url();

    public void setX509Chain(List<String> x509Chain);

    public List<String> getX509Chain();

    public void setX509Thumbprint(String x509Thumbprint);

    public String getX509Thumbprint();

    public void setX509ThumbprintSHA256(String x509Thumbprint);

    public String getX509ThumbprintSHA256();
    
    public void setType(String type);

    public String getType();

    public void setContentType(String type);

    public String getContentType();
    
    public int getNumberOfEntries();
    
    public boolean contains(Object key);
    
    public void removeHeaderEntry(String key);
    
    public static List<String> defindedHeaderParametersAsList() {
        List<String> list = new ArrayList<String>();
        list.add(JoseHeaders.HEADER_ALGORITHM);
        list.add(JoseHeaders.HEADER_CONTENT_TYPE);
        list.add(JoseHeaders.HEADER_CRITICAL);
        list.add(JoseHeaders.HEADER_JSON_WEB_KEY);
        list.add(JoseHeaders.HEADER_JSON_WEB_KEY_SET_URL);
        list.add(JoseHeaders.HEADER_KEY_ID);
        list.add(JoseHeaders.HEADER_TYPE);
        list.add(JoseHeaders.HEADER_X509_CHAIN);
        list.add(JoseHeaders.HEADER_X509_THUMBPRINT);
        list.add(JoseHeaders.HEADER_X509_THUMBPRINT_SHA256);
        list.add(JoseHeaders.HEADER_X509_URL);
        list.add(JoseHeaders.JWE_HEADER_CONTENT_ENC_ALGORITHM);
        list.add(JoseHeaders.JWE_HEADER_ZIP_ALGORITHM);
        list.add(JoseHeaders.JWE_HEADER_ECDHES_EPHEMERAL_PUBLIC_KEY);
        list.add(JoseHeaders.JWE_HEADER_ECDHES_AGREEMENT_PARTYUINFO);
        list.add(JoseHeaders.JWE_HEADER_ECDHES_AGREEMENT_PARTYVINFO);
        list.add(JoseHeaders.JWE_HEADER_PBES_SALT);
        list.add(JoseHeaders.JWE_HEADER_PBES_ITERATION_COUNT);    
        list.add(JoseHeaders.JWE_HEADER_GCMKW_KEY_WRAP_INITIALIZATION_VECTOR);
        list.add(JoseHeaders.JWE_HEADER_GCMKW_KEY_WRAP_AUTHENTICATION_TAG);
        return list;
    }
    
}


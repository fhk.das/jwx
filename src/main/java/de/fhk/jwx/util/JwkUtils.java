/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.util;

import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.ECParameterSpec;
import java.security.spec.ECPoint;
import java.security.spec.ECPrivateKeySpec;
import java.security.spec.ECPublicKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.text.ParseException;

import javafx.util.converter.BigIntegerStringConverter;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.jwk.JwkKeySet;

public class JwkUtils {
    
    public static String encodeJwkKey(JwkKey jwkKey) {
        return JoseUtils.stringToBase64URL(jwkKey.toJSON());
    }

    public static String encodeJwkSet(JwkKeySet jwkSet) {
        return JoseUtils.stringToBase64URL(jwkSet.toJSON());
    }

    public static Key convertJwkToSecretKeySpec(JwkKey key){
        if (key.getProperty("k") == null)
            throw new SecurityException("cannot convert to SecretKeySpec.");
        return new SecretKeySpec(JoseUtils.base64URLTobytes((String) key
                .getProperty("k")), "AES");
    }

    public static Key convertJwksToSecretKeySpec(JwkKey... key){
        String wholeKey = "";
        for (int i = 0; i < key.length; i++) {
            if (key[i].getProperty("k") == null)
                throw new SecurityException("cannot convert to SecretKeySpec.");
            wholeKey += JoseUtils.base64URLTobytes((String) key[i]
                    .getProperty("k"));
        }
        return new SecretKeySpec(wholeKey.getBytes(), "AES");
    }

    //TODO private?
    private static BigInteger toBigInteger(byte[] bytes) {
        if (bytes[0] == -128) {
            return new BigInteger(bytes);
        } else {
            return new BigInteger(1, bytes);
        }
    }
    
    public static byte[] convertHexStringToByteArray(String hex) {    	
    	return DatatypeConverter.parseHexBinary(hex);
    }
        
    public static String convertByteArrayToHexString(byte[] array) {    
    	return DatatypeConverter.printHexBinary(array);
    }    
    
    
    public static byte[] toBytes(BigInteger integer){
       BigIntegerStringConverter con= new BigIntegerStringConverter();
       return (con.toString(integer)).getBytes();
       
    }

    public static RSAPrivateKey convertJwkToRSAPrivateKey(JwkKey jwk) {
        /*
         * http://docs.oracle.com/cd/E12839_01/apirefs.1111/e10668/oracle/security
         * /crypto/core/RSAPrivateKey.html#
         * RSAPrivateKey_java_math_BigInteger__java_math_BigInteger__java_math_BigInteger__java_math_BigInteger__java_math_BigInteger__java_math_BigInteger__java_math_BigInteger__java_math_BigInteger_
         * Parameters: mod - The modulus. exp - The private exponent. e - The
         * public exponent. p - A big prime p. q - A big prime q. e1 - Prime
         * exponent p. e2 - Prime exponent q. c - The CRT coefficient.
         * publicExponent, privateExponent, primeP, primeQ, primeExpP,
         * primeExpQ, crtCoefficient));
         */
        java.math.BigInteger mod, exp, e, p, q, e1, e2, c;
        mod = toBigInteger(JoseUtils.base64URLTobytes((String) jwk
                .getProperty(JwkKey.RSA_MODULUS)));
        exp = toBigInteger(JoseUtils.base64URLTobytes((String) jwk
                .getProperty(JwkKey.RSA_PRIVATE_EXP)));
        if (jwk.getProperty(JwkKey.RSA_PUBLIC_EXP) == null) {
            try {
                return (RSAPrivateKey) KeyFactory.getInstance("RSA")
                        .generatePrivate(new RSAPrivateKeySpec(mod, exp));
            } catch (InvalidKeySpecException | NoSuchAlgorithmException e3) {
                // TODO Auto-generated catch block
                e3.printStackTrace();
            }
        }

        e = toBigInteger(JoseUtils.base64URLTobytes((String) jwk
                .getProperty(JwkKey.RSA_PUBLIC_EXP)));
        p = toBigInteger(JoseUtils.base64URLTobytes((String) jwk
                .getProperty(JwkKey.RSA_FIRST_PRIME_FACTOR)));
        q = toBigInteger(JoseUtils.base64URLTobytes((String) jwk
                .getProperty(JwkKey.RSA_SECOND_PRIME_FACTOR)));
        e1 = toBigInteger(JoseUtils.base64URLTobytes((String) jwk
                .getProperty(JwkKey.RSA_FIRST_PRIME_CRT)));
        e2 = toBigInteger(JoseUtils.base64URLTobytes((String) jwk
                .getProperty(JwkKey.RSA_SECOND_PRIME_CRT)));
        c = toBigInteger(JoseUtils.base64URLTobytes((String) jwk
                .getProperty(JwkKey.RSA_FIRST_CRT_COEFFICIENT)));

        try {
            return (RSAPrivateKey) KeyFactory.getInstance("RSA")
                    .generatePrivate(
                            new RSAPrivateCrtKeySpec(mod, e, exp, p, q, e1, e2,
                                    c));
        } catch (InvalidKeySpecException | NoSuchAlgorithmException e3) {
            // TODO Auto-generated catch block
            e3.printStackTrace();
            return null;
        }
    }

    public static RSAPublicKey convertJwkToRSAPublicKey(JwkKey jwk) {
        /*
         * Parameters: mod - The modulus. e - The public exponent.
         */
        java.math.BigInteger mod, e;
        mod = toBigInteger(JoseUtils.base64URLTobytes((String) jwk
                .getProperty(JwkKey.RSA_MODULUS)));
        e = toBigInteger(JoseUtils.base64URLTobytes((String) jwk
                .getProperty(JwkKey.RSA_PUBLIC_EXP)));
        try {
            return (RSAPublicKey) KeyFactory.getInstance("RSA").generatePublic(
                    new RSAPublicKeySpec(mod, e));
        } catch (InvalidKeySpecException | NoSuchAlgorithmException e3) {
            // TODO Auto-generated catch block
            e3.printStackTrace();
            return null;
        }
    }

    public static ECPrivateKey convertJwkToECPrivateKey(JwkKey jwk) {

        try {
            KeyPairGenerator kpg = KeyPairGenerator.getInstance("EC");
            
            ECGenParameterSpec paramSpec = new ECGenParameterSpec("sec"
                    + ((String) jwk.getProperty(JwkKey.EC_CURVE)).toLowerCase()
                            .replace("-", "") + "r1");
            
            kpg.initialize(paramSpec);
            
            ECParameterSpec params = ((ECPrivateKey) kpg.generateKeyPair()
                    .getPrivate()).getParams();
            
            ECPrivateKeySpec keySpec = new ECPrivateKeySpec(
                    toBigInteger(JoseUtils.base64URLTobytes((String) jwk
                            .getProperty(JwkKey.EC_PRIVATE_KEY))), params);
            
            KeyFactory factory = KeyFactory.getInstance("EC");
            
            return (ECPrivateKey) factory.generatePrivate(keySpec);
            
        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException
                | InvalidKeySpecException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }

    }

    public static PublicKey convertJwkToECPublicKey(JwkKey jwk) {
        try {
        BigInteger xCoord = toBigInteger(JoseUtils.base64URLTobytes((String)jwk.getProperty(JwkKey.EC_X_COORDINATE)));
        BigInteger yCoord = toBigInteger(JoseUtils.base64URLTobytes((String)jwk.getProperty(JwkKey.EC_Y_COORDINATE)));
        KeyPairGenerator kpg;

            kpg = KeyPairGenerator.getInstance("EC");
       
        ECGenParameterSpec paramSpec = new ECGenParameterSpec("sec"
                + ((String) jwk.getProperty(JwkKey.EC_CURVE)).toLowerCase()
                        .replace("-", "") + "r1");
        kpg.initialize(paramSpec);
        KeyPair kp = kpg.generateKeyPair();
        ECParameterSpec params = ((ECPublicKey) kp.getPublic()).getParams();
               
        ECPoint ecPoint = new ECPoint(xCoord, yCoord);
        ECPublicKeySpec keySpec = new ECPublicKeySpec(ecPoint, params);
        KeyFactory kf = KeyFactory.getInstance("EC");
        
        return (ECPublicKey) kf.generatePublic(keySpec);
        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException | InvalidKeySpecException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

}
/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.util;

import javax.crypto.spec.SecretKeySpec;

import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.jws.crypto.EcdsaJwsSigner;
import de.fhk.jwx.jws.crypto.EcdsaJwsVerifier;
import de.fhk.jwx.jws.crypto.HmacJwsSigner;
import de.fhk.jwx.jws.crypto.HmacJwsVerifier;
import de.fhk.jwx.jws.crypto.JwsSigner;
import de.fhk.jwx.jws.crypto.JwsVerifier;
import de.fhk.jwx.jws.crypto.RsaJwsSigner;
import de.fhk.jwx.jws.crypto.RsaJwsVerifier;
import de.fhk.jwx.jws.crypto.RsassapssJwsSigner;
import de.fhk.jwx.jws.crypto.RsassapssJwsVerifier;

public class JwsSignerAssigning {
    public static JwsSigner getJwsSigner(JwkKey jwk, String alg) 
             {
        String signatureAlg = jwk.getAlgorithm() == null ? alg
                : jwk.getAlgorithm();
        JwsSigner signer = null;
        if (JwkKey.KEY_TYPE_OCTET.equals(jwk.getKeyType())) {
            SecretKeySpec key = new SecretKeySpec(
                    JoseUtils.base64URLTobytes(String.valueOf(jwk
                            .getProperty(JwkKey.OCTET_KEY_VALUE))), "AES");
            signer = new HmacJwsSigner(key);
        } else if (signatureAlg.startsWith("ES")) {
            signer = new EcdsaJwsSigner(JwkUtils.convertJwkToECPrivateKey(jwk));
        } else if (signatureAlg.startsWith("RS")) {
            signer = new RsaJwsSigner(JwkUtils.convertJwkToRSAPrivateKey(jwk));
        } else if (signatureAlg.startsWith("PS")) {
            signer = new RsassapssJwsSigner(JwkUtils.convertJwkToRSAPrivateKey(jwk));
        } else throw new SecurityException("no signer detected!");
        return signer;
    }

    public static JwsVerifier getJwsVerifier(JwkKey jwk, String alg)
            {
        String signatureAlg = jwk.getAlgorithm() == null ? alg
                : jwk.getAlgorithm();
        JwsVerifier validator = null;
        if (JwkKey.KEY_TYPE_OCTET.equals(jwk.getKeyType())) {
            SecretKeySpec key = new SecretKeySpec(
                    JoseUtils.base64URLTobytes((String) jwk
                            .getProperty(JwkKey.OCTET_KEY_VALUE)), "AES");
            validator = new HmacJwsVerifier(key);
        } else if (signatureAlg.startsWith("ES")) {
            validator = new EcdsaJwsVerifier(JwkUtils.convertJwkToECPublicKey(jwk));
        }else if (signatureAlg.startsWith("PS")) {
            validator = new RsassapssJwsVerifier(JwkUtils.convertJwkToRSAPublicKey(jwk));
        }else if (signatureAlg.startsWith("RS")) {
            validator = new RsaJwsVerifier(JwkUtils.convertJwkToRSAPublicKey(jwk));
        } else throw new SecurityException("no verifier detected!");
        return validator;
    }

}

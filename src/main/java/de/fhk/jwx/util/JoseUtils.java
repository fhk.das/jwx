/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.util;

import java.text.ParseException;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.fhk.jwx.JoseHeaders;
import de.fhk.jwx.jwe.JweProtectedHeader;
import de.fhk.jwx.jws.JwsHeaders;
import de.fhk.jwx.jws.JwsProtectedHeader;

public class JoseUtils {
    public static String stringToBase64(String input) {
        Encoder encoder = Base64.getEncoder();
        return new String(encoder.encode(new String(input).getBytes()));
    }

    public static byte[] bytesToBase64(byte[] input) {
        Encoder encoder = Base64.getEncoder();
        return encoder.encode(input);
    }

    public static String stringToBase64URL(String input) {
        Encoder encoder = Base64.getEncoder();
        String encodedInput = new String(encoder.encode(new String(input)
                .getBytes()));
        encodedInput = encodedInput.split("=")[0];
        encodedInput = encodedInput.replaceAll("\\+", "-");
        encodedInput = encodedInput.replaceAll("/", "_");
        return encodedInput;
    }

    public static byte[] bytesToBase64URL(byte[] input) {
        Encoder encoder = Base64.getEncoder();
        String encodedInput = new String(encoder.encode(input));
        encodedInput = encodedInput.split("=")[0];
        encodedInput = encodedInput.replaceAll("\\+", "-");
        encodedInput = encodedInput.replaceAll("/", "_");
        return encodedInput.getBytes();
    }

    public static String bytesToBase64URLString(byte[] input) {
        Encoder encoder = Base64.getEncoder();
        String encodedInput = new String(encoder.encode(input));
        encodedInput = encodedInput.split("=")[0];
        encodedInput = encodedInput.replaceAll("\\+", "-");
        encodedInput = encodedInput.replaceAll("/", "_");
        return encodedInput;
    }

    public static String base64ToString(String input) {
        Decoder decoder = Base64.getDecoder();
        return new String(decoder.decode(input.getBytes()));
    }

    public static byte[] base64ToBytes(byte[] input) {
        Decoder decoder = Base64.getDecoder();
        return decoder.decode(input);
    }

    public static String base64URLToString(String input){
        Decoder decoder = Base64.getDecoder();
        String output = new String(input);
        output = output.replaceAll("-", "\\+");
        output = output.replaceAll("_", "/");
        switch (output.length() % 4) {
        case 0:
            break; // No pad chars in this case
        case 2:
            output += "==";
            break; // Two pad chars
        case 3:
            output += "=";
            break; // One pad char
        default:
				try {
					throw new ParseException("Illegal base64url string!", 0);
				} catch (ParseException e) {
					e.printStackTrace();
				}
        }

        return new String(decoder.decode(output.getBytes()));
    }

    public static byte[] base64URLTobytes(String input){
        Decoder decoder = Base64.getDecoder();
        String output = new String(input);
        output = output.replaceAll("-", "\\+");
        output = output.replaceAll("_", "/");
        switch (output.length() % 4) {
        case 0:
            break; // No pad chars in this case
        case 2:
            output += "==";
            break; // Two pad chars
        case 3:
            output += "=";
            break; // One pad char
        default:
            try {
                throw new ParseException("Illegal base64url string!", 0);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return decoder.decode(output.getBytes());
    }

    public static boolean validateCriticalHeaders(JoseHeaders headers) {

        List<String> criticalList = (List<String>) headers.getHeader(JoseHeaders.HEADER_CRITICAL);
        
        if (criticalList == null) {
            return true;
        }

        if (criticalList.isEmpty() || detectDoubleEntry(criticalList)
                || criticalList.contains(JoseHeaders.defindedHeaderParametersAsList())) {
            return false;
        }
        for (String key : criticalList) {
            if (!headers.contains(key))
                return false;
        }
        return true;
    }
    
    public static boolean detectDoubleEntry(List<String> list) {
        Set<Object> inputSet = new HashSet<Object>(list);
        return list.size() > inputSet.size();
    }
    
    public static byte[] getByteArrayFromIntArray(int[] intArray) {
        byte[] byteArray = new byte[intArray.length];
        for (int i = 0; i < intArray.length; i++) {
            byteArray[i] = (byte) intArray[i];
        }
        return byteArray;
    }
    
    public static int[] getIntArrayFromByteArray(byte[] byteArray) {
    	int[] intArray = new int[byteArray.length];
    	for (int i = 0; i < byteArray.length; i++) {
    		intArray[i] = (int) byteArray[i];
    		if (intArray[i] < 0) {
    			intArray[i] = 256 + intArray[i];
			} 
    	}
    	return intArray;
    }
    
	public static void sysoIntArray(int[] array) {
		System.out.print("[");
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i]);
			if (i < array.length - 1) {
				System.out.print(", ");
			}
		}
		System.out.println("]");
	}
}

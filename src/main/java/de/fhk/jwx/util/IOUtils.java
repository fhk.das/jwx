/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class IOUtils {
    public static final int BUFFER = 1024;

    public static String streamToString(InputStream in)
        throws IOException {

        StringBuilder sb = new StringBuilder(1024);

        for (int i = in.read(); i != -1; i = in.read()) {
            sb.append((char) i);
        }

        in.close();

        return sb.toString();
    }

    public static byte[] streamToBytes(InputStream in) throws IOException {
        int i = in.available();
        if (i < BUFFER) {
            i = BUFFER;
        }
        ByteArrayOutputStream bos = new ByteArrayOutputStream(i);
        int available = in.available();
        int bufferSize = BUFFER;
        if (available > bufferSize) {
            bufferSize = available;
        }
        final byte[] buffer = new byte[bufferSize];
        int n = 0;
        n = in.read(buffer);
        @SuppressWarnings("unused")
        int counter = 0;
        while (-1 != n) {
            if (n == 0) {
                throw new IOException();
            }
            bos.write(buffer, 0, n);
            counter += n;
            n = in.read(buffer);
        }
        in.close();
        return bos.toByteArray();
    }

}
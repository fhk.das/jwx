/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwe;

import de.fhk.jwx.jwk.JwkKey;

public class JweRecipient
{
    private JwePerRecipientUnprotectedHeader header;
    private JwePerRecipientUnprotectedHeader mergedHeader;
    private JwkKey encryptionKey;
    private byte[] encryptedKey;

    public JweRecipient(JwePerRecipientUnprotectedHeader header, JwkKey encryptionKey)
    {
        this.header = header;
        this.encryptionKey = encryptionKey;
    }

    public JwePerRecipientUnprotectedHeader getHeader()
    {
        return header;
    }

    public JwePerRecipientUnprotectedHeader getMergedHeader()
    {
        return mergedHeader;
    }

    JwkKey getEncryptionKey()
    {
        return encryptionKey;
    }

    byte[] getEncryptedKey()
    {
        return encryptedKey;
    }

    public void setMergedHeader(JwePerRecipientUnprotectedHeader mergedHeader)
    {
        this.mergedHeader = mergedHeader;
    }

    void setEncryptedKey(byte[] encryptedKey)
    {
        this.encryptedKey = encryptedKey;
    }
}

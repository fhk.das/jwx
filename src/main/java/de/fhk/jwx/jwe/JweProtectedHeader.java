/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwe;

import java.util.ArrayList;

public class JweProtectedHeader extends JweHeaders
{
    public JweProtectedHeader()
    {
        super();
    }

    /**
     * @param type
     *            zip algorithm type
     */
    public void setZipAlgorithm(String type)
    {
        setHeader(JWE_HEADER_ZIP_ALGORITHM, type);
    }

    /**
     * @return zip algorithm
     */
    public String getZipAlgorithm()
    {
        return (String) getHeader(JWE_HEADER_ZIP_ALGORITHM);
    }

    public void setCriticalHeader(ArrayList<String> arrayList)
    {
        if (arrayList.size() == 0)
        {
            throw new SecurityException("The critical header array list must not be empty!");
        } else
        {
            setHeader(HEADER_CRITICAL, arrayList);
        }
    }

    public void addCriticalHeaderValue(String criticalHeaderValue)
    {
        if (criticalHeaderValue == null || criticalHeaderValue.length() == 0)
        {
            throw new SecurityException("The critical header value must not be null or empty!");
        }

        if (getHeader(HEADER_CRITICAL) == null)
        {
            ArrayList<String> arrayList = new ArrayList<String>();
            arrayList.add(criticalHeaderValue);
            setCriticalHeader(arrayList);
        } else
        {
            @SuppressWarnings("unchecked")
            ArrayList<String> arrayList = ((ArrayList<String>) getHeader(HEADER_CRITICAL));
            arrayList.add(criticalHeaderValue);
            setCriticalHeader(arrayList);
        }
    }

    @SuppressWarnings("unchecked")
    public ArrayList<String> getCriticalHeaders()
    {
        return (ArrayList<String>) getHeader(HEADER_CRITICAL);
    }
}

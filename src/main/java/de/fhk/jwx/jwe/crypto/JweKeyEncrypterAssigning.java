/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwe.crypto;

import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jwe.JweProtectedHeader;
import de.fhk.jwx.jwe.JweRecipient;

public class JweKeyEncrypterAssigning
{
    public static AbstractJweKeyEncrypter getJweKeyEncrypter(final JweRecipient recipient, int recipientCount, JweProtectedHeader protectedHeader,
            final JweCookbookBypass cookbookBypass)
    {
        final String alg = recipient.getMergedHeader().getAlgorithm();

        if (JwaAlgorithms.isKeyManagementAesWrap(alg))
        {
            return getAesKeyWrap(recipient);

        } else if (JwaAlgorithms.isKeyManagementRsa(alg))
        {
            return getRsaKeyEncrypter(alg, cookbookBypass);

        } else if (JwaAlgorithms.isKeyManagementPbes(alg))
        {
            if (protectedHeader == null)
            {
                protectedHeader = new JweProtectedHeader();
            }
            return getPbesHsAesKeyEncrypter(recipient, recipientCount, protectedHeader, cookbookBypass);

        } else if (JwaAlgorithms.isKeyManagementGcmWrap(alg))
        {
            return getAesGcmKeyWrap(recipient, recipientCount, protectedHeader, cookbookBypass);

        } else if (JwaAlgorithms.isKeyManagementEcdh(alg))
        {
            if (protectedHeader == null)
            {
                protectedHeader = new JweProtectedHeader();
            }
            return getEcdhEsKeyEncrypter(recipient, recipientCount, protectedHeader, cookbookBypass);
            
        } else
        {
            final String message = recipient.getMergedHeader().getAlgorithm() + " is not a Key Management Algorithm";
            throw new SecurityException(message);
        }
    }

    private static RsaKeyEncrypter getRsaKeyEncrypter(String algHeaderValue, JweCookbookBypass cookbookBypass)
    {
        return new RsaKeyEncrypter(algHeaderValue, cookbookBypass);
    }

    private static AesKeyWrap getAesKeyWrap(JweRecipient recipient)
    {
        return new AesKeyWrap(recipient);
    }

    private static EcdhEsKeyEncrypter getEcdhEsKeyEncrypter(final JweRecipient recipient, final int recipientCount,
            final JweProtectedHeader protectedHeader, final JweCookbookBypass cookbookBypass)
    {
        return new EcdhEsKeyEncrypter(recipient, recipientCount, protectedHeader, cookbookBypass);
    }

    private static PbesAesKeyEncrypter getPbesHsAesKeyEncrypter(final JweRecipient recipient, final int recipientCount,
            final JweProtectedHeader protectedHeader, final JweCookbookBypass cookbookBypass)
    {
        return new PbesAesKeyEncrypter(recipient, recipientCount, protectedHeader, cookbookBypass);
    }

    private static AesGcmKeyWrap getAesGcmKeyWrap(final JweRecipient recipient, final int recipientCount, final JweProtectedHeader protectedHeader,
            final JweCookbookBypass cookbookBypass)
    {
        return new AesGcmKeyWrap(recipient, recipientCount, protectedHeader, cookbookBypass);
    }
}

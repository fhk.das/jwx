/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwe.crypto;

import java.security.Key;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.spec.IvParameterSpec;

import de.fhk.jwx.jwe.JweProtectedHeader;
import de.fhk.jwx.util.JoseUtils;

public abstract class AbstractJweEncrypter
{
    private byte[] ciphertext;
    private byte[] authenticationTag;
    private AlgorithmParameterSpec spec;

    public abstract void encryptPayload(byte[] payload, Key key, String aad, JweProtectedHeader protectedHeader);

    public abstract void generateIV();
 
    String generateAadEncryptionParameter(JweProtectedHeader protectedHeader, String aad)
    {
        String pH = (protectedHeader == null) ? "" : protectedHeader.getBase64URLEncodedHeaders();

        if (aad != null && aad.length() > 0)
            return pH + "." + JoseUtils.stringToBase64URL(aad);
        else
            return pH;
    }

    public abstract byte[] getIV();

    public abstract boolean setIV(byte[] iv);

    void setSpec(AlgorithmParameterSpec spec)
    {
        this.spec = spec;
    }

    AlgorithmParameterSpec getSpec()
    {
        return spec;
    }

    public byte[] getCiphertext()
    {
        return ciphertext;
    }

    void setCiphertext(byte[] ciphertext)
    {
        this.ciphertext = ciphertext;
    }

    public byte[] getAuthenticationTag()
    {
        return authenticationTag;
    }

    void setAuthenticationTag(byte[] authenticationTag)
    {
        this.authenticationTag = authenticationTag;
    }
}
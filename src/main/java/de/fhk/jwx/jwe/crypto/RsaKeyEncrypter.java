/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwe.crypto;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.interfaces.RSAPublicKey;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.util.JwkUtils;

public class RsaKeyEncrypter extends AbstractJweKeyEncrypter{
    
    private String instance;

	public RsaKeyEncrypter(String algHeaderValue, JweCookbookBypass cookbookBypass) {

		if (algHeaderValue.equals(JwaAlgorithms.RSA1_5.getSpecName())){
			//"RSA/ECB/PKCS1Padding";
			instance = JwaAlgorithms.RSA1_5.getJavaName();
		}
		else if (algHeaderValue.equals(JwaAlgorithms.RSA_OAEP.getSpecName())){
			//"RSA/ECB/OAEPWithSHA-1AndMGF1Padding";
			instance = JwaAlgorithms.RSA_OAEP.getJavaName();
		}
		else if (algHeaderValue.equals(JwaAlgorithms.RSA_OAEP_256.getSpecName())){
			//"RSA/ECB/OAEPWithSHA-256AndMGF1Padding";
			instance = JwaAlgorithms.RSA_OAEP_256.getJavaName();
		}		
	}

	@Override
	public byte[] encryptKey(Key sharedSymmetricKey, JwkKey recipientKey) {

	    Key keyEncryptionKey = (RSAPublicKey) JwkUtils.convertJwkToRSAPublicKey(recipientKey);
	    
		byte[] temp = null;
		
		Cipher encryptedKey = null;
		
		try {
			encryptedKey = Cipher.getInstance(instance);
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			e.printStackTrace();
		}
		
		SecureRandom random = new SecureRandom();
		
	     try {
			encryptedKey.init(Cipher.ENCRYPT_MODE, keyEncryptionKey, random);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		}
	     
		try {
			temp = encryptedKey.doFinal(sharedSymmetricKey.getEncoded());
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			e.printStackTrace();
		}
	
	    return temp;
	}
}

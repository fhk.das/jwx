/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwe.crypto;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.util.JwkUtils;

public class RsaKeyDecrypter extends AbstractJweKeyDecrypter
{
    private String instance;

    public RsaKeyDecrypter(String algHeaderValue)
    {
        if (algHeaderValue.equals(JwaAlgorithms.RSA1_5.getSpecName()))
        {
            instance = "RSA/ECB/PKCS1Padding";

        } else if (algHeaderValue.equals(JwaAlgorithms.RSA_OAEP.getSpecName()))
        {
            instance = "RSA/ECB/OAEPWithSHA-1AndMGF1Padding";

        } else if (algHeaderValue.equals(JwaAlgorithms.RSA_OAEP_256.getSpecName()))
        {
            instance = "RSA/ECB/OAEPWithSHA-256AndMGF1Padding";
        }
    }

    @Override
    public SecretKeySpec decryptKey(byte[] encryptedSecretKey, JwkKey key)
    {
        byte[] decryptedCEC = null;
        Cipher encryptedKey = null;

        try
        {
            encryptedKey = Cipher.getInstance(instance);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e)
        {
            e.printStackTrace();
        }

        Key privateKey = JwkUtils.convertJwkToRSAPrivateKey(key);

        try
        {
            encryptedKey.init(Cipher.DECRYPT_MODE, privateKey);
        } catch (InvalidKeyException e)
        {
            e.printStackTrace();
        }

        try
        {
            decryptedCEC = encryptedKey.doFinal(encryptedSecretKey);
        } catch (IllegalBlockSizeException | BadPaddingException e)
        {
            e.printStackTrace();
        }

        return new SecretKeySpec(decryptedCEC, "AES");
    }
}

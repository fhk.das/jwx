/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwe.crypto;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;

import de.fhk.jwx.jwe.JoseException;
import de.fhk.jwx.jwe.JweProtectedHeader;
import de.fhk.jwx.util.JoseUtils;

class AesGcmEncrypter extends AbstractJweEncrypter
{
    private final String algorithm = "AES";
    private final String mode = "GCM";
    private final String padding = "NoPadding";
    private final String transformation = algorithm + "/" + mode + "/" + padding;
    private int determinedKeyLength;
    private Cipher encryptCipher = null;

    public AesGcmEncrypter(String encHeaderValue)
    {
        if (encHeaderValue.equals("A128GCM")){
            determinedKeyLength = 128;
        }
        else if (encHeaderValue.equals("A192GCM")){
            determinedKeyLength = 192;
        }
        else if (encHeaderValue.equals("A256GCM")){
            determinedKeyLength = 256;
        }
    }

    public void encryptPayload(byte[] payload, Key key, String aad, JweProtectedHeader protectedHeader)
    {

        int actualKeySize = key.getEncoded().length*8;
        if (actualKeySize != determinedKeyLength){
            throw new JoseException("Illegal key size " + actualKeySize + " bit.");
        }
        
        try
        {
            encryptCipher = Cipher.getInstance(transformation);

            // TODO provider dependent?
            // encryptCipher = Cipher.getInstance(transformation, provider);

            generateIV();

            // Initialize the Cipher Object

            encryptCipher.init(Cipher.ENCRYPT_MODE, key, getSpec());

            String aadEncryptionParameter = generateAadEncryptionParameter(protectedHeader, aad);
            encryptCipher.updateAAD(aadEncryptionParameter.getBytes());

            byte[] gcmOutput = encryptCipher.doFinal(payload);

            setCiphertext(new byte[gcmOutput.length - 16]);
            System.arraycopy(gcmOutput, 0, getCiphertext(), 0, gcmOutput.length - 16);
            setAuthenticationTag(new byte[16]);

            System.arraycopy(gcmOutput, gcmOutput.length - 16, getAuthenticationTag(), 0, 16);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException | InvalidKeyException
                | InvalidAlgorithmParameterException e1)
        {
            e1.printStackTrace();
        }
    }

    public void generateIV()
    {
        if (getSpec() == null)
        {
            SecureRandom random = new SecureRandom();
            byte iv[] = new byte[12];
            random.nextBytes(iv);
            setSpec(new GCMParameterSpec(128, iv));
        }
    }

    public byte[] getIV()
    {
        if (getSpec() == null)
            generateIV();
        return ((GCMParameterSpec) getSpec()).getIV();
    }

    public boolean setIV(byte[] iv)
    {
        if (getSpec() == null)
        {
            setSpec(new GCMParameterSpec(128, iv));
            return true;
        }
        return false;
    }
}

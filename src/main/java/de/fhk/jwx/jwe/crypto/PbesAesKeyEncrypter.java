/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwe.crypto;

import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.bouncycastle.pqc.math.linearalgebra.ByteUtils;

import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jwe.JweHeaders;
import de.fhk.jwx.jwe.JwePerRecipientUnprotectedHeader;
import de.fhk.jwx.jwe.JweProtectedHeader;
import de.fhk.jwx.jwe.JweRecipient;
import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.util.JoseUtils;
import de.fhk.jwx.util.JwkUtils;

class PbesAesKeyEncrypter extends AbstractJweKeyEncrypter
{
    private String keyFactoryInstance;
    private String keyEncryptionInstance;
    private JwePerRecipientUnprotectedHeader mergedHeaders;
    private int derivedKeyLength;
    private JweCookbookBypass cookbookBypass;
    private JweProtectedHeader protectedHeader;
    private JweRecipient recipient;
    private int recipientCount;

    public PbesAesKeyEncrypter(JweRecipient recipient, int recipientCount, JweProtectedHeader protectedHeader, JweCookbookBypass cookbookBypass)
    {
        String algHeaderValue = recipient.getMergedHeader().getAlgorithm();

        if (algHeaderValue.equals(JwaAlgorithms.PBES2_HS512_A256KW.getSpecName()))
        {
            keyFactoryInstance = "PBKDF2WithHmacSHA512";
            keyEncryptionInstance = JwaAlgorithms.AES_256_KW.getSpecName();
            derivedKeyLength = 32 * 8;
            
        } else if (algHeaderValue.equals(JwaAlgorithms.PBES2_HS384_A192KW.getSpecName()))
        {
            keyFactoryInstance = "PBKDF2WithHmacSHA384";
            keyEncryptionInstance = JwaAlgorithms.AES_192_KW.getSpecName();
            derivedKeyLength = 24 * 8;
            
        } else if (algHeaderValue.equals(JwaAlgorithms.PBES2_HS256_A128KW.getSpecName()))
        {
            keyFactoryInstance = "PBKDF2WithHmacSHA256";
            keyEncryptionInstance = JwaAlgorithms.AES_128_KW.getSpecName();
            derivedKeyLength = 16 * 8;
        }

        this.mergedHeaders = recipient.getMergedHeader();
        this.cookbookBypass = cookbookBypass;
        this.protectedHeader = protectedHeader;
        this.recipient = recipient;
        this.recipientCount = recipientCount;
    }

    @Override
    public byte[] encryptKey(Key sharedSymmetricKey, JwkKey recipientKey)
    {
        Key keyEncryptionKey = (SecretKey) JwkUtils.convertJwkToSecretKeySpec(recipientKey);

        byte[] saltInput = null;

        // PBES2 Salt Input
        if (cookbookBypass != null)
        {
            if (cookbookBypass.getBypassPbes2Salt() != null)
            {
                saltInput = JoseUtils.base64URLTobytes(cookbookBypass.getBypassPbes2Salt());
            }
        } else
        {
            SecureRandom salt = new SecureRandom();
            byte[] octets = new byte[16];
            salt.nextBytes(octets);
            saltInput = octets;
        }

        // A Salt Input value containing 8 or more octets MUST be used.
        if (saltInput.length < 8)
        {
            throw new SecurityException("A Salt Input value containing 8 or more octets MUST be used.");
        }

        // salt value computation, (UTF8(Alg) || 0x00 || Salt Input)
        byte[] part1 = ByteUtils.concatenate(mergedHeaders.getAlgorithm().getBytes(), new byte[]
        { 0x00 });
        byte[] saltValue = ByteUtils.concatenate(part1, saltInput);

        // PBKDF2 iteration count
        // TODO A minimum iteration count of 1000 is RECOMMENDED.
        Integer iterationCount = null;
        if (cookbookBypass != null)
        {
            if (cookbookBypass.getBypassPbes2IterationCount() != null)
            {
                iterationCount = cookbookBypass.getBypassPbes2IterationCount();
            }
        } else
        {
            iterationCount = 10000;
        }

        // A minimum iteration count of 1000 is RECOMMENDED.
        if (iterationCount < 1000)
        {
            throw new SecurityException("A minimum iteration count of 1000 is RECOMMENDED.");
        }

        // distinction of cases
        if (recipientCount == 1)
        {
            if (protectedHeader == null)
            {
                protectedHeader = new JweProtectedHeader();
            }

            protectedHeader.setPbesSalt(JoseUtils.bytesToBase64URLString(saltInput));
            protectedHeader.setPbesIterationCount(iterationCount);
            // sort for cookbook result
            if (cookbookBypass != null)
            {
                String enc = protectedHeader.getContentEncryptionAlgorithm();
                String cty = protectedHeader.getContentType();
                protectedHeader.removeHeaderEntry(JweHeaders.JWE_HEADER_CONTENT_ENC_ALGORITHM);
                protectedHeader.removeHeaderEntry(JweHeaders.HEADER_CONTENT_TYPE);
                protectedHeader.setContentType(cty);
                protectedHeader.setContentEncryptionAlgorithm(enc);
            }

        } else
        {
            recipient.getHeader().setPbesSalt(JoseUtils.bytesToBase64URLString(saltInput));
            recipient.getHeader().setPbesIterationCount(iterationCount);
        }

        SecretKeyFactory factory = null;
        try
        {
            factory = SecretKeyFactory.getInstance(keyFactoryInstance);
        } catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }

        KeySpec keySpec = new PBEKeySpec(new String(keyEncryptionKey.getEncoded()).toCharArray(), saltValue, iterationCount, derivedKeyLength);

        Key kek = null;

        try
        {
            kek = factory.generateSecret(keySpec);
        } catch (InvalidKeySpecException e)
        {
            e.printStackTrace();
        }

        JwePerRecipientUnprotectedHeader keyWrapHeader = new JwePerRecipientUnprotectedHeader();
        keyWrapHeader.setHeader(JweHeaders.HEADER_ALGORITHM, keyEncryptionInstance);
        JweRecipient keywrapRecipient = new JweRecipient(null, null);
        keywrapRecipient.setMergedHeader(keyWrapHeader);

        // encrypt cek with derivated key
        AbstractJweKeyEncrypter keyEncrypter = JweKeyEncrypterAssigning.getJweKeyEncrypter(keywrapRecipient, 1, null, this.cookbookBypass);

        String jwk = "{" + "\"k\":\"" + JoseUtils.bytesToBase64URLString(kek.getEncoded()) + "\"}\"";
        JwkKey derKey = new JwkKey(jwk);

        byte[] encryptedKey = keyEncrypter.encryptKey(sharedSymmetricKey, derKey);

        return encryptedKey;
    }
}

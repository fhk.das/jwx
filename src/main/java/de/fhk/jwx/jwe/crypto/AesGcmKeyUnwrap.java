/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwe.crypto;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.pqc.math.linearalgebra.ByteUtils;

import de.fhk.jwx.jwe.JweRecipient;
import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.util.JoseUtils;
import de.fhk.jwx.util.JwkUtils;

public class AesGcmKeyUnwrap extends AbstractJweKeyDecrypter
{

    AlgorithmParameterSpec spec;
    private JweRecipient recipient;

    public AesGcmKeyUnwrap(JweRecipient recipient)
    {
        this.recipient = recipient;
    }

    public SecretKeySpec decryptKey(byte[] encryptedSharedSymmetricKey, JwkKey key)
    {

        Cipher decryptCipher = null;
        
        try
        {
            decryptCipher = Cipher.getInstance("AES/GCM/NoPadding");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e)
        {
            e.printStackTrace();
        }
        
        // Content Encryption Key
        byte[] iv = JoseUtils.base64URLTobytes((String) recipient.getMergedHeader().getGcmKwIv());

        // Initialization vector
        this.spec = new GCMParameterSpec(128, iv);

        SecretKeySpec keyDecryptionKey = (SecretKeySpec) JwkUtils.convertJwkToSecretKeySpec(key);

        // Initialize the Cipher Object
        try
        {
            decryptCipher.init(Cipher.UNWRAP_MODE, keyDecryptionKey, spec);
        } catch (InvalidKeyException | InvalidAlgorithmParameterException e)
        {
            e.printStackTrace();
        }

        byte[] gcmOutput = null;
        byte[] tag = JoseUtils.base64URLTobytes((String) recipient.getMergedHeader().getGcmKwtag());

        try
        {
            gcmOutput = decryptCipher.unwrap(ByteUtils.concatenate(encryptedSharedSymmetricKey, tag), "", Cipher.SECRET_KEY).getEncoded();
        } catch (InvalidKeyException | NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }

        return new SecretKeySpec(gcmOutput, "AES");
    }
}

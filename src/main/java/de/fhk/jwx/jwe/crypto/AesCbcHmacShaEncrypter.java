/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwe.crypto;

import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.pqc.math.linearalgebra.ByteUtils;

import de.fhk.jwx.jwe.JoseException;
import de.fhk.jwx.jwe.JweProtectedHeader;

class AesCbcHmacShaEncrypter extends AbstractJweEncrypter
{
    private final String algorithm = "AES"; // NIST.800-38A
    private final String mode = "CBC"; // NIST.800-38A
    private int determinedKeyLength;

    // TODO PKCS7 Padding
    private final String padding = "PKCS5Padding"; // RFC5652
    private final String transformation = algorithm + "/" + mode + "/" + padding;
    private String hMac = null;

    private final int ENC_KEY_LEN;

    Cipher encryptCipher = null;

    public AesCbcHmacShaEncrypter(String encHeaderValue)
    {
        switch (encHeaderValue)
        {
        case "A128CBC-HS256":
            ENC_KEY_LEN = 16;
            determinedKeyLength = 256;
            this.hMac = "HmacSHA256";
            break;
        case "A192CBC-HS384":
            ENC_KEY_LEN = 24;
            determinedKeyLength = 384;
            this.hMac = "HmacSHA384";
            break;
        case "A256CBC-HS512":
            ENC_KEY_LEN = 32;
            determinedKeyLength = 512;
            this.hMac = "HmacSHA512";
            break;
        default:
            throw new SecurityException("Not an AES CBC Algorithm");
        }
    }

    public void encryptPayload(byte[] payload, Key key, String aad, JweProtectedHeader protectedHeader)
    {
        int actualKeySize = key.getEncoded().length*8;
        if (actualKeySize != determinedKeyLength){
            throw new JoseException("Illegal key size " + actualKeySize + " bit.");
        }
        
        try
        {
            encryptCipher = Cipher.getInstance(transformation);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e)
        {
            e.printStackTrace();
        }

        byte[] actualCek = new byte[ENC_KEY_LEN];
        System.arraycopy(key.getEncoded(), ENC_KEY_LEN, actualCek, 0, ENC_KEY_LEN);

        // Content Encryption Key
        SecretKey cekSpec = new SecretKeySpec(actualCek, "AES");
        
        // Initialization vector
        this.generateIV();

        // Initialize the Cipher Object
        try
        {
            encryptCipher.init(Cipher.ENCRYPT_MODE, cekSpec, getSpec());
        } catch (InvalidKeyException | InvalidAlgorithmParameterException e)
        {
            e.printStackTrace();
        }

        byte[] cipherText = null;
        try
        {
            cipherText = encryptCipher.doFinal(payload);
        } catch (IllegalBlockSizeException | BadPaddingException e)
        {
            e.printStackTrace();
        }

        setCiphertext(cipherText);

        authenticateData(aad, cipherText, key, ((IvParameterSpec) getSpec()).getIV(), protectedHeader);
    }

    public void authenticateData(String aad, byte[] cipherText, Key key, byte[] iv, JweProtectedHeader protectedHeader)
    {
        String aadEncryptionParameter = generateAadEncryptionParameter(protectedHeader, aad);

        ByteBuffer byteBuffer = ByteBuffer.allocate(8);

        byteBuffer.putLong(aadEncryptionParameter.getBytes().length * 8);

        byte[] al = byteBuffer.array();

        // concatenate A || IV || E || AL

        byte[] partOne = ByteUtils.concatenate(aadEncryptionParameter.getBytes(), iv.clone());
        byte[] partTwo = ByteUtils.concatenate(cipherText.clone(), al.clone());
        byte[] macInput = ByteUtils.concatenate(partOne.clone(), partTwo.clone());

        // calculate MAC
        Mac mac = null;
        try
        {
            mac = Mac.getInstance(hMac);
        } catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        byte[] actualMacKey = new byte[ENC_KEY_LEN];

        // left half of key is Hmac Key
        System.arraycopy(key.getEncoded(), 0, actualMacKey, 0, ENC_KEY_LEN);
        try
        {
            mac.init(new SecretKeySpec(actualMacKey, ""));
        } catch (InvalidKeyException e)
        {
            e.printStackTrace();
        }

        byte[] doFinalValue = mac.doFinal(macInput);
        byte[] value = new byte[actualMacKey.length];
        for (int i = 0; i < actualMacKey.length; i++)
        {
            value[i] = doFinalValue[i];
        }

        setAuthenticationTag(value);
    }

    public void generateIV()
    {
        if (getSpec() == null)
        {
            SecureRandom random = new SecureRandom();
            byte iv[] = new byte[16];
            random.nextBytes(iv);
            setSpec(new IvParameterSpec(iv));
        }
    }

    public boolean setIV(byte[] iv)
    {
        if (getSpec() == null)
        {
            setSpec(new IvParameterSpec(iv));
            return true;
        }
        return false;
    }

    public byte[] getIV()
    {

        return ((IvParameterSpec) getSpec()).getIV();
    }

}

/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwe.crypto;

import java.security.Key;

import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jwe.JweHeaders;
import de.fhk.jwx.jwe.JwePerRecipientUnprotectedHeader;
import de.fhk.jwx.jwe.JweProtectedHeader;
import de.fhk.jwx.jwe.JweRecipient;
import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.util.JoseUtils;

class EcdhEsKeyEncrypter extends AbstractJweKeyEncrypter
{
    private final int recipientCount;
    private final JweRecipient recipient;
    private final JweCookbookBypass cookbookBypass;
    private final JweProtectedHeader protectedHeader;
    private String keyEncryptionInstance;

    public EcdhEsKeyEncrypter(JweRecipient recipient, int recipientCount, JweProtectedHeader protectedHeader, JweCookbookBypass cookbookBypass)
    {
        this.recipient = recipient;
        this.recipientCount = recipientCount;
        this.protectedHeader = protectedHeader;
        this.cookbookBypass = cookbookBypass;

        final String alg = recipient.getMergedHeader().getAlgorithm();

        if (alg.equals(JwaAlgorithms.ECDH_ES_AES256KW.getSpecName()))
        {
            keyEncryptionInstance = JwaAlgorithms.AES_256_KW.getSpecName();

        } else if (alg.equals(JwaAlgorithms.ECDH_ES_AES192KW.getSpecName()))
        {
            keyEncryptionInstance = JwaAlgorithms.AES_192_KW.getSpecName();

        } else if (alg.equals(JwaAlgorithms.ECDH_ES_AES128KW.getSpecName()))
        {
            keyEncryptionInstance = JwaAlgorithms.AES_128_KW.getSpecName();
        }
    }

    public byte[] encryptKey(Key sharedSymmetricKey, JwkKey publicKey)
    {
        final EcdhEsKeyAgreement keyAgreement = new EcdhEsKeyAgreement(this.recipient, this.recipientCount, this.protectedHeader, this.cookbookBypass);

        final byte[] derivedKey = keyAgreement.deriveKey(publicKey);
        final String jwk = "{" + "\"k\":\"" + JoseUtils.bytesToBase64URLString(derivedKey) + "\"}\"";
        final JwkKey derKey = new JwkKey(jwk);

        final JwePerRecipientUnprotectedHeader keyWrapHeader = new JwePerRecipientUnprotectedHeader();
        keyWrapHeader.setHeader(JweHeaders.HEADER_ALGORITHM, keyEncryptionInstance);
        final JweRecipient keyWrapRecipient = new JweRecipient(null, null);
        keyWrapRecipient.setMergedHeader(keyWrapHeader);

        final AbstractJweKeyEncrypter keyEncrypter = JweKeyEncrypterAssigning.getJweKeyEncrypter(keyWrapRecipient, recipientCount, protectedHeader,
                cookbookBypass);

        final byte[] encryptedKey = keyEncrypter.encryptKey(sharedSymmetricKey, derKey);

        return encryptedKey;
    }
}

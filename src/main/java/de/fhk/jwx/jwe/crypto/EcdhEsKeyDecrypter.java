/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwe.crypto;

import javax.crypto.spec.SecretKeySpec;

import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jwe.JwePerRecipientUnprotectedHeader;
import de.fhk.jwx.jwe.JweRecipient;
import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.util.JoseUtils;

class EcdhEsKeyDecrypter extends AbstractJweKeyDecrypter
{
    private int recipientCount;
    private JweRecipient recipient;
    private String keyDecryptionInstance;

    public EcdhEsKeyDecrypter(JweRecipient recipient)
    {

        this.recipient = recipient;

        String alg = recipient.getMergedHeader().getAlgorithm();

        if (alg.equals(JwaAlgorithms.ECDH_ES_AES256KW.getSpecName()))
        {
            keyDecryptionInstance = JwaAlgorithms.AES_256_KW.getSpecName();

        } else if (alg.equals(JwaAlgorithms.ECDH_ES_AES192KW.getSpecName()))
        {
            keyDecryptionInstance = JwaAlgorithms.AES_192_KW.getSpecName();

        } else if (alg.equals(JwaAlgorithms.ECDH_ES_AES128KW.getSpecName()))
        {
            keyDecryptionInstance = JwaAlgorithms.AES_128_KW.getSpecName();
        }
    }

    @Override
    public SecretKeySpec decryptKey(byte[] encryptedSharedSymmetricKey, JwkKey privateKey)
    {
        EcdhEsKeyAgreement keyAgreement = new EcdhEsKeyAgreement(this.recipient, this.recipientCount, null, null);

        byte[] derivedKey = keyAgreement.deriveKey(privateKey);

        JwePerRecipientUnprotectedHeader header = new JwePerRecipientUnprotectedHeader();
        header.setAlgorithm(keyDecryptionInstance);
        JweRecipient keyDecryptionRecipient = new JweRecipient(null, null);
        keyDecryptionRecipient.setMergedHeader(header);

        AbstractJweKeyDecrypter keyDecrypter = JweKeyDecrypterAssigning.getJweKeyDecrypter(keyDecryptionRecipient);

        String jwk = "{" + "\"kty\":\"oct\"," + "\"use\":\"enc\"," + "\"k\":\"" + JoseUtils.bytesToBase64URLString(derivedKey) + "\"}\"";

        JwkKey derJwkKey = new JwkKey(jwk);

        SecretKeySpec decryptedKey = keyDecrypter.decryptKey(encryptedSharedSymmetricKey, derJwkKey);

        return decryptedKey;
    }
}

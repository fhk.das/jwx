/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwe.crypto;

import de.fhk.jwx.jwa.JwaAlgorithms;

public class JweEncrypterAssigning
{
    public static AbstractJweEncrypter getJweEncrypter(String encHeaderValue)
    {
        if (JwaAlgorithms.isContentEncAlgCBC(encHeaderValue))
        {
            return new AesCbcHmacShaEncrypter(encHeaderValue);
        } else if (JwaAlgorithms.isContentEncAlgGCM(encHeaderValue))
        {
            return new AesGcmEncrypter(encHeaderValue);
        } else
            throw new SecurityException("Not a Content Encryption Algorithm");
    }
}
/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwe.crypto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.crypto.KeyAgreement;

import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;
import org.bouncycastle.pqc.math.linearalgebra.ByteUtils;

import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jwe.JweHeaders;
import de.fhk.jwx.jwe.JwePerRecipientUnprotectedHeader;
import de.fhk.jwx.jwe.JweProtectedHeader;
import de.fhk.jwx.jwe.JweRecipient;
import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.util.JoseUtils;
import de.fhk.jwx.util.JwkUtils;

public class EcdhEsKeyAgreement
{
    private int keydatalen;
    private int recipientCount;
    private JweCookbookBypass cookbookBypass;
    private JweProtectedHeader protectedHeader;
    private JwePerRecipientUnprotectedHeader mergedHeader;
    private JwePerRecipientUnprotectedHeader recipientHeader;

    public EcdhEsKeyAgreement(JweRecipient recipient, int recipientCount, JweProtectedHeader protectedHeader, JweCookbookBypass cookbookBypass)
    {
        this.mergedHeader = recipient.getMergedHeader();
        this.recipientHeader = recipient.getHeader();
        this.recipientCount = recipientCount;
        this.protectedHeader = protectedHeader;
        this.cookbookBypass = cookbookBypass;

        String enc = mergedHeader.getContentEncryptionAlgorithm();
        String alg = mergedHeader.getAlgorithm();

        // determine keydatalen
        if (alg.equals(JwaAlgorithms.ECDH_ES_AES256KW.getSpecName()))
        {
            keydatalen = 32;

        } else if (alg.equals(JwaAlgorithms.ECDH_ES_AES192KW.getSpecName()))
        {
            keydatalen = 24;

        } else if (alg.equals(JwaAlgorithms.ECDH_ES_AES128KW.getSpecName()))
        {
            keydatalen = 16;

        } else if (alg.equals(JwaAlgorithms.ECDH_ES.getSpecName()))
        {
            if (enc.equals(JwaAlgorithms.AES_256_CBC_HMAC_SHA_512.getSpecName()))
            {
                keydatalen = 64;

            } else if (enc.equals(JwaAlgorithms.AES_192_CBC_HMAC_SHA_384.getSpecName()))
            {
                keydatalen = 48;

            } else if (enc.equals(JwaAlgorithms.AES_256_GCM.getSpecName()) || enc.equals(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256.getSpecName()))
            {
                keydatalen = 32;

            } else if (enc.equals(JwaAlgorithms.AES_192_GCM.getSpecName()))
            {
                keydatalen = 24;

            } else if (enc.equals(JwaAlgorithms.AES_128_GCM.getSpecName()))
            {
                keydatalen = 16;
            }

        } else
        {
            throw new SecurityException("Could not dermine the value for \"keydatalen\"");
        }
    }

    /**
     * @param keyInput
     *            ECDH public key or ECDH private Key
     * @return derived Key
     */
    public byte[] deriveKey(JwkKey keyInput)
    {
        // 1. establish shared secret Z through the ECDH algorithm (keyagreement)
        // https://docs.oracle.com/javase/8/docs/api/javax/crypto/KeyAgreement.html

        ECPrivateKey privateKeyForDerivation = null;
        ECPublicKey publicKeyForDerivation;
        ECPublicKey publicKeyForHeader = null;
        KeyAgreement keyAgreement = null;

        try
        {
            keyAgreement = KeyAgreement.getInstance("ECDH", "BC");
        } catch (NoSuchAlgorithmException e2)
        {
            e2.printStackTrace();
        } catch (NoSuchProviderException e2)
        {
            e2.printStackTrace();
        }

        // generate key pair
        // cookbook bypass case
        if (mergedHeader.getEcdhEsEphemeralPublicKey() == null)
        {
            // keyA = recipients public key
            publicKeyForDerivation = (ECPublicKey) JwkUtils.convertJwkToECPublicKey(keyInput);

            if (cookbookBypass != null)
            {
                if (cookbookBypass.getBypassEcdhEphemeralPrivateKey() != null && cookbookBypass.getBypassEcdhEphemeralPublicKey() != null)
                {
                    privateKeyForDerivation = (ECPrivateKey) cookbookBypass.getBypassEcdhEphemeralPrivateKey();
                    publicKeyForHeader = (ECPublicKey) cookbookBypass.getBypassEcdhEphemeralPublicKey();
                }
            } else
            {
                KeyPair pair = generateKeyPair(keyInput.getCurveName());
                privateKeyForDerivation = (ECPrivateKey) pair.getPrivate();
                publicKeyForHeader = (ECPublicKey) pair.getPublic();
            }
        } else
        {
            // keyA = this. private key
            privateKeyForDerivation = JwkUtils.convertJwkToECPrivateKey(keyInput);
            @SuppressWarnings("unchecked")
            JwkKey key = new JwkKey((Map<String, Object>) mergedHeader.getEcdhEsEphemeralPublicKey());
            publicKeyForDerivation = (ECPublicKey) JwkUtils.convertJwkToECPublicKey(key);
        }

        try
        {
            keyAgreement.init(privateKeyForDerivation);
        } catch (InvalidKeyException e)
        {
            e.printStackTrace();
        }

        try
        {
            keyAgreement.doPhase(publicKeyForDerivation, true);
        } catch (InvalidKeyException | IllegalStateException e1)
        {
            e1.printStackTrace();
        }

        byte[] z = null;

        try
        {
            z = keyAgreement.generateSecret();
        } catch (IllegalStateException e)
        {
            e.printStackTrace();
        }

        // This is set to the number of bits in the desired output key. For
        // "ECDH-ES", this is length of the key used by the "enc" algorithm.
        int keydatalen = this.keydatalen;

        // 3. OtherInfo: A bit string of context-specific data (see Section
        // 5.8.1.2 for details).

        // Data is a variable-length string of zero or more octets
        byte[] algorithmIdData = new byte[0];

        // In the Direct Key Agreement case,
        // Data is set to the octets of the ASCII representation of the "enc"
        // Header Parameter value.
        if (mergedHeader.getAlgorithm().equals(JwaAlgorithms.ECDH_ES.getSpecName()))
        {
            algorithmIdData = mergedHeader.getContentEncryptionAlgorithm().getBytes();
        }

        // In the Key Agreement with Key Wrapping
        // case, Data is set to the octets of the ASCII representation of the
        // "alg" (algorithm) Header Parameter value.
        else
        {
            algorithmIdData = mergedHeader.getAlgorithm().getBytes();
        }

        // Datalen is a fixed-length, big-endian 32-bit counter that indicates
        // the
        // length (in octets) of Data.
        byte[] algorithmIdDatalen = bigEndian32BitCounter(algorithmIdData.length);

        // The AlgorithmID value is of the form Datalen || Data
        byte[] algorithmId = ByteUtils.concatenate(algorithmIdDatalen, algorithmIdData);

        // PartyUInfo
        // The PartyUInfo value is of the form Datalen || Data, where Data is
        // a variable-length string of zero or more octets, and Datalen is a
        // fixed-length, big-endian 32-bit counter that indicates the length
        // (in octets) of Data.
        byte[] partyUInfoDatalen = null;
        byte[] partyUInfoData = null;

        // If an "apu" (agreement PartyUInfo) Header
        // Parameter is present, Data is set to the result of base64url
        // decoding the "apu" value and Datalen is set to the number of
        // octets in Data. Otherwise, Datalen is set to 0 and Data is set to
        // the empty octet sequence.
        if (mergedHeader.getEcdhPartyUInfo() != null)
        {
            String partyUInfo = JoseUtils.base64URLToString(mergedHeader.getEcdhPartyUInfo());
            partyUInfoData = partyUInfo.getBytes();
            partyUInfoDatalen = bigEndian32BitCounter(partyUInfoData.length);
        } else
        {
            partyUInfoData = new byte[0];
            partyUInfoDatalen = bigEndian32BitCounter(0);
        }

        byte[] partyUInfo = ByteUtils.concatenate(partyUInfoDatalen, partyUInfoData);

        // PartyVInfo
        byte[] partyVInfoDatalen = null;
        byte[] partyVInfoData = null;

        if (mergedHeader.getEcdhPartyVInfo() != null)
        {
            String partyVInfo = JoseUtils.base64URLToString(mergedHeader.getEcdhPartyVInfo());
            partyVInfoData = partyVInfo.getBytes();
            partyVInfoDatalen = bigEndian32BitCounter(partyVInfoData.length);
        } else
        {
            partyVInfoData = new byte[0];
            partyVInfoDatalen = bigEndian32BitCounter(0);
        }

        byte[] partyVInfo = ByteUtils.concatenate(partyVInfoDatalen, partyVInfoData);

        // SuppPubInfo
        // This is set to the keydatalen represented as a 32-bit big-endian
        // integer.

        // 32-bit big-endian integer
        int suppPubInfo = keydatalen * 8;

        final byte[] suppPubInfo_byte = bigEndian32BitCounter(suppPubInfo);

        // SuppPrivInfo
        // This is set to the empty octet sequence.
        byte[] suppPrivInfo = new byte[0];

        // KDF
        ConcatenationKeyDerivationFunction derive = null;
        try
        {
            derive = new ConcatenationKeyDerivationFunction("SHA-256");
        } catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }

        // JoseUtils.sysoIntArray(JoseUtils.getIntArrayFromByteArray(z));
        // System.out.println(keydatalen*8);
        // JoseUtils.sysoIntArray(JoseUtils.getIntArrayFromByteArray(AlgorithmId));
        // JoseUtils.sysoIntArray(JoseUtils.getIntArrayFromByteArray(partyUInfo));
        // JoseUtils.sysoIntArray(JoseUtils.getIntArrayFromByteArray(partyVInfo));
        // JoseUtils.sysoIntArray(JoseUtils.getIntArrayFromByteArray(suppPubInfo_byte));
        // JoseUtils.sysoIntArray(JoseUtils.getIntArrayFromByteArray(suppPrivInfo));

        byte[] derivedKey = derive.concatKDF(z, keydatalen, algorithmId, partyUInfo, partyVInfo, suppPubInfo_byte, suppPrivInfo);

        if (mergedHeader.getEcdhEsEphemeralPublicKey() == null)
        {
            ECNamedCurveParameterSpec ecSpec = ECNamedCurveTable.getParameterSpec((String) keyInput.getCurveName());

            // mlen = d(log2q)/8e octets
            int coordinateByteLength = (int) Math.ceil(ecSpec.getCurve().getFieldSize() / 8d);

            LinkedHashMap<String, Object> epkEntry = new LinkedHashMap<String, Object>();

            epkEntry.put(JwkKey.KEY_TYPE, keyInput.getKeyType());
            epkEntry.put(JwkKey.EC_CURVE, keyInput.getCurveName());

            byte[] x = fieldElementToOctetStringConversion(publicKeyForHeader.getW().getAffineX(), coordinateByteLength);
            epkEntry.put("x", JoseUtils.bytesToBase64URLString(x));
            byte[] y = fieldElementToOctetStringConversion(publicKeyForHeader.getW().getAffineY(), coordinateByteLength);
            epkEntry.put("y", JoseUtils.bytesToBase64URLString(y));

            // distinction of cases
            if (recipientCount == 1)
            {
                protectedHeader.setEcdhEsEphemeralPublicKey(epkEntry);
                // sort for cookbook result
                if (cookbookBypass != null)
                {
                    String enc = protectedHeader.getContentEncryptionAlgorithm();
                    protectedHeader.removeHeaderEntry(JweHeaders.JWE_HEADER_CONTENT_ENC_ALGORITHM);
                    protectedHeader.setContentEncryptionAlgorithm(enc);
                }
            } else
            {
                recipientHeader.setEcdhEsEphemeralPublicKey(epkEntry);
            }
        }
        return derivedKey;
    }

    private static byte[] bigEndian32BitCounter(int length)
    {
        final ByteBuffer big_endian_32_bit_counter = ByteBuffer.allocate(4);
        big_endian_32_bit_counter.putInt(length);
        return big_endian_32_bit_counter.array();
    }

    private KeyPair generateKeyPair(String curveName)
    {
        ECNamedCurveParameterSpec ecSpec = ECNamedCurveTable.getParameterSpec((String) curveName);

        KeyPairGenerator g = null;

        try
        {
            g = KeyPairGenerator.getInstance("ECDSA", "BC");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e1)
        {
            e1.printStackTrace();
        }

        try
        {
            g.initialize(ecSpec, new SecureRandom());
        } catch (InvalidAlgorithmParameterException e1)
        {
            e1.printStackTrace();
        }

        return g.generateKeyPair();
    }

    /**
     * Field element conversion to octet string
     * 
     * https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40#
     * section-6.2.1.2 6.2.1.2. "x" (X Coordinate) Parameter
     * https://tools.ietf.org
     * /html/draft-ietf-jose-json-web-algorithms-40#section-6.2.1.3 6.2.1.3. "y"
     * (Y Coordinate) Parameter
     *
     * [SEC1] Standards for Efficient Cryptography Group,
     * "SEC 1: Elliptic Curve Cryptography", Version 2.0, May 2009.
     * http://www.secg.org/sec1-v2.pdf
     * 
     * Section 2.3.5 SEC1 "Field-Element-to-Octet-String Conversion"
     * 
     * @param a
     *            An element a of the field F_q.
     * @param mLen
     * @return An octet string M of length mlen
     * 
     */
    public byte[] fieldElementToOctetStringConversion(BigInteger a, int mLen)
    {
        // 1. If q = p is an odd prime, then a is an integer in the interval [0,
        // p − 1].
        // Convert a to M using the conversion routine specified in Section
        // 2.3.7
        // (with a and mlen as inputs). Output M.

        // 2.3.7 Integer-to-Octet-String Conversion
        BigInteger x = a;

        // Input: A non-negative integer x together with the desired length mlen
        // of the octet string.
        if (x.signum() < 0)
        {
            throw new IllegalArgumentException("a must be a non-negative integer");
        }

        // It must be the case that: 2^[8(mlen)] > x.
        BigDecimal k = BigDecimal.valueOf(Math.pow(2, 8 * mLen));
        if (x.compareTo(k.toBigIntegerExact()) >= 0)
        {
            throw new IllegalArgumentException(" The requirement 2^[8(mlen)] > x is not fulfilled.");
        }

        // byte array containing the two's-complement representation of this
        // BigInteger.
        byte[] xTwosComplement = x.toByteArray();

        byte[] m = new byte[xTwosComplement.length - 1];

        // 1. For 0 ≤ i ≤ mlen − 1, set: M_i = x_(mlen−1−i).
        if ((x.bitLength() % 8 == 0) && (xTwosComplement[0] == 0) && xTwosComplement.length > 1)
        {
            System.arraycopy(a.toByteArray(), 1, m, 0, m.length);
        } else
        {
            m = x.toByteArray();
        }

        // 2. Output An octet string M of length mlen octets.
        return m;
    }

}

/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwe.crypto;

import java.security.spec.AlgorithmParameterSpec;

import de.fhk.jwx.jwe.JweProtectedHeader;
import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.util.JoseUtils;

public abstract class AbstractJweDecrypter
{
    private AlgorithmParameterSpec spec;

    public abstract byte[] decryptPayload(byte[] payload, JwkKey key, byte[] tag, byte[] iv, JweProtectedHeader protectedHeader, String aad);

    public String generateAadEncryptionParameter(JweProtectedHeader protectedHeader, String aad)
    {
        String pH = (protectedHeader == null) ? "" : protectedHeader.getBase64URLEncodedHeaders();
        
        if (aad == null && protectedHeader.getNumberOfEntries() == 0)
        {
            return "";

        } else if (aad != null && aad.length() > 0)
        {
            return pH + "." + JoseUtils.stringToBase64URL(aad);

        } else
        {
            return pH;
        }
    }

    public void setSpec(AlgorithmParameterSpec spec)
    {
        this.spec = spec;
    }

    public AlgorithmParameterSpec getSpec()
    {
        return spec;
    }
}

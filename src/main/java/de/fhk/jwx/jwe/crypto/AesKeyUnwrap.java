/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwe.crypto;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.util.JwkUtils;

public class AesKeyUnwrap extends AbstractJweKeyDecrypter
{

    public AesKeyUnwrap()
    {
    }

    @Override
    public SecretKeySpec decryptKey(byte[] contentEncryptionKey, JwkKey decryptionKey)
    {
        Cipher encryptedKey = null;
        byte[] decryptedKey = null;

        SecretKeySpec key = (SecretKeySpec) JwkUtils.convertJwkToSecretKeySpec(decryptionKey);

        try
        {
            encryptedKey = Cipher.getInstance("AESWrap");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e)
        {
            e.printStackTrace();
        }
        try
        {
            encryptedKey.init(Cipher.UNWRAP_MODE, key);
        } catch (InvalidKeyException e)
        {
            e.printStackTrace();
        }
        try
        {
            decryptedKey = encryptedKey.unwrap(contentEncryptionKey, "", Cipher.SECRET_KEY).getEncoded();
        } catch (InvalidKeyException | NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return new SecretKeySpec(decryptedKey, "AES");
    }

}

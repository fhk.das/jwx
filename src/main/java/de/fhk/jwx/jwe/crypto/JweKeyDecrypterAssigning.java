/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwe.crypto;

import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jwe.JweRecipient;

public class JweKeyDecrypterAssigning
{
    public static AbstractJweKeyDecrypter getJweKeyDecrypter(JweRecipient recipient)
    {
        String alg = recipient.getMergedHeader().getAlgorithm();

        if (JwaAlgorithms.isKeyManagementDir(alg))
        {
            return null;

        } else if (JwaAlgorithms.isKeyManagementGcmWrap(alg))
        {
            return getAesGcmKeyUnwrap(recipient);

        } else if (JwaAlgorithms.isKeyManagementEcdh(alg))
        {
            return getEcdhKeyDecrypter(recipient);

        } else if (JwaAlgorithms.isKeyManagementAesWrap(alg))
        {
            return getAesKeyUnwrap(alg);

        } else if (JwaAlgorithms.isKeyManagementRsa(alg))
        {
            return getRsaKeyDecrypter(alg);

        } else if (JwaAlgorithms.isKeyManagementPbes(alg))
        {
            return getPbesAesKeyDecrypter(recipient);
            
        } else
        {
            throw new SecurityException("\"" + alg + "\"" + "is not a Key Management Algorithm");
        }
    }

    public static RsaKeyDecrypter getRsaKeyDecrypter(String algHeaderValue)
    {
        return new RsaKeyDecrypter(algHeaderValue);
    }

    public static AesKeyUnwrap getAesKeyUnwrap(String algHeaderValue)
    {
        return new AesKeyUnwrap();
    }

    public static AesGcmKeyUnwrap getAesGcmKeyUnwrap(JweRecipient recipient)
    {
        return new AesGcmKeyUnwrap(recipient);
    }

    public static EcdhEsKeyDecrypter getEcdhKeyDecrypter(JweRecipient recipient)
    {
        return new EcdhEsKeyDecrypter(recipient);
    }

    public static PbesAesKeyDecrypter getPbesAesKeyDecrypter(JweRecipient recipient)
    {
        return new PbesAesKeyDecrypter(recipient);
    }

}

/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwe.crypto;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.pqc.math.linearalgebra.ByteUtils;

import de.fhk.jwx.jwe.JweProtectedHeader;
import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.util.JwkUtils;

public class AesGcmDecrypter extends AbstractJweDecrypter
{

    public AesGcmDecrypter()
    {
    }

    @Override
    public byte[] decryptPayload(byte[] payload, JwkKey decryptionKey, byte[] tag, byte[] iv, JweProtectedHeader protectedHeader, String aad)
    {

        Cipher decryptCipher = null;

        try
        {
            decryptCipher = Cipher.getInstance("AES/GCM/NoPadding");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e)
        {
            e.printStackTrace();
        }

        // Initialization vector
        setSpec(new GCMParameterSpec(128, iv));

        // Content Decryption Key
        SecretKeySpec key = (SecretKeySpec) JwkUtils.convertJwkToSecretKeySpec(decryptionKey);

        // Initialize the Cipher Object
        try
        {
            decryptCipher.init(Cipher.DECRYPT_MODE, key, getSpec());
        } catch (InvalidKeyException | InvalidAlgorithmParameterException e)
        {
            e.printStackTrace();
        }

        String generatedAad = generateAadEncryptionParameter(protectedHeader, aad);
        decryptCipher.updateAAD(generatedAad.getBytes());

        byte[] gcmOutput = null;

        try
        {
            gcmOutput = decryptCipher.doFinal(ByteUtils.concatenate(payload, tag));
        } catch (IllegalBlockSizeException | BadPaddingException e)
        {
            e.printStackTrace();
        }

        return gcmOutput;
    }
}

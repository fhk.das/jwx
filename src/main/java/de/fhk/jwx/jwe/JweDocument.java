/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhk.jwx.jwe;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.Key;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

import javax.crypto.spec.SecretKeySpec;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jwe.crypto.AbstractJweDecrypter;
import de.fhk.jwx.jwe.crypto.AbstractJweKeyDecrypter;
import de.fhk.jwx.jwe.crypto.EcdhEsKeyAgreement;
import de.fhk.jwx.jwe.crypto.JweDecrypterAssigning;
import de.fhk.jwx.jwe.crypto.JweKeyDecrypterAssigning;
import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.util.JoseUtils;
import de.fhk.jwx.util.JwkUtils;

public final class JweDocument
{
    // For reasons of uniformity the JWE "message" or "plaintext content" is
    // designated as "payload" in analogy to JWS
    private byte[] payload = null;
    private String encodedPayload = null;
    private byte[] iv = null; // JWE Initialization Vector
    private String aad = null; // JWE Additional Authentication Data
    private JweProtectedHeader protectedHeader = null;
    private JweSharedUnprotectedHeader unprotectedHeader = null;
    private List<JweRecipient> recipients = null;
    private byte[] tag = null; // JWE Authentication Tag
    private boolean keyDecryption;

    /**
     * protected constructor, please see JweMaker
     */
    protected JweDocument(byte[] encryptedKey, byte[] payload, byte[] tag, String aad, JweProtectedHeader pH, JweSharedUnprotectedHeader uH,
            List<JweRecipient> recipientsEntries, byte[] iv)
    {
        this.payload = payload;
        if (payload != null)
        {
            this.encodedPayload = JoseUtils.bytesToBase64URLString(payload);
        }
        this.aad = aad;
        this.iv = iv;
        this.tag = tag;
        this.protectedHeader = pH;
        this.unprotectedHeader = uH;
        this.recipients = recipientsEntries;
    }

    public void addRecipient(Key key, JwePerRecipientUnprotectedHeader perRecipientHeader)
    {
        JweRecipient recipientEntry = new JweRecipient(perRecipientHeader, null);
        recipients.add(recipientEntry);
    }

    public String getCompactSerialization()
    {
        if (this.recipients.size() > 1)
        {
            throw new SecurityException("Can not create Compact Serialization. There are more than one recipients for this Jwe!");
        }

        if (this.aad != null)
        {
            throw new SecurityException("Can not create Compact Serialization because of set Additional Authentication Data!");
        }

        if (this.unprotectedHeader != null)
        {
            throw new SecurityException("Can not create Compact Serialization because of set Unprotected Header!");
        }

        if (this.recipients.get(0).getHeader() != null)
        {
            throw new SecurityException("Can not create Compact Serialization because of set Per Recipient Unprotected Header!");
        }

        boolean encryptedKey = false;
        if (this.recipients.get(0).getEncryptedKey() != null)
            encryptedKey = true;

        String output = this.protectedHeader.getBase64URLEncodedHeaders() + "."
                + (encryptedKey ? (JoseUtils.bytesToBase64URLString(this.recipients.get(0).getEncryptedKey())) : "") + "."
                + JoseUtils.bytesToBase64URLString(this.iv) + "." + this.encodedPayload + "." + JoseUtils.bytesToBase64URLString(this.tag);
        return output;
    }

    public String getJsonFlattenedSerialization()
    {
        return getJsonFlattenedSerialization(false);
    }

    public String getJsonFlattenedSerialization(boolean prettyPrint)
    {

        // just for the single-recipient case
        if (this.recipients.size() > 1)
        {
            throw new SecurityException("Can not create Json Flattened Serialization. There are more than one recipients for this Jwe!");
        }

        Map<String, Object> json = new LinkedHashMap<String, Object>();

        // a) protected header
        if (this.protectedHeader != null && this.protectedHeader.getAllHeaders().size() > 0)
        {
            try
            {
                json.put("protected", JoseUtils.stringToBase64URL(this.protectedHeader.getHeaders()));
            } catch (JsonProcessingException e)
            {
                e.printStackTrace();
            }
        }

        // b) unprotected header
        if (this.unprotectedHeader != null && this.unprotectedHeader.getAllHeaders().size() > 0)
        {
            Map<String, Object> unprotectedHeaders = new LinkedHashMap<String, Object>();
            unprotectedHeaders.putAll(this.unprotectedHeader.getAllHeaders());
            json.put("unprotected", unprotectedHeaders);
        }

        // c) header
        if (this.recipients.get(0).getHeader() != null && this.recipients.get(0).getHeader().getNumberOfEntries() > 0)
        {
            json.put("header", this.recipients.get(0).getHeader().getAllHeaders());
        }

        // d) encrypted_key
        if (this.recipients.get(0).getEncryptedKey() != null)
        {
            json.put("encrypted_key", JoseUtils.bytesToBase64URLString(this.recipients.get(0).getEncryptedKey()));
        }

        // e) aad
        if (this.aad != null)
        {
            json.put("aad", JoseUtils.stringToBase64URL(this.aad));
        }

        // f) iv
        if (this.iv != null)
        {
            json.put("iv", JoseUtils.bytesToBase64URLString(this.iv));
        }

        // g) ciphertext
        if (this.encodedPayload != null)
        {
            json.put("ciphertext", encodedPayload);
        }

        // h) tag
        if (this.tag != null)
        {
            json.put("tag", JoseUtils.bytesToBase64URLString(this.tag));
        }

        String jsonFlattenedSerialization = null;

        ObjectMapper mapper = new ObjectMapper();

        if (prettyPrint)
        {
            try
            {
                jsonFlattenedSerialization = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
            } catch (JsonProcessingException e)
            {
                e.printStackTrace();
            }
        } else
        {
            try
            {
                jsonFlattenedSerialization = mapper.writeValueAsString(json);
            } catch (JsonProcessingException e)
            {
                e.printStackTrace();
            }
        }
        return jsonFlattenedSerialization;
    }

    public String getJsonSerialization()
    {
        return getJsonSerialization(false);
    }

    public String getJsonSerialization(boolean prettyPrint)
    {
        LinkedHashMap<String, Object> json = new LinkedHashMap<String, Object>();

        ArrayList<LinkedHashMap<String, Object>> recipientsArray = new ArrayList<LinkedHashMap<String, Object>>();

        for (JweRecipient recipient : this.recipients)
        {
            LinkedHashMap<String, Object> recipientEntries = new LinkedHashMap<String, Object>();

            if (recipient.getEncryptedKey() != null)
            {
                recipientEntries.put("encrypted_key", JoseUtils.bytesToBase64URLString(recipient.getEncryptedKey()));
            }

            if (recipient.getHeader() != null && recipient.getHeader().getAllHeaders().size() > 0)
            {
                recipientEntries.put("header", recipient.getHeader().getAllHeaders());
            }

            if (recipientEntries.size() > 0)
            {
                recipientsArray.add(recipientEntries);
            }
        }

        if (this.recipients.size() > 0 && (this.recipients.get(0).getEncryptedKey() != null || this.recipients.get(0).getHeader() != null && this.recipients.get(0).getHeader().getNumberOfEntries() > 0))
        {
            json.put("recipients", recipientsArray);
        }

        // build the unprotected header entry
        if (this.unprotectedHeader != null && this.unprotectedHeader.getAllHeaders().size() > 0)
        {
            Map<String, Object> headers = new LinkedHashMap<String, Object>(this.unprotectedHeader.getAllHeaders());
            json.put("unprotected", headers);
        }

        // build the protected header entry
        if (this.protectedHeader != null && this.protectedHeader.getAllHeaders().size() > 0)
        {
            try
            {
                json.put("protected", JoseUtils.stringToBase64URL(this.protectedHeader.getHeaders()));
            } catch (JsonProcessingException e)
            {
                e.printStackTrace();
            }
        }

        // build the initialization vector entry
        if (this.iv != null)
            json.put("iv", JoseUtils.bytesToBase64URLString(this.iv));

        // build the aad entry
        if (this.aad != null)
            json.put("aad", JoseUtils.stringToBase64URL(aad));

        if (this.encodedPayload != null)
            json.put("ciphertext", encodedPayload);

        if (this.tag != null)
            json.put("tag", JoseUtils.bytesToBase64URLString(this.tag));

        ObjectMapper mapper = new ObjectMapper();

        String jsonSer = null;

        if (prettyPrint)
            try
            {
                jsonSer = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
            } catch (JsonProcessingException e)
            {
                e.printStackTrace();
            }
        else
            try
            {
                jsonSer = mapper.writeValueAsString(json);
            } catch (JsonProcessingException e)
            {
                e.printStackTrace();
            }
        return jsonSer;
    }

    public byte[] decrypt(JwkKey myKey)
    {
        keyDecryption = true;

        Key contentDecryptionKey = null;
        
        // RFC7516 5.2

        JweHeaders headers = mergeHeaders(this.protectedHeader, this.unprotectedHeader, this.recipients.get(0).getHeader());

        JwePerRecipientUnprotectedHeader header = new JwePerRecipientUnprotectedHeader();
        header.setHeaders(headers.getAllHeaders());
        recipients.get(0).setMergedHeader(header);

        String keyManagementAlgorithm = headers.getAlgorithm();
        String enc = headers.getContentEncryptionAlgorithm();

        // Direct Encryption
        if ((keyManagementAlgorithm.equals(JwaAlgorithms.DIRECT.getSpecName())))
        {
            keyDecryption = false;
            contentDecryptionKey = JwkUtils.convertJwkToSecretKeySpec(myKey);
        }

        // Direct Key Agreement
        if ((keyManagementAlgorithm.equals(JwaAlgorithms.ECDH_ES.getSpecName())))
        {
            keyDecryption = false;

            EcdhEsKeyAgreement keyAgreement = new EcdhEsKeyAgreement(this.recipients.get(0), recipients.size(), protectedHeader, null);
            contentDecryptionKey = new SecretKeySpec(keyAgreement.deriveKey(myKey), "AES");
        }

        // Key Decryption
        if (keyDecryption)
        {
            for (JweRecipient recipient : this.recipients)
            {
                // key identification with "kid"

                // TODO search key by ID

                JweHeaders recipientHeaders = mergeHeaders(this.protectedHeader, this.unprotectedHeader, recipient.getHeader());

                JwePerRecipientUnprotectedHeader recipientHeader = new JwePerRecipientUnprotectedHeader();
                recipientHeader.setHeaders(recipientHeaders.getAllHeaders());
                recipient.setMergedHeader(recipientHeader);

                AbstractJweKeyDecrypter keyDecrypter = null;
                keyDecrypter = JweKeyDecrypterAssigning.getJweKeyDecrypter(recipient);

                try
                {
                    contentDecryptionKey = keyDecrypter.decryptKey(recipient.getEncryptedKey(), myKey);
                } catch (Exception e)
                {
                    continue;
                }
                break;
            }
        }

        if (contentDecryptionKey == null)
        {
            throw new SecurityException("Decryption failed for every recipient!");
        }

        // Decrypt Payload
        AbstractJweDecrypter decrypter = JweDecrypterAssigning.getJweDecrypter(enc);

        String jwk = "{" + "\"kty\":\"oct\"," + "\"use\":\"enc\"," + "\"k\":\"" + JoseUtils.bytesToBase64URLString(contentDecryptionKey.getEncoded())
                + "\"}\"";

        JwkKey contentDecryptionJwk = new JwkKey(jwk);

        byte[] decryptedPayload = decrypter.decryptPayload(this.payload, contentDecryptionJwk, this.tag, this.iv, this.protectedHeader, this.aad);

        // Uncompress
        if (protectedHeader != null)
        {
            String zipAlgo = protectedHeader.getZipAlgorithm();

            if (zipAlgo != null && zipAlgo.equals("DEF"))
            {
                decryptedPayload = uncompressMessage(decryptedPayload);
            }
        }
        return decryptedPayload;
    }

    private static JweHeaders mergeHeaders(JweHeaders... headers)
    {
        JweHeaders allOfHeaders = new JweHeaders();
        int size = 0;
        for (JweHeaders jweHeaders : headers)
        {
            if (jweHeaders != null)
            {
                allOfHeaders.setHeaders(jweHeaders.getAllHeaders());
                size += jweHeaders.getNumberOfEntries();
            }
        }
        if (size > allOfHeaders.getNumberOfEntries())
            throw new SecurityException("double entry detected!");
        return allOfHeaders;
    }

    private byte[] uncompressMessage(byte[] decryptedPayload)
    {
        ByteArrayInputStream payloadStream = new ByteArrayInputStream(decryptedPayload);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        Inflater inflater = new Inflater(true);
        InflaterInputStream inputStream = new InflaterInputStream(payloadStream, inflater);

        int byteRead = 0;
        try
        {
            while ((byteRead = inputStream.read()) != -1)
            {
                outputStream.write(byteRead);
            }
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        return outputStream.toByteArray();
    }
}

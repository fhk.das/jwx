/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwe;

public class JoseException extends RuntimeException
{
    private static final long serialVersionUID = -131595189368976619L;

    /**
     * Constructs a NullPointerException with no detail message.
     */
    public JoseException()
    {
        super();
    }

    /**
     * Constructs a ConcatKdfException with the specified detail message.
     *
     * @param s
     *            the detail message.
     */
    public JoseException(String s)
    {
        super(s);
    }
}
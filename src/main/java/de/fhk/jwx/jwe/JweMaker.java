/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhk.jwx.jwe;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.zip.Deflater;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.fhk.jwx.JoseHeaders;
import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jwe.crypto.AbstractJweEncrypter;
import de.fhk.jwx.jwe.crypto.AbstractJweKeyEncrypter;
import de.fhk.jwx.jwe.crypto.EcdhEsKeyAgreement;
import de.fhk.jwx.jwe.crypto.JweCookbookBypass;
import de.fhk.jwx.jwe.crypto.JweEncrypterAssigning;
import de.fhk.jwx.jwe.crypto.JweKeyEncrypterAssigning;
import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.util.JoseUtils;
import de.fhk.jwx.util.JwkUtils;

public final class JweMaker
{
    private byte[] message = null;
    private byte[] ciphertext = null;
    private String aad = null;
    private byte[] authenticationTag = null;
    private byte[] initializationVector = null;
    private JweProtectedHeader protectedHeader = null;
    private JweSharedUnprotectedHeader unprotectedHeader = null;
    private JweRecipient[] recipients = null;
    private boolean keyEncryption = true;
    private byte[] encryptedKey = null;

    private JweMaker(byte[] payload, String aad, JweProtectedHeader pH, JweSharedUnprotectedHeader uH, JweRecipient... recipients)
    {
        this.message = payload.clone();
        this.aad = aad;
        this.protectedHeader = pH;
        this.unprotectedHeader = uH;
        this.recipients = recipients;

        validateHeaders();

        if (this.recipients == null || this.recipients.length == 0)
        {
            produceMergedHeaders();
        }
    }

    // generate from Jwe Compact Serialization
    private JweMaker(JweProtectedHeader pH, byte[] encryptedKey, byte[] ciphertext, byte[] authenticationTag, byte[] initializationVector)
    {
        this.protectedHeader = pH;
        this.ciphertext = ciphertext;
        this.authenticationTag = authenticationTag;
        this.initializationVector = initializationVector;

        validateHeaders();

        JweRecipient[] recipients = new JweRecipient[1];
        JweRecipient recipient = new JweRecipient(null, null);
        recipient.setEncryptedKey(encryptedKey);
        recipients[0] = recipient;
        this.recipients = recipients;
    }
    
    // generate from Jwe JSON Serialization
    private JweMaker(byte[] encryptedKey, byte[] ciphertext, byte[] authenticationTag, String aad, byte[] initializationVector, JweProtectedHeader pH, JweSharedUnprotectedHeader uH, JweRecipient... recipients)
    {    
        this.encryptedKey = encryptedKey;
        this.ciphertext = ciphertext;
        this.authenticationTag = authenticationTag;
        this.aad = aad;
        this.initializationVector = initializationVector;
        
        this.protectedHeader = pH;
        this.unprotectedHeader = uH;
        this.recipients = recipients;
        
        validateHeaders();
    } 

    private boolean validateHeaders()
    {
        boolean valid = false;
        switch (getHeaderCase())
        {
        case 0:
            throw new JoseException("All headers are null");
        case 1:
            validateGlobalHeader(protectedHeader);
            break;
        case 2:
            validateGlobalHeader(unprotectedHeader);
            break;
        case 3:
            validateGlobalHeaders();
            break;
        case 4:
            validatePerRecipientHeaders(false);
            break;
        case 5:
            validateGlobalandPerRecipientHeader(protectedHeader);
            break;
        case 6:
            validateGlobalandPerRecipientHeader(unprotectedHeader);
            break;
        case 7:
            validateAllHeaders();
            break;
        }
        return valid;
    }

    /**
     * # Headers 7 all 6 uH + pRUH 5 ph + pRUH 4 pRUH 3 ph + uH 2 uH 1 pH 0 none
     * 
     * @return header case number
     */
    private int getHeaderCase()
    {
        int headerCase = 0;
        if (protectedHeader != null)
            headerCase += 1;
        if (unprotectedHeader != null)
            headerCase += 2;
        if (recipients != null && recipients.length > 0)
            headerCase += 4;
        return headerCase;
    }

    private void validateAlgHeaderEntry(String alg)
    {
        if (alg != null)
        {
            if (!JwaAlgorithms.isKeyManagementAlg(alg))
            {
                throw new JoseException("Key Management Algorithm value \"" + alg + "\" is not allowed");
            }
        } else
            throw new JoseException("Key Management Algorithm is null");
    }

    private void validateEncHeaderEntry(String enc)
    {
        if (enc != null)
        {
            if (!JwaAlgorithms.isContentEncAlg(enc))
            {
                throw new JoseException("Content Encryption Algorithm value \"" + enc + "\" is not allowed");
            }
        } else
            throw new JoseException("Content Encryption Algorithm is null");
    }

    private void validateGlobalHeader(JweHeaders header)
    {
        // no double entries possible
        validateAlgHeaderEntry(header.getAlgorithm());
        validateEncHeaderEntry(header.getContentEncryptionAlgorithm());

        // check protected only entries
        if (header.getClass() == JweSharedUnprotectedHeader.class || header.getClass() == JwePerRecipientUnprotectedHeader.class)
        {
            validateProtectedOnlyEntries(header);
        }
        // check critical header
        else
        {
            if (protectedHeader.getCriticalHeaders() != null)
            {
                for (String critH : protectedHeader.getCriticalHeaders())
                {
                    if (!JoseHeaders.defindedHeaderParametersAsList().contains(critH))
                    {
                        throw new JoseException("The critical header \"" + critH + "\" is not supported by this JOSE implementation");
                    }
                }
            }
        }
    }

    private void validateGlobalHeaders()
    {
        // check for double entries
        Map<String, Object> pH = this.protectedHeader.getAllHeaders();
        Map<String, Object> uH = this.unprotectedHeader.getAllHeaders();

        for (String key : pH.keySet())
        {
            if (uH.containsKey(key))
                throw new JoseException("double entry \"" + key + "\" must not be set in protected and unprotected header");
        }
        for (String key : uH.keySet())
        {
            if (pH.containsKey(key))
                throw new JoseException("double entry \"" + key + "\" must not be set in protected and unprotected header");
        }

        // check alg
        if (this.protectedHeader.getAlgorithm() != null)
            validateAlgHeaderEntry(this.protectedHeader.getAlgorithm());
        else
            validateAlgHeaderEntry(this.unprotectedHeader.getAlgorithm());

        // check enc
        if (protectedHeader.getContentEncryptionAlgorithm() != null)
            validateEncHeaderEntry(this.protectedHeader.getContentEncryptionAlgorithm());
        else
            validateEncHeaderEntry(this.unprotectedHeader.getContentEncryptionAlgorithm());

        // check for protected only entries in unprotected header
        validateProtectedOnlyEntries(this.unprotectedHeader);
    }

    // JweProtected should not be used as parameter here
    private void validateProtectedOnlyEntries(JweHeaders header)
    {
        Map<String, Object> uH = header.getAllHeaders();
        if (uH.containsKey(JoseHeaders.HEADER_CRITICAL))
            throw new JoseException("Critical Header is not allowed in unprotected header");
        if (uH.containsKey(JoseHeaders.JWE_HEADER_ZIP_ALGORITHM))
            throw new JoseException("zip header not allowed in unprotected header");
    }

    private void validatePerRecipientHeaders(boolean merged)
    {
        int count = 0;
        String encValue = null;

        for (JweRecipient jweRecipient : this.recipients)
        {
            // header to be testet
            JwePerRecipientUnprotectedHeader header;

            if (merged)
            {
                header = jweRecipient.getMergedHeader();
            } else
            {
                header = jweRecipient.getHeader();
            }

            // double entries in single recipient header not possible

            // validate every recipient entry
            validateGlobalHeader(header);

            // validate enc, must be the same in every recipient header
            if (count == 0)
            {
                encValue = header.getContentEncryptionAlgorithm();
            } else
            {
                if (!encValue.equals(header.getContentEncryptionAlgorithm()))
                {
                    throw new JoseException("Content Encryption Algorithm must be the same for every recipient. \""
                            + jweRecipient.getHeader().getContentEncryptionAlgorithm() + "\" does not equal \"" + encValue + "\"");
                }
            }
            count++;

            if (jweRecipient.getMergedHeader() == null)
            {
                putProtectedAndUnprotectedinPerRecipientMergedHeader();
            }
        }
    }

    private void validateGlobalandPerRecipientHeader(JweHeaders globalHeader)
    {
        // search for double entries
        Map<String, Object> gH = globalHeader.getAllHeaders();
        for (JweRecipient header : this.recipients)
        {
            for (String key : gH.keySet())
            {
                if (header.getHeader().getAllHeaders().containsKey(key))
                    throw new JoseException("double entry \"" + key + "\" must not be set in global and per recipient header");
            }
        }

        // validate merged headers
        if (globalHeader.getClass() == JweProtectedHeader.class)
        {
            putProtectedinPerRecipientMergedHeader();

        } else
        {
            putUnprotectedinPerRecipientMergedHeader();

        }
        validatePerRecipientHeaders(true);
    }

    private static JweHeaders mergeHeaders(JweHeaders... headers)
    {
        JweHeaders allOfHeaders = new JweHeaders();
        int size = 0;
        for (JweHeaders jweHeaders : headers)
        {
            if (jweHeaders != null)
            {
                allOfHeaders.setHeaders(jweHeaders.getAllHeaders());
                size += jweHeaders.getNumberOfEntries();
            }
        }
        if (size > allOfHeaders.getNumberOfEntries())
            throw new JoseException("double entry detected!");
        return allOfHeaders;

    }

    private void putProtectedinPerRecipientMergedHeader()
    {
        // for each recipient header
        for (JweRecipient jweRecipient : this.recipients)
        {
            // are protected only entries in recipient header?
            validateProtectedOnlyEntries(jweRecipient.getHeader());

            // merge protected header and recipient header
            JwePerRecipientUnprotectedHeader mergedHeaders = new JwePerRecipientUnprotectedHeader();
            mergedHeaders.setHeaders(mergeHeaders(this.protectedHeader, jweRecipient.getHeader()).getAllHeaders());

            // remove possible protected only entries
            mergedHeaders.getAllHeaders().remove(JoseHeaders.HEADER_CRITICAL);
            mergedHeaders.getAllHeaders().remove(JoseHeaders.JWE_HEADER_ZIP_ALGORITHM);

            // set merged header to recipient object for later encryption
            // process

            jweRecipient.setMergedHeader(mergedHeaders);
        }
    }

    private void putUnprotectedinPerRecipientMergedHeader()
    {
        // for each recipient header
        for (JweRecipient jweRecipient : this.recipients)
        {
            // merge protected header and recipient header
            JwePerRecipientUnprotectedHeader mergedHeaders = new JwePerRecipientUnprotectedHeader();
            mergedHeaders.setHeaders(mergeHeaders(this.unprotectedHeader, jweRecipient.getHeader()).getAllHeaders());
            // set merged header to recipient object for later encryption
            // process
            jweRecipient.setMergedHeader(mergedHeaders);
        }
    }

    private JweRecipient[] putProtectedAndUnprotectedinPerRecipientMergedHeader()
    {
        putProtectedinPerRecipientMergedHeader();
        // for each recipient header
        for (JweRecipient jweRecipient : this.recipients)
        {
            // merge protected header and recipient header

            JwePerRecipientUnprotectedHeader mergedHeaders = new JwePerRecipientUnprotectedHeader();
            mergedHeaders.setHeaders(mergeHeaders(unprotectedHeader, jweRecipient.getMergedHeader()).getAllHeaders());

            // set merged header to recipient object for later encryption
            // process
            jweRecipient.setMergedHeader(mergedHeaders);
        }
        return this.recipients;
    }

    private void validateAllHeaders()
    {
        // check for double entries in global headers
        Map<String, Object> pH = this.protectedHeader.getAllHeaders();
        Map<String, Object> uH = this.unprotectedHeader.getAllHeaders();

        for (String key : pH.keySet())
        {
            if (uH.containsKey(key))
                throw new JoseException("double entry \"" + key + "\" must not be set in protected and unprotected header");
        }

        for (String key : uH.keySet())
        {
            if (pH.containsKey(key))
                throw new JoseException("double entry \"" + key + "\" must not be set in protected and unprotected header");
        }

        // search for double entries in protected and perRecipientHeader
        for (JweRecipient jweRecipient : this.recipients)
        {
            for (String key : this.protectedHeader.getAllHeaders().keySet())
            {
                if (jweRecipient.getHeader().getAllHeaders().containsKey(key))
                    throw new JoseException("double entry \"" + key + "\" must not be set in global and per recipient header");
            }
        }

        // search for double entries in unprotected and perRecipientHeader
        for (JweRecipient jweRecipient : this.recipients)
        {
            for (String key : this.unprotectedHeader.getAllHeaders().keySet())
            {
                if (jweRecipient.getHeader().getAllHeaders().containsKey(key))
                    throw new JoseException("double entry \"" + key + "\" must not be set in global and per recipient header");
            }
        }
        putProtectedAndUnprotectedinPerRecipientMergedHeader();

        validatePerRecipientHeaders(true);

    }

    private JweDocument encrypt(JwkKey key)
    {
        return encrypt(key, null);
    }

    private JweDocument encrypt(JwkKey key, JweCookbookBypass cookbookBypass)
    {
        // same content encryption algorithm for all recipients
        final String contentEncryptionAlg = this.recipients[0].getMergedHeader().getContentEncryptionAlgorithm();

        SecretKey contentEncryptionKey = null;

        // Direct Encryption and Direct Key Agreement exceptions
        for (JweRecipient recipient : this.recipients)
        {
            int recipientCount = this.recipients.length;
            String keyManagementAlgorithm = recipient.getMergedHeader().getAlgorithm();

            if ((keyManagementAlgorithm == JwaAlgorithms.DIRECT.getSpecName()))
            {
                if (recipientCount != 1)
                {
                    throw new JoseException("Direct Encryption (" + keyManagementAlgorithm + ") is not applicable for a Jwe with"
                            + recipientCount + "recipients");
                }
            }

            if ((keyManagementAlgorithm.equals(JwaAlgorithms.ECDH_ES.getSpecName())))
            {
                if (recipientCount != 1)
                {
                    throw new JoseException("Direct Key Agreement (" + keyManagementAlgorithm + ") is not applicable for a Jwe with"
                            + recipientCount + "recipients");
                }
            }
                 
            //double keys
            if (recipient.getEncryptionKey() != null && key != null){
                throw new JoseException("Encryption key must not be set global and per recipient.");
            }          
        }

        String keyManagementAlgorithm = this.recipients[0].getMergedHeader().getAlgorithm();
        
        if (this.recipients.length == 1 && key == null)
        {
            key = this.recipients[0].getEncryptionKey();
        }

        // Key Management Algorithm: Direct Encryption
        // RFC 7518 chapter 4.5.: Direct Encryption with a Shared Symmetric Key
        if ((keyManagementAlgorithm == JwaAlgorithms.DIRECT.getSpecName()))
        {
            keyEncryption = false;
            contentEncryptionKey = (SecretKey) JwkUtils.convertJwkToSecretKeySpec(key);
        }

        // Key Agreement with Elliptic Curve Diffie-Hellman Ephemeral Static
        // RFC 7518 chapter 4.6.: Key Agreement with Elliptic Curve
        // Diffie-Hellman Ephemeral Static
        else if ((keyManagementAlgorithm == JwaAlgorithms.ECDH_ES.getSpecName()))
        {
            keyEncryption = false;
            if (protectedHeader == null)
            {
                protectedHeader = new JweProtectedHeader();
            }

            EcdhEsKeyAgreement keyAgreement = new EcdhEsKeyAgreement(this.recipients[0], this.recipients.length, this.protectedHeader, cookbookBypass);

            contentEncryptionKey = new SecretKeySpec(keyAgreement.deriveKey(key), "AES");
        } else
        {
            // compute random Content Encryption Key value
            KeyGenerator keyGen = null;

            switch (contentEncryptionAlg)
            {
            case "A128CBC-HS256":
                keyGen = keyGenInit(keyGen, "HmacSHA256", 256);
                break;
            case "A192CBC-HS384":
                keyGen = keyGenInit(keyGen, "HmacSHA384", 384);
                break;
            case "A256CBC-HS512":
                keyGen = keyGenInit(keyGen, "HmacSHA512", 512);
                break;
            case "A128GCM":
                keyGen = keyGenInit(keyGen, "AES", 128);
                break;
            case "A192GCM":
                keyGen = keyGenInit(keyGen, "AES", 192);
                break;
            case "A256GCM":
                keyGen = keyGenInit(keyGen, "AES", 256);
                break;
            default:
                throw new JoseException("Wrong Content Encryption Algorithm");
            }

            if (cookbookBypass == null)
            {
                contentEncryptionKey = keyGen.generateKey();
            } else
            {
                if (cookbookBypass.getBypassContentEncryptionKey() == null)
                {
                    contentEncryptionKey = keyGen.generateKey();
                } else
                {
                    contentEncryptionKey = cookbookBypass.getBypassContentEncryptionKey();
                }
            }
        }

        // repeat for each recipient.
        if (keyEncryption)
        {
            for (JweRecipient recipient : this.recipients)
            {
                JwePerRecipientUnprotectedHeader mergedHeader = recipient.getMergedHeader();

                keyManagementAlgorithm = mergedHeader.getAlgorithm();

                AbstractJweKeyEncrypter keyEncrypter = null;
                keyEncrypter = JweKeyEncrypterAssigning.getJweKeyEncrypter(recipient, recipients.length, protectedHeader, cookbookBypass);

                JwkKey recipientKey = null;

                if (key == null)
                {
                    recipientKey = recipient.getEncryptionKey();
                } else
                {
                    recipientKey = key;
                }

                byte[] encryptedKey = keyEncrypter.encryptKey(contentEncryptionKey, recipientKey);
                recipient.setEncryptedKey(encryptedKey);
            }
        }

        // content encryption

        if (protectedHeader != null)
        {
            String zipAlgo = protectedHeader.getZipAlgorithm();
            if (zipAlgo != null && zipAlgo.equals("DEF"))
            {
                compressMessage();
            }
        }

        AbstractJweEncrypter encrypter = null;
        encrypter = JweEncrypterAssigning.getJweEncrypter(contentEncryptionAlg);

        if (cookbookBypass != null)
        {
            if (cookbookBypass.getBypassInitializationVectorForContentEncryption() != null)
                encrypter.setIV(JoseUtils.base64URLTobytes(cookbookBypass.getBypassInitializationVectorForContentEncryption()));
        }

        encrypter.encryptPayload(message, contentEncryptionKey, this.aad, this.protectedHeader);

        this.ciphertext = encrypter.getCiphertext();

        this.authenticationTag = encrypter.getAuthenticationTag();

        JweDocument jwe = new JweDocument(null, this.ciphertext, this.authenticationTag, this.aad, this.protectedHeader, this.unprotectedHeader,
                Arrays.asList(this.recipients), encrypter.getIV());
        return jwe;
    }

    public static byte[] getByteArrayFromIntArray(int[] intArray)
    {
        byte[] byteArray = new byte[intArray.length];
        for (int i = 0; i < intArray.length; i++)
        {
            byteArray[i] = (byte) intArray[i];
        }
        return byteArray;
    }

    private void produceMergedHeaders()
    {
        JwePerRecipientUnprotectedHeader mergedHeaders = new JwePerRecipientUnprotectedHeader();

        if ((this.protectedHeader != null) && (this.unprotectedHeader != null))
        {
            mergedHeaders.setHeaders(mergeHeaders(this.protectedHeader, this.unprotectedHeader).getAllHeaders());
            mergedHeaders.getAllHeaders().remove(JoseHeaders.HEADER_CRITICAL);
            mergedHeaders.getAllHeaders().remove(JoseHeaders.JWE_HEADER_ZIP_ALGORITHM);

        } else if (this.protectedHeader == null)
        {
            mergedHeaders.setHeaders(this.unprotectedHeader.getAllHeaders());
        } else
        {
            mergedHeaders.setHeaders(this.protectedHeader.getAllHeaders());
        }

        JweRecipient recipient = new JweRecipient(null, null);
        recipient.setMergedHeader(mergedHeaders);
        this.recipients = new JweRecipient[1];
        this.recipients[0] = recipient;
    }

    /**
     * Case1/7: "Protected Header"
     */
    public static JweDocument generateFromMessage(byte[] payload, String aad, JwkKey key, JweProtectedHeader protectedHeader)
    {
        JweMaker maker = new JweMaker(payload, aad, protectedHeader, null);
        return maker.encrypt(key);
    }

    public static JweDocument generateFromMessage(byte[] payload, JwkKey key, JweProtectedHeader protectedHeader)
    {
        JweMaker maker = new JweMaker(payload, null, protectedHeader, null);
        return maker.encrypt(key);
    }

    /**
     * Case2/7: "Unprotected Header"
     */
    public static JweDocument generateFromMessage(byte[] payload, String aad, JwkKey key, JweSharedUnprotectedHeader unprotectedHeader)
    {
        JweMaker maker = new JweMaker(payload, aad, null, unprotectedHeader);
        return maker.encrypt(key);
    }

    public static JweDocument generateFromMessage(byte[] payload, JwkKey key, JweSharedUnprotectedHeader unprotectedHeader)
    {
        final JweMaker maker = new JweMaker(payload, null, null, unprotectedHeader);
        return maker.encrypt(key);
    }

    /**
     * Case3/7: "Protected Header and Unprotected Header"
     */
    public static JweDocument generateFromMessage(byte[] payload, String aad, JwkKey key, JweProtectedHeader protectedHeader,
            JweSharedUnprotectedHeader unprotectedHeader)
    {
        JweMaker maker = new JweMaker(payload, aad, protectedHeader, unprotectedHeader);
        return maker.encrypt(key);
    }

    public static JweDocument generateFromMessage(byte[] payload, JwkKey key, JweProtectedHeader protectedHeader,
            JweSharedUnprotectedHeader unprotectedHeader)
    {
        JweMaker maker = new JweMaker(payload, null, protectedHeader, unprotectedHeader);
        return maker.encrypt(key);
    }

    /**
     * Case4/7: "Per Recipient Header"
     */
    public static JweDocument generateFromMessage(byte[] payload, String aad, JwkKey key, JweRecipient... perRecipient)
    {
        JweMaker maker = new JweMaker(payload, aad, null, null, perRecipient);
        return maker.encrypt(key);     
    }

    public static JweDocument generateFromMessage(byte[] payload, JwkKey key, JweRecipient... perRecipient)
    {
        JweMaker maker = new JweMaker(payload, null, null, null, perRecipient);
        return maker.encrypt(key);
    }
    
    public static JweDocument generateFromMessage(byte[] payload, JweRecipient... perRecipient)
    {
        JweMaker maker = new JweMaker(payload, null, null, null, perRecipient);
        return maker.encrypt(null);
    }
    
    public static JweDocument generateFromMessage(byte[] payload, String aad, JweRecipient... perRecipient)
    {
        JweMaker maker = new JweMaker(payload, aad, null, null, perRecipient);
        return maker.encrypt(null);
    }

    /**
     * Case5/7: "Protected and Per Recipient Header"
     */
    public static JweDocument generateFromMessage(byte[] payload, String aad, JwkKey key, JweProtectedHeader protectedHeader,
            JweRecipient... perRecipient)
    {
        JweMaker maker = new JweMaker(payload, aad, protectedHeader, null, perRecipient);
        return maker.encrypt(key);
    }

    public static JweDocument generateFromMessage(byte[] payload, JwkKey key, JweProtectedHeader protectedHeader, JweRecipient... perRecipient)
    {
        JweMaker maker = new JweMaker(payload, null, protectedHeader, null, perRecipient);
        return maker.encrypt(key);
    }

    /**
     * Case6/7: "Unprotected and Per Recipient Header"
     */
    public static JweDocument generateFromMessage(byte[] payload, String aad, JwkKey key, JweSharedUnprotectedHeader unprotectedHeader,
            JweRecipient... perRecipient)
    {
        JweMaker maker = new JweMaker(payload, aad, null, unprotectedHeader, perRecipient);
        return maker.encrypt(key);
    }

    public static JweDocument generateFromMessage(byte[] payload, JwkKey key, JweSharedUnprotectedHeader unprotectedHeader,
            JweRecipient... perRecipient)
    {
        JweMaker maker = new JweMaker(payload, null, null, unprotectedHeader, perRecipient);
        return maker.encrypt(key);
    }

    /**
     * Case7/7: "All Headers"
     * 
     */
    public static JweDocument generateFromMessage(byte[] payload, String aad, JwkKey key, JweProtectedHeader protectedHeader,
            JweSharedUnprotectedHeader unprotectedHeader, JweRecipient... perRecipient)
    {
        JweMaker maker = new JweMaker(payload, aad, protectedHeader, unprotectedHeader, perRecipient);
        return maker.encrypt(key);
    }

    public static JweDocument generateFromMessage(byte[] payload, JwkKey key, JweProtectedHeader protectedHeader,
            JweSharedUnprotectedHeader unprotectedHeader, JweRecipient... perRecipient)
    {
        JweMaker maker = new JweMaker(payload, null, protectedHeader, unprotectedHeader, perRecipient);
        return maker.encrypt(key);
    }

    public static JweDocument generateFromMessageForJoseCookBook(JweCookbookBypass cookbookBypass, byte[] payload, String aad, JwkKey key,
            JweProtectedHeader protectedHeader, JweSharedUnprotectedHeader unprotectedHeader, JweRecipient... perRecipient)
    {

        JweMaker maker = new JweMaker(payload, aad, protectedHeader, unprotectedHeader, perRecipient);

        if (cookbookBypass == null)
        {
            throw new JoseException("cookbookBypass is null");
        }

        return maker.encrypt(key, cookbookBypass);
    }

    /**
     * RFC7516 5.2 1. Parse the JWE representation to extract the serialized
     * values for the components of the JWE. 2. Base64url decode the encoded
     * components of the JWE
     * 
     * @param jwe
     *            JWE representation
     * @return JweDocument
     */
    @SuppressWarnings("unchecked")
    public static JweDocument generateFromJwe(String jwe)
    {
        boolean compact = false;
        JweDocument jweDocument = null;

        if (jwe.split("\\.").length == 5 && !jwe.startsWith("{"))
        {
            compact = true;
        }

        if (!compact)
        {

            ObjectMapper mapper = new ObjectMapper();

            if (jwe.startsWith("{") && jwe.endsWith("}"))
            {

                LinkedHashMap<String, Object> jweAsJson;

                try
                {
                    jweAsJson = mapper.readValue(jwe, new TypeReference<LinkedHashMap<String, Object>>()
                    {
                    });

                    // a) protected header
                    String protectedHeaders = (String) jweAsJson.get("protected");

                    // b) unprotected header
                    LinkedHashMap<String, Object> unprotectedSharedHeaders = (LinkedHashMap<String, Object>) jweAsJson.get("unprotected");

                    // c) recipents
                    ArrayList<LinkedHashMap<String, Object>> recipients = (ArrayList<LinkedHashMap<String, Object>>) jweAsJson.get("recipients");

                    List<JweRecipient> recipientsList = new LinkedList<JweRecipient>();

                    if (recipients != null)
                    {
                        for (LinkedHashMap<String, Object> jweRecipient : recipients)
                        {
                            // header
                            JwePerRecipientUnprotectedHeader header = new JwePerRecipientUnprotectedHeader();

                            if (jweRecipient.get("encrypted_key") != null && jweAsJson.get("encrypted_key") != null){
                                throw new JoseException("Encrypted key must not be set global and per recipient.");
                            }
                                                               
                            if (jweRecipient.get("header") != null)
                            {
                                header.setHeaders((LinkedHashMap<String, Object>) jweRecipient.get("header"));
                            }

                            JweRecipient recipient = new JweRecipient(header, null);

                            // encrypted_key
                            if (jweRecipient.get("encrypted_key") != null)
                            {
                                recipient.setEncryptedKey(JoseUtils.base64URLTobytes((String) (jweRecipient.get("encrypted_key"))));
                            }

                            recipientsList.add(recipient);
                        }
                    } else
                    {
                        JwePerRecipientUnprotectedHeader header = new JwePerRecipientUnprotectedHeader();
                        JweRecipient recipient = new JweRecipient(header, null);
                        
                        if (jweAsJson.get("encrypted_key") != null)
                        {
                            recipient.setEncryptedKey(JoseUtils.base64URLTobytes((String) (jweAsJson.get("encrypted_key"))));
                        }
                        recipientsList.add(recipient);
                    }

                    // d) aad
                    String aad = (String) jweAsJson.get("aad");

                    if (aad != null)
                    {
                        aad = JoseUtils.base64URLToString(aad);
                    }

                    // e) iv
                    String iv = (String) jweAsJson.get("iv");

                    // f) ciphertext
                    String ciphertext = (String) jweAsJson.get("ciphertext");

                    // g) tag
                    String tag = (String) jweAsJson.get("tag");

                    JweProtectedHeader pH = new JweProtectedHeader();

                    if (jweAsJson.get("protected") != null)
                    {
                        pH.setHeaders(mapper.readValue(JoseUtils.base64URLToString(protectedHeaders),
                                new TypeReference<LinkedHashMap<String, Object>>()
                                {
                                }));
                    }

                    JweSharedUnprotectedHeader uH = new JweSharedUnprotectedHeader();

                    if (unprotectedSharedHeaders != null)
                    {
                        uH.setHeaders(unprotectedSharedHeaders);
                    }
                    
                    JweRecipient[] recipientsArray = new JweRecipient[recipientsList.size()];
                    int number = 0;
                    for (JweRecipient recipient : recipientsList){
                        recipientsArray[number] = recipient;
                        number++;
                    }
                    
                    JweMaker maker = new JweMaker(null, 
                                                JoseUtils.base64URLTobytes(ciphertext), 
                                                JoseUtils.base64URLTobytes(tag), 
                                                aad,
                                                JoseUtils.base64URLTobytes(iv),
                                                pH, 
                                                uH, 
                                                recipientsArray);
                    
                    jweDocument = maker.getImportedJweDocument();
                    
                } catch (IOException e)
                {
                    e.printStackTrace();
                }
                
            } else
            {
                throw new JoseException("Malformed JWE");
            }

        /*
         * when using the JWE Compact Serialization
         */
        } else
        {
            String[] jweComponents = jwe.split("\\.");

            // base64url-encoded representations of the JWE Protected Header
            LinkedHashMap<String, Object> headers = null;
            ObjectMapper mapper = new ObjectMapper();

            try
            {
                headers = mapper.readValue(JoseUtils.base64URLToString(jweComponents[0]), new TypeReference<LinkedHashMap<String, Object>>()
                {
                });
            } catch (IOException e)
            {
                e.printStackTrace();
            }
            JweProtectedHeader protectedHeader = new JweProtectedHeader();

            protectedHeader.setHeaders(headers);

            JweMaker maker = new JweMaker(protectedHeader,
            // base64url-decoded representations of the JWE Encrypted Key
                    JoseUtils.base64URLTobytes(jweComponents[1]),
                    // base64url-encoded representations of the JWE Ciphertext
                    JoseUtils.base64URLTobytes(jweComponents[3]),
                    // base64url-encoded representations of and the JWE
                    // Authentication Tag
                    JoseUtils.base64URLTobytes(jweComponents[4]),
                    // base64url-encoded representations of the JWE
                    // Initialization Vector
                    JoseUtils.base64URLTobytes(jweComponents[2]));

            jweDocument = maker.getImportedJweDocument();
        }
        return jweDocument;
    }

    private JweDocument getImportedJweDocument()
    {
        JweDocument jwe = new JweDocument(this.encryptedKey, this.ciphertext, this.authenticationTag, this.aad, this.protectedHeader, this.unprotectedHeader,
                Arrays.asList(this.recipients), this.initializationVector);
        return jwe;
    }

    private void compressMessage()
    {
        // TODO compare to buffer performance

        byte[] output = new byte[message.length];

        Deflater deflater = new Deflater(Deflater.DEFLATED, true);
        deflater.setInput(message);
        deflater.finish();
        int compressedMessageLength = deflater.deflate(output);
        deflater.end();

        byte[] compressedMessage = new byte[compressedMessageLength];
        System.arraycopy(output, 0, compressedMessage, 0, compressedMessageLength);

        message = compressedMessage;
    }

    private KeyGenerator keyGenInit(KeyGenerator keyGen, String algorithmName, int keySize)
    {
        try
        {
            keyGen = KeyGenerator.getInstance(algorithmName);
        } catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        keyGen.init(keySize);
        return keyGen;
    }
}

/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhk.jwx.jwe.tests;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidParameterSpecException;
import java.text.ParseException;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.SecretKeySpec;

import org.junit.Test;

import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jwe.JweDocument;
import de.fhk.jwx.jwe.JweMaker;
import de.fhk.jwx.jwe.JwePerRecipientUnprotectedHeader;
import de.fhk.jwx.jwe.JweProtectedHeader;
import de.fhk.jwx.jwe.JweRecipient;
import de.fhk.jwx.jwe.JweSharedUnprotectedHeader;
import de.fhk.jwx.jwe.crypto.JweCookbookBypass;
import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.jwk.JwkKeySet;
import de.fhk.jwx.util.JoseUtils;
import de.fhk.jwx.util.JwkUtils;

public class AppendixA {

    public static final int[] A1_PLAIN_TEXT = { 84, 104, 101, 32, 116, 114, 117, 101,
            32, 115, 105, 103, 110, 32, 111, 102, 32, 105, 110, 116, 101, 108,
            108, 105, 103, 101, 110, 99, 101, 32, 105, 115, 32, 110, 111, 116,
            32, 107, 110, 111, 119, 108, 101, 100, 103, 101, 32, 98, 117, 116,
            32, 105, 109, 97, 103, 105, 110, 97, 116, 105, 111, 110, 46 };
    
    public static final int[] A1_CEK = { 177, 161, 244, 128, 84, 143, 225, 115, 63,
            180, 3, 255, 107, 154, 212, 246, 138, 7, 110, 91, 112, 46, 34, 105,
            47, 130, 203, 46, 122, 234, 64, 252 };
    
    public static final String A1_IV = "48V1_ALb6US04U3b";
    
	public static final int[] A1_AAD = {101, 121, 74, 104, 98, 71, 99, 105, 79, 105,
			74, 83, 85, 48, 69, 116, 84, 48, 70, 70, 85, 67, 73, 115, 73, 109,
			86, 117, 89, 121, 73, 54, 73, 107, 69, 121, 78, 84, 90, 72, 81, 48,
			48, 105, 102, 81};
	
	
	public static final String A1_COMPACT =
	          "eyJhbGciOiJSU0EtT0FFUCIsImVuYyI6IkEyNTZHQ00ifQ."
	        + "OKOawDo13gRp2ojaHV7LFpZcgV7T6DVZKTyKOMTYUmKoTCVJRgckCL9kiMT03JGe"
	        + "ipsEdY3mx_etLbbWSrFr05kLzcSr4qKAq7YN7e9jwQRb23nfa6c9d-StnImGyFDb"
	        + "Sv04uVuxIp5Zms1gNxKKK2Da14B8S4rzVRltdYwam_lDp5XnZAYpQdb76FdIKLaV"
	        + "mqgfwX7XWRxv2322i-vDxRfqNzo_tETKzpVLzfiwQyeyPGLBIO56YJ7eObdv0je8"
	        + "1860ppamavo35UgoRdbYaBcoh9QcfylQr66oc6vFWXRcZ_ZT2LawVCWTIy3brGPi"
	        + "6UklfCpIMfIjf7iGdXKHzg."
	        + "48V1_ALb6US04U3b."
	        + "5eym8TW_c8SuK0ltJ3rpYIzOeDQz7TALvtu6UG9oMo4vpzs9tX_EFShS8iB7j6ji"
	        + "SdiwkIr3ajwQzaBtQD_A."
	        + "XFBoMYUZodetZdvTiFvSkQ";
	
    
	public static final int[] A2_PLAIN_TEXT = {76, 105, 118, 101, 32, 108, 111, 110,
			103, 32, 97, 110, 100, 32, 112, 114, 111, 115, 112, 101, 114, 46};

	public static final int[] A2_CEK = {4, 211, 31, 197, 84, 157, 252, 254, 11, 100,
			157, 250, 63, 170, 106, 206, 107, 124, 212, 45, 111, 107, 9, 219,
			200, 177, 0, 240, 143, 156, 44, 207};
	
	public static final String A2_IV = "AxY8DCtDaGlsbGljb3RoZQ";
	
	public static final String A2_COMPACT =      
			"eyJhbGciOiJSU0ExXzUiLCJlbmMiOiJBMTI4Q0JDLUhTMjU2In0."
			+ "UGhIOguC7IuEvf_NPVaXsGMoLOmwvc1GyqlIKOK1nN94nHPoltGRhWhw7Zx0-kFm"
		    + "1NJn8LE9XShH59_i8J0PH5ZZyNfGy2xGdULU7sHNF6Gp2vPLgNZ__deLKxGHZ7Pc"
		    + "HALUzoOegEI-8E66jX2E4zyJKx-YxzZIItRzC5hlRirb6Y5Cl_p-ko3YvkkysZIF"
		    + "NPccxRU7qve1WYPxqbb2Yw8kZqa2rMWI5ng8OtvzlV7elprCbuPhcCdZ6XDP0_F8"
		    + "rkXds2vE4X-ncOIM8hAYHHi29NX0mcKiRaD0-D-ljQTP-cFPgwCp6X-nZZd9OHBv"
		    + "-B3oWh2TbqmScqXMR4gp_A."
		    + "AxY8DCtDaGlsbGljb3RoZQ."
		    + "KDlTtXchhZTGufMYmOYGS4HffxPSUrfmqCHXaI9wOGY."
		    + "9hH0vgRfYgPnAHOd8stkvw";

	public static final int[] A3_PLAIN_TEXT = {76, 105, 118, 101, 32, 108, 111, 110,
			103, 32, 97, 110, 100, 32, 112, 114, 111, 115, 112, 101, 114, 46};
	
	public static final int[] A3_CEK = {4, 211, 31, 197, 84, 157, 252, 254, 11, 100,
			157, 250, 63, 170, 106, 206, 107, 124, 212, 45, 111, 107, 9, 219,
			200, 177, 0, 240, 143, 156, 44, 207};
	
	public static final String A3_IV = "AxY8DCtDaGlsbGljb3RoZQ";
	
	public static final int[] A3_AAD = {101, 121, 74, 104, 98, 71, 99, 105, 79, 105,
			74, 66, 77, 84, 73, 52, 83, 49, 99, 105, 76, 67, 74, 108, 98, 109,
			77, 105, 79, 105, 74, 66, 77, 84, 73, 52, 81, 48, 74, 68, 76, 85,
			104, 84, 77, 106, 85, 50, 73, 110, 48};
	
	public static final String A3_COMPACT = 
			"eyJhbGciOiJBMTI4S1ciLCJlbmMiOiJBMTI4Q0JDLUhTMjU2In0."
			+ "6KB707dM9YTIgHtLvtgWQ8mKwboJW3of9locizkDTHzBC2IlrT1oOQ."
			+ "AxY8DCtDaGlsbGljb3RoZQ."
			+ "KDlTtXchhZTGufMYmOYGS4HffxPSUrfmqCHXaI9wOGY."
			+ "U0m_YmjN04DJvceFICbCVQ";
	
	public static final String A4_JSON =
	        "{"
	        + "\"recipients\":"
	        + "["
	        + "{"
	        + "\"encrypted_key\":"
            + "\"UGhIOguC7IuEvf_NPVaXsGMoLOmwvc1GyqlIKOK1nN94nHPoltGRhWhw7Zx0-"
            + "kFm1NJn8LE9XShH59_i8J0PH5ZZyNfGy2xGdULU7sHNF6Gp2vPLgNZ__deLKx"
            + "GHZ7PcHALUzoOegEI-8E66jX2E4zyJKx-YxzZIItRzC5hlRirb6Y5Cl_p-ko3"
            + "YvkkysZIFNPccxRU7qve1WYPxqbb2Yw8kZqa2rMWI5ng8OtvzlV7elprCbuPh"
            + "cCdZ6XDP0_F8rkXds2vE4X-ncOIM8hAYHHi29NX0mcKiRaD0-D-ljQTP-cFPg"
            + "wCp6X-nZZd9OHBv-B3oWh2TbqmScqXMR4gp_A\","
            + "\"header\":"
	        + "{\"alg\":\"RSA1_5\",\"kid\":\"2011-04-29\"}},"
            + "{"
            + "\"encrypted_key\":"
            + "\"6KB707dM9YTIgHtLvtgWQ8mKwboJW3of9locizkDTHzBC2IlrT1oOQ\","
            + "\"header\":"
	        + "{\"alg\":\"A128KW\",\"kid\":\"7\"}}"
	        + "],"
            + "\"unprotected\":{\"jku\":\"https://server.example.com/keys.jwks\"},"
            + "\"protected\":\"eyJlbmMiOiJBMTI4Q0JDLUhTMjU2In0\","
            + "\"iv\":"
	        + "\"AxY8DCtDaGlsbGljb3RoZQ\","
	        + "\"ciphertext\":"
	        + "\"KDlTtXchhZTGufMYmOYGS4HffxPSUrfmqCHXaI9wOGY\","
	        + "\"tag\":"
	        + "\"Mz-VPPyU4RlcuYv1IwIvzw\""
	        + "}";
	
    public static final String A5_FLAT =
              "{\"protected\":"
            + "\"eyJlbmMiOiJBMTI4Q0JDLUhTMjU2In0\","
            + "\"unprotected\":"
            + "{\"jku\":\"https://server.example.com/keys.jwks\"},"
            + "\"header\":"
            + "{\"alg\":\"A128KW\",\"kid\":\"7\"},"
            + "\"encrypted_key\":"
            + "\"6KB707dM9YTIgHtLvtgWQ8mKwboJW3of9locizkDTHzBC2IlrT1oOQ\","
            + "\"iv\":"
            + "\"AxY8DCtDaGlsbGljb3RoZQ\","
            + "\"ciphertext\":"
            + "\"KDlTtXchhZTGufMYmOYGS4HffxPSUrfmqCHXaI9wOGY\","
            + "\"tag\":"
            + "\"Mz-VPPyU4RlcuYv1IwIvzw\"}";
       
    @Test
    public void appendix1(){
    	
        byte[] message = JoseUtils.getByteArrayFromIntArray(A1_PLAIN_TEXT);
        
        JweProtectedHeader protectedHeader = new JweProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.RSA_OAEP);
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_256_GCM);
        
        SecretKey cek = new SecretKeySpec(JoseUtils.getByteArrayFromIntArray(A1_CEK), "AES");
        
        JweCookbookBypass bypass = new JweCookbookBypass();
        bypass.setBypassInitializationVectorForContentEncryption(A1_IV);
        bypass.setBypassContentEncryptionKey(cek);
       
        JwkKeySet jwks = new JwkKeySet(readKeySet("JweAppendixA1.txt"));
        List<JwkKey> keys = jwks.getKeys();
        JwkKey rsaKey = keys.get(0);
        
        JweDocument jwe = JweMaker.generateFromMessageForJoseCookBook(bypass, message, null, rsaKey, protectedHeader, null);
        
        assertEquals(A1_COMPACT.length(), jwe.getCompactSerialization().length());
    }
    
    @Test
    public void appendix2(){
    
    	JweProtectedHeader protectedHeader = new JweProtectedHeader();
    	protectedHeader.setAlgorithm(JwaAlgorithms.RSA1_5.getSpecName());
    	protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256.getSpecName());
    	
        byte[] message = JoseUtils.getByteArrayFromIntArray(A2_PLAIN_TEXT);
               
        SecretKey cek = new SecretKeySpec(JoseUtils.getByteArrayFromIntArray(A2_CEK), "AES");   
  
        JweCookbookBypass bypass = new JweCookbookBypass();
        bypass.setBypassContentEncryptionKey(cek);
        bypass.setBypassInitializationVectorForContentEncryption(A2_IV);

        JwkKeySet jwks = new JwkKeySet(readKeySet("JweAppendixA2.txt"));
        List<JwkKey> keys = jwks.getKeys();
        JwkKey rsaKey = keys.get(0);
        
        JweDocument jwe = JweMaker.generateFromMessageForJoseCookBook(bypass, message, null, rsaKey, protectedHeader, null);
        
        assertEquals(A2_COMPACT.length(), jwe.getCompactSerialization().length());
    }
    
    @Test
    public void appendix3(){
    	
    	JweProtectedHeader protectedHeader = new JweProtectedHeader();
    	protectedHeader.setAlgorithm(JwaAlgorithms.AES_128_KW.getSpecName());
    	protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256.getSpecName());
    	
        byte[] message = JoseUtils.getByteArrayFromIntArray(A3_PLAIN_TEXT);
        
        SecretKey cek = new SecretKeySpec(JoseUtils.getByteArrayFromIntArray(A3_CEK), "AES");
        
        JwkKeySet jwks = new JwkKeySet(readKeySet("JweAppendixA3.txt"));
        List<JwkKey> keys = jwks.getKeys();
        JwkKey symmKey = keys.get(0);  
        
        JweCookbookBypass bypass = new JweCookbookBypass();
        bypass.setBypassContentEncryptionKey(cek);
        bypass.setBypassInitializationVectorForContentEncryption(A3_IV);
        
        JweDocument jwe = JweMaker.generateFromMessageForJoseCookBook(bypass, message, null, symmKey, protectedHeader, null);

        assertEquals(A3_COMPACT, jwe.getCompactSerialization());
    }
    
    @Test
    public void appendix4(){

        JwePerRecipientUnprotectedHeader headerOne = new JwePerRecipientUnprotectedHeader();
        headerOne.setAlgorithm(JwaAlgorithms.RSA1_5);
        headerOne.setKeyId("2011-04-29");
        
        JwkKeySet jwks = new JwkKeySet(readKeySet("JweAppendixA2.txt"));
        List<JwkKey> keys = jwks.getKeys();
        JwkKey rsaKey = keys.get(0);
        
        JweRecipient firstRecipient = new JweRecipient(headerOne, rsaKey);
        
        JwePerRecipientUnprotectedHeader headerTwo = new JwePerRecipientUnprotectedHeader();
        headerTwo.setAlgorithm(JwaAlgorithms.AES_128_KW);
        headerTwo.setKeyId("7");
        
        jwks = new JwkKeySet(readKeySet("JweAppendixA3.txt"));
        keys = jwks.getKeys();
        JwkKey symmKey = keys.get(0); 
        
        JweRecipient secondRecipient = new JweRecipient(headerTwo, symmKey);
        
        JweProtectedHeader protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        
        JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
        
        try
        {
            unprotectedHeader.setJSONWebKeySet(new URL("https://server.example.com/keys.jwks"));
        } catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        
        byte[] message = JoseUtils.getByteArrayFromIntArray(A3_PLAIN_TEXT);
        
        SecretKey cek = new SecretKeySpec(JoseUtils.getByteArrayFromIntArray(A3_CEK), "AES");
        
        JweCookbookBypass bypass = new JweCookbookBypass();
        bypass.setBypassContentEncryptionKey(cek);
        bypass.setBypassInitializationVectorForContentEncryption(A3_IV);
        
        JweDocument jwe = JweMaker.generateFromMessageForJoseCookBook(bypass, message, null, null, protectedHeader, unprotectedHeader, firstRecipient, secondRecipient);

        assertEquals(A4_JSON.length(), jwe.getJsonSerialization().length());
    } 
    
    @Test
    public void appendix5(){
        
        JwePerRecipientUnprotectedHeader headerTwo = new JwePerRecipientUnprotectedHeader();
        headerTwo.setAlgorithm(JwaAlgorithms.AES_128_KW);
        headerTwo.setKeyId("7");
        
        JwkKeySet jwks = new JwkKeySet(readKeySet("JweAppendixA3.txt"));
        List<JwkKey> keys = jwks.getKeys();
        JwkKey symmKey = keys.get(0); 
        
        JweRecipient secondRecipient = new JweRecipient(headerTwo, symmKey);
        
        JweProtectedHeader protectedHeader = new JweProtectedHeader();
        protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_CBC_HMAC_SHA_256);
        
        JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
        
        try
        {
            unprotectedHeader.setJSONWebKeySet(new URL("https://server.example.com/keys.jwks"));
        } catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        
        byte[] message = JoseUtils.getByteArrayFromIntArray(A3_PLAIN_TEXT);
        
        SecretKey cek = new SecretKeySpec(JoseUtils.getByteArrayFromIntArray(A3_CEK), "AES");
        
        JweCookbookBypass bypass = new JweCookbookBypass();
        bypass.setBypassContentEncryptionKey(cek);
        bypass.setBypassInitializationVectorForContentEncryption(A3_IV);
        
        JweDocument jwe = JweMaker.generateFromMessageForJoseCookBook(bypass, message, null, null, protectedHeader, unprotectedHeader, secondRecipient);
       
        assertEquals(A5_FLAT, jwe.getJsonFlattenedSerialization());
    }
    
    private String readKeySet(String fileName){
        InputStream is = this.getClass().getClassLoader()
                .getResourceAsStream(fileName);
        StringBuilder sb = new StringBuilder(1024);

        try {
            for (int i = is.read(); i != -1; i = is.read()) {
                sb.append((char) i);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            is.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return sb.toString();
    }
    
}

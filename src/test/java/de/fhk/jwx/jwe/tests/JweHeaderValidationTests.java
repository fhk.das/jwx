/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwe.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;

import de.fhk.jwx.JoseConstants;
import de.fhk.jwx.JoseHeaders;
import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jwe.JoseException;
import de.fhk.jwx.jwe.JweDocument;
import de.fhk.jwx.jwe.JweMaker;
import de.fhk.jwx.jwe.JwePerRecipientUnprotectedHeader;
import de.fhk.jwx.jwe.JweProtectedHeader;
import de.fhk.jwx.jwe.JweRecipient;
import de.fhk.jwx.jwe.JweSharedUnprotectedHeader;
import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.jwk.JwkKeySet;

public class JweHeaderValidationTests
{
    private final byte[] PAYLOAD = ("You can trust us to stick with you through thick and thin\\xe2\\x80\\x93to the bitter end. "
            + "And you can trust us to keep any secret of yours\\xe2\\x80\\x93closer than you keep it yourself. "
            + "But you cannot trust us to let you face trouble alone, and go off without a word. We are your friends, Frodo.").getBytes();

    private final JwkKey CEK = new JwkKeySet(readKeySet("cookbookSecretSet.txt")).getKeys().get(2);

    private final JwaAlgorithms enc = JwaAlgorithms.AES_128_GCM;
    private final JwaAlgorithms enc2 = JwaAlgorithms.AES_192_GCM;
    private final JwaAlgorithms enc3 = JwaAlgorithms.AES_256_GCM;

    private final JwaAlgorithms alg = JwaAlgorithms.AES_128_KW;
    private final JwaAlgorithms alg2 = JwaAlgorithms.AES_192_KW;
    private final JwaAlgorithms alg3 = JwaAlgorithms.AES_256_KW;
    private final String noSpecAlg = "noSpecAlg";
    private final String noSpecEnc = "noSpecEnc";

    private final String keyId1 = "T25lIHRvZGF5IGlzIHdvcnRoIHR3byB0b21vcnJvd3M=";
    private final String keyId2 = "key2";
    private final String keyId3 = "key3";

    private final String errorMessage001 = "Content Encryption Algorithm is null";
    private final String errorMessage002 = "Key Management Algorithm is null";
    private final String errorMessage003 = "Content Encryption Algorithm value \"noSpecEnc\" is not allowed";
    private final String errorMessage004 = "Key Management Algorithm value \"noSpecAlg\" is not allowed";
    private final String errorMessage005 = "Critical Header is not allowed in unprotected header";
    private final String errorMessage006 = "double entry \"enc\" must not be set in protected and unprotected header";
    private final String errorMessage007 = "double entry \"alg\" must not be set in protected and unprotected header";
    private final String errorMessage008 = "double entry \"kid\" must not be set in protected and unprotected header";
    private final String errorMessage009 = "Content Encryption Algorithm must be the same for every recipient. \"A192GCM\" does not equal \"A128GCM\"";
    private final String errorMessage010 = "double entry \"enc\" must not be set in global and per recipient header";
    private final String errorMessage011 = "double entry \"alg\" must not be set in global and per recipient header";
    private final String errorMessage012 = "double entry \"kid\" must not be set in global and per recipient header";
    private final String errorMessage013 = "double entry \"cty\" must not be set in protected and unprotected header";
    private final String errorMessage014 = "The critical header \"EXP\" is not supported by this JOSE implementation";
       
    /**
     * Case1/7: Only JWE Protected Header: All headers are in the JWE Protected
     * Header.
     */
    @SuppressWarnings("unused")
    @Test
    public void testCase1()
    {   
        // case1.1: enc and alg valid
        try
        {
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setContentEncryptionAlgorithm(enc);
            protectedHeader.setAlgorithm(alg);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader);

        } catch (Exception e)
        {
            System.out.println(e);
        }

//      +------------------------+----------+
//      | "enc" header placed in | case a)  |
//      +------------------------+----------+
//      | pH                     | required |
//      +------------------------+----------+
        
        // case1.2: enc missing
        try
        {
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setAlgorithm(alg);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader);
            fail("case1.2)");

        } catch (JoseException e)
        {
            assertEquals(errorMessage001, e.getMessage());
        }

        // case1.3: No Spec enc
        try
        {
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setContentEncryptionAlgorithm(noSpecEnc);
            protectedHeader.setAlgorithm(alg);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader);
            fail("case1.3)");

        } catch (JoseException e)
        {
            assertEquals(errorMessage003, e.getMessage());
        }
        
//      +------------------------+----------+
//      | "alg" header placed in | case a)  |
//      +------------------------+----------+
//      | pH                     | required |
//      +------------------------+----------+
        
        // case1.4: alg missing
        try
        {
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setContentEncryptionAlgorithm(enc);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader);
            fail("case1.4)");

        } catch (JoseException e)
        {
            assertEquals(errorMessage002, e.getMessage());
        }

        // case1.5: No Spec alg
        try
        {
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setContentEncryptionAlgorithm(enc);
            protectedHeader.setAlgorithm(noSpecAlg);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader);
            fail("case1.5)");

        } catch (JoseException e)
        {
            assertEquals(errorMessage004, e.getMessage());
        }
        
//      +------------------------+----------+
//      | other header placed in | case a)  |
//      +------------------------+----------+
//      | pH                     | optional |
//      +------------------------+----------+
//      +--------------------------+----------+
//      | pH only header placed in | case a)  |
//      +--------------------------+----------+
//      | pH                       | optional |
//      +--------------------------+----------+
        
        // case1.6: optional other and pH only header
        try
        {
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setContentEncryptionAlgorithm(enc);
            protectedHeader.setAlgorithm(alg);
            protectedHeader.setZipAlgorithm("DEF");
            protectedHeader.setKeyId(keyId1);
            
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader);

        } catch (Exception e)
        {
            System.out.println(e);
        }
        
        // case1.7: no double entries possible in LinkedHashMap
        try
        {
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setContentEncryptionAlgorithm(enc);
            protectedHeader.setContentEncryptionAlgorithm(enc2);
            protectedHeader.setAlgorithm(alg);
            protectedHeader.setAlgorithm(alg2);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader);

        } catch (Exception e)
        {
            System.out.println(e);
        }
        
        // case1.8: unknown critical header test
        try
        {
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setContentEncryptionAlgorithm(enc);
            protectedHeader.setAlgorithm(alg);
            protectedHeader.addCriticalHeaderValue("alg");
            protectedHeader.addCriticalHeaderValue("enc");
            protectedHeader.addCriticalHeaderValue("EXP");

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader);

            fail("case1.8)");

        } catch (JoseException e)
        {
            assertEquals(errorMessage014, e.getMessage());
        }
    }

    /**
     * Case2/7: Only JWE Shared Unprotected Header: All headers are in the JWE
     * Shared Unprotected Header.
     */
    @SuppressWarnings("unused")
    @Test
    public void testCase2()
    {
        // case2.1: enc and alg valid
        try
        {
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentEncryptionAlgorithm(enc);
            unprotectedHeader.setAlgorithm(alg);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, unprotectedHeader);

        } catch (JoseException e)
        {
            System.out.println(e);
        }

//      +------------------------+----------+
//      | "enc" header placed in | case a)  |
//      +------------------------+----------+
//      | uH                     | required |
//      +------------------------+----------+
        
        // case2.2: enc missing
        try
        {
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();

            unprotectedHeader.setAlgorithm(alg);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, unprotectedHeader);
            fail("case2.2)");

        } catch (JoseException e)
        {
            assertEquals(errorMessage001, e.getMessage());
        }

        // case2.3): No Spec enc
        try
        {
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentEncryptionAlgorithm(noSpecEnc);
            unprotectedHeader.setAlgorithm(alg);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, unprotectedHeader);
            fail("case2.3)");

        } catch (JoseException e)
        {
            assertEquals(errorMessage003, e.getMessage());
        }
         
//      +------------------------+----------+
//      | "alg" header placed in | case a)  |
//      +------------------------+----------+
//      | uH                     | required |
//      +------------------------+----------+
        
        // case2.4: alg missing
        try
        {
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentEncryptionAlgorithm(enc);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, unprotectedHeader);
            fail("case2.4");

        } catch (JoseException e)
        {
            assertEquals(errorMessage002, e.getMessage());
        }
        
        // case2.5: No Spec alg
        try
        {
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentEncryptionAlgorithm(enc);
            unprotectedHeader.setAlgorithm(noSpecAlg);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, unprotectedHeader);
            fail("case2.5)");

        } catch (JoseException e)
        {
            assertEquals(errorMessage004, e.getMessage());
        }

//      +--------------------------+-----------+
//      | pH only header placed in | case a)   |
//      +--------------------------+-----------+
//      | uH                       | forbidden |
//      +--------------------------+-----------+
        
        // case2.6: protected only header in unprotected
        try
        {
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentEncryptionAlgorithm(enc);
            unprotectedHeader.setAlgorithm(alg);
            unprotectedHeader.setHeader(JoseHeaders.HEADER_CRITICAL, "EXP");

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, unprotectedHeader);
            fail("case2.6)");

        } catch (JoseException e)
        {
            assertEquals(errorMessage005, e.getMessage());
        }
    
        // +------------------------+----------+
        // | other header placed in | case a) |
        // +------------------------+----------+
        // | uH                     | optional |
        // +------------------------+----------+
 
        // case2.7: optional other and pH only header
        try
        {
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentEncryptionAlgorithm(enc);
            unprotectedHeader.setAlgorithm(alg);
            unprotectedHeader.setKeyId(keyId1);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, unprotectedHeader);

        } catch (JoseException e)
        {
            System.out.println(e);
        }
}
    
    // case1.7: no double entries possible in LinkedHashMap
    
    /**
     * Case3/7: Only global headers: The headers are in the JWE Protected Header
     * and JWE Shared Unprotected Header.
     */
    @SuppressWarnings("unused")
    @Test
    public void testCase3()
    {
        // case3.1: valid alg in protected & valid enc in protected
        try
        {
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setContentEncryptionAlgorithm(enc);
            protectedHeader.setAlgorithm(alg);
            protectedHeader.setZipAlgorithm("DEF");

            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setKeyId(keyId1);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader);

        } catch (JoseException e)
        {
            System.out.println(e);
        }

        // case3.2: valid alg in unprotected & valid enc in unprotected
        try
        {
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setKeyId(keyId1);

            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentEncryptionAlgorithm(enc);
            unprotectedHeader.setAlgorithm(alg);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader);

        } catch (JoseException e)
        {
            System.out.println(e);
        }

//      +------------------------+-----------+-----------+
//      | "enc" header placed in | case a)   | case b)   |
//      +------------------------+-----------+-----------+
//      | pH                     | required  | forbidden |
//      +------------------------+-----------+-----------+
//      | uH                     | forbidden | required  |
//      +------------------------+-----------+-----------+
      
      // case3.3: enc missing
      try
      {
          JweProtectedHeader protectedHeader = new JweProtectedHeader();
          protectedHeader.setAlgorithm(alg);

          JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
          unprotectedHeader.setKeyId(keyId1);

          JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader);
          fail("case3.3)");

          System.out.println(jwe.getJsonSerialization(true));
          
      } catch (JoseException e)
      {
          assertEquals(errorMessage001, e.getMessage());
      }
      
      // case3.4: enc in protected and unprotected
      try
      {
          JweProtectedHeader protectedHeader = new JweProtectedHeader();
          protectedHeader.setContentEncryptionAlgorithm(enc);
          protectedHeader.setAlgorithm(alg);

          JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
          unprotectedHeader.setContentEncryptionAlgorithm(enc);

          JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader);
          fail("case3.4)");

      } catch (JoseException e)
      {
          assertEquals(errorMessage006, e.getMessage());
      }
      
      // case3.5): no Spec enc in protected
      try
      {
          JweProtectedHeader protectedHeader = new JweProtectedHeader();
          protectedHeader.setContentEncryptionAlgorithm(noSpecEnc);
          protectedHeader.setAlgorithm(alg);

          JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
          unprotectedHeader.setKeyId(keyId1);

          JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader);
          fail("case3.5");

      } catch (JoseException e)
      {
          assertEquals(errorMessage003, e.getMessage());
      }
      
      // case3.6): no Spec enc in unprotected
      try
      {
          JweProtectedHeader protectedHeader = new JweProtectedHeader();
          protectedHeader.setAlgorithm(alg);

          JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
          unprotectedHeader.setContentEncryptionAlgorithm(noSpecEnc);

          JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader);
          fail("case3.6");

      } catch (JoseException e)
      {
          assertEquals(errorMessage003, e.getMessage());
      }
      
//        +------------------------+-----------+-----------+
//        | "alg" header placed in | case a)   | case b)   |
//        +------------------------+-----------+-----------+
//        | pH                     | required  | forbidden |
//        +------------------------+-----------+-----------+
//        | uH                     | forbidden | required  |
//        +------------------------+-----------+-----------+
        
        // case3.7: alg missing
        try
        {
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setZipAlgorithm("DEF");

            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentEncryptionAlgorithm(enc);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader);
            fail("case3.7");

        } catch (JoseException e)
        {
            assertEquals(errorMessage002, e.getMessage());
        }

        // case3.8: alg in protected and unprotected
        try
        {
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setContentEncryptionAlgorithm(enc);
            protectedHeader.setAlgorithm(alg);

            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setAlgorithm(alg);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader);
            fail("case3.8)");

        } catch (JoseException e)
        {
            assertEquals(errorMessage007, e.getMessage());
        }

        // case3.9: no Spec alg in protected
        try
        {
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setAlgorithm(noSpecAlg);

            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentEncryptionAlgorithm(enc);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader);
            fail("case3.9");

        } catch (JoseException e)
        {
            assertEquals(errorMessage004, e.getMessage());
        }

        // case3.10: no Spec alg in unprotected
        try
        {
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setContentEncryptionAlgorithm(enc);

            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setAlgorithm(noSpecAlg);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader);
            fail("case3.10");

        } catch (JoseException e)
        {
            assertEquals(errorMessage004, e.getMessage());
        }

//        +------------------------+-----------+-----------+
//        | other header placed in | case a)   | case b)   |
//        +------------------------+-----------+-----------+
//        | pH                     | optional  | forbidden |
//        +------------------------+-----------+-----------+
//        | uH                     | forbidden | optional  |
//        +------------------------+-----------+-----------+
        
        // case3.11: other header in protected and unprotected
        try
        {
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setContentEncryptionAlgorithm(enc);
            protectedHeader.setAlgorithm(alg);
            protectedHeader.setKeyId(keyId1);

            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setKeyId(keyId1);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader);
            fail("case3.11)");

        } catch (JoseException e)
        {
            assertEquals(errorMessage008, e.getMessage());
        }
        
//        +--------------------------+-----------+-----------+
//        | pH only header placed in | case a)   | case b)   |
//        +--------------------------+-----------+-----------+
//        | pH                       | optional  | optional  |
//        +--------------------------+-----------+-----------+
//        | uH                       | forbidden | forbidden |
//        +--------------------------+-----------+-----------+
        
        // case3.12): protected only entry in unprotected
        try
        {
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setContentEncryptionAlgorithm(enc);
            protectedHeader.setAlgorithm(alg);
            protectedHeader.setZipAlgorithm("DEF");

            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setHeader(JoseHeaders.HEADER_CRITICAL, "DEF");

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader);
            fail("case3.12)");

        } catch (JoseException e)
        {
            assertEquals(errorMessage005, e.getMessage());
        }
    }

    /**
     * Case4/7: "Only JWE Per-Recipient Unprotected Header/s": The JWE
     * Per-Recipient Unprotected Headers are in the "recipients" array.
     */
    @SuppressWarnings("unused")
    @Test
    public void testCase4()
    {
        // case4.1: same valid enc in all recipient headers & valid alg in all
        // recipient headers
        try
        {
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader1.setContentEncryptionAlgorithm(enc);
            perRecipientHeader1.setAlgorithm(alg);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setContentEncryptionAlgorithm(enc);
            perRecipientHeader2.setAlgorithm(alg2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc);
            perRecipientHeader3.setAlgorithm(alg3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3, null);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, recipient1, recipient2, recipient3);

        } catch (JoseException e)
        {
            System.out.println(e);
        }

//        +------------------------+------------------+
//        | "enc" header placed in | case a)          |
//        +------------------------+------------------+
//        | pRUH1                  | required (enc X) |
//        +------------------------+------------------+
//        | pRUH2                  | required (enc X) |
//        +------------------------+------------------+
//        | pRUHn                  | required (enc X) |
//        +------------------------+------------------+
        
        // case4.2: missing enc in one recipient header
        try
        {
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader1.setContentEncryptionAlgorithm(enc);
            perRecipientHeader1.setAlgorithm(alg);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setAlgorithm(alg2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc);
            perRecipientHeader3.setAlgorithm(alg3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3, null);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, recipient1, recipient2, recipient3);
            fail("");

        } catch (JoseException e)
        {
            assertEquals(errorMessage001, e.getMessage());
        }

        // case4.3: different enc
        try
        {
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader1.setContentEncryptionAlgorithm(enc);
            perRecipientHeader1.setAlgorithm(alg);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setContentEncryptionAlgorithm(enc);
            perRecipientHeader2.setAlgorithm(alg2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc2);
            perRecipientHeader3.setAlgorithm(alg3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3, null);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, recipient1, recipient2, recipient3);
            fail("4.3");

        } catch (JoseException e)
        {
            assertEquals(errorMessage009, e.getMessage());
        }

        // case4.4: no Spec enc
        try
        {
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader1.setContentEncryptionAlgorithm(enc);
            perRecipientHeader1.setAlgorithm(alg);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setContentEncryptionAlgorithm(enc);
            perRecipientHeader2.setAlgorithm(alg2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(noSpecEnc);
            perRecipientHeader3.setAlgorithm(alg3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3, null);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, recipient1, recipient2, recipient3);
            fail("4.4");

        } catch (JoseException e)
        {
            assertEquals(errorMessage003, e.getMessage());
        }

//        +------------------------+------------------+
//        | "alg" header placed in | case a)          |
//        +------------------------+------------------+
//        | pRUH1                  | required (alg X) |
//        +------------------------+------------------+
//        | pRUH2                  | required (alg X) |
//        +------------------------+------------------+
//        | pRUHn                  | required (alg Y) |
//        +------------------------+------------------+
        
        // case4.5: missing alg in one recipient header
        try
        {
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader1.setContentEncryptionAlgorithm(enc);
            perRecipientHeader1.setAlgorithm(alg);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setContentEncryptionAlgorithm(enc);
            perRecipientHeader2.setAlgorithm(alg);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3, null);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, recipient1, recipient2, recipient3);
            fail("4.5");

        } catch (JoseException e)
        {
            assertEquals(errorMessage002, e.getMessage());
        }

        // case4.6: no Spec alg in one recipient header
        try
        {
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader1.setContentEncryptionAlgorithm(enc);
            perRecipientHeader1.setAlgorithm(alg); 
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setContentEncryptionAlgorithm(enc);
            perRecipientHeader2.setAlgorithm(alg);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc);
            perRecipientHeader3.setAlgorithm(noSpecAlg);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3, null);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, recipient1, recipient2, recipient3);
            fail("4.6");

        } catch (JoseException e)
        {
            assertEquals(errorMessage004, e.getMessage());
        }
        
//        +--------------------------+-----------+
//        | pH only header placed in | case a)   |
//        +--------------------------+-----------+
//        | pRUH1                    | forbidden |
//        +--------------------------+-----------+
//        | pRUH2                    | forbidden |
//        +--------------------------+-----------+
//        | pRUHn                    | forbidden |
//        +--------------------------+-----------+
        
        // case4.7: protected only entry in unprotected
        try
        {
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader1.setContentEncryptionAlgorithm(enc);
            perRecipientHeader1.setAlgorithm(alg);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setContentEncryptionAlgorithm(enc);
            perRecipientHeader2.setAlgorithm(alg);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc);
            perRecipientHeader3.setAlgorithm(alg);
            perRecipientHeader3.setHeader(JoseHeaders.HEADER_CRITICAL, "DEF");
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3, null);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, recipient1, recipient2, recipient3);
            fail("4.7");

        } catch (JoseException e)
        {
            assertEquals(errorMessage005, e.getMessage());
        }
        
//        +------------------------+------------------+
//        | other header placed in | case a)          |
//        +------------------------+------------------+
//        | pRUH1                  | optional other X |
//        +------------------------+------------------+
//        | pRUH2                  | optional other X |
//        +------------------------+------------------+
//        | pRUHn                  | optional other Y |
//        +------------------------+------------------+
        
        // case4.8: protected only entry in unprotected
        try
        {
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader1.setContentEncryptionAlgorithm(enc);
            perRecipientHeader1.setAlgorithm(alg);
            perRecipientHeader1.setKeyId(keyId1);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setContentEncryptionAlgorithm(enc);
            perRecipientHeader2.setAlgorithm(alg);
            perRecipientHeader2.setKeyId(keyId1);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc);
            perRecipientHeader3.setAlgorithm(alg);
            perRecipientHeader3.setKeyId(keyId2);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3, null);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, recipient1, recipient2, recipient3);

        } catch (JoseException e)
        {
            System.out.println(e);
        }
    }

    /**
     * Case5/7: JWE Per-Recipient Unprotected Header/s with JWE Protected Header
     */
    @SuppressWarnings("unused")
    @Test
    public void testCase5()
    {
        // case5.1: global enc in protected & global alg in protected
        try
        {
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader1.setKeyId(keyId1);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setKeyId(keyId2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setKeyId(keyId3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3, null);

            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setContentEncryptionAlgorithm(enc);
            protectedHeader.setAlgorithm(alg);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, recipient1, recipient2, recipient3);

        } catch (JoseException e)
        {
            System.out.println(e);
        }

        // case5.2: same enc in all recipient headers & algs in per recipient
        // headers
        try
        {
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader1.setContentEncryptionAlgorithm(enc);
            perRecipientHeader1.setAlgorithm(alg);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setContentEncryptionAlgorithm(enc);
            perRecipientHeader2.setAlgorithm(alg2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc);
            perRecipientHeader3.setAlgorithm(alg3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3, null);

            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setZipAlgorithm("DEF");

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, recipient1, recipient2, recipient3);

        } catch (JoseException e)
        {
            System.out.println(e);
        }

//        +------------------------+-----------+------------------+
//        | "enc" header placed in | case a)   | case b)          |
//        +------------------------+-----------+------------------+
//        | pH                     | required  | forbidden        |
//        +------------------------+-----------+------------------+
//        | pRUH1                  | forbidden | required (enc X) |
//        +------------------------+-----------+------------------+
//        | pRUH2                  | forbidden | required (enc X) |
//        +------------------------+-----------+------------------+
//        | pRUHn                  | forbidden | required (enc X) |
//        +------------------------+-----------+------------------+
        
        // case5.3: enc missing
        try
        {
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader1.setKeyId(keyId1);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setKeyId(keyId2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setKeyId(keyId3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3, null);

            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setAlgorithm(alg);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, recipient1, recipient2, recipient3);
            fail("5.3");

        } catch (JoseException e)
        {
            assertEquals(errorMessage001, e.getMessage());
        }

        // case5.4: different enc in per recipient headers
        try
        {
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader1.setContentEncryptionAlgorithm(enc);
            perRecipientHeader1.setAlgorithm(alg);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setContentEncryptionAlgorithm(enc);
            perRecipientHeader2.setAlgorithm(alg2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc2);
            perRecipientHeader3.setAlgorithm(alg3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3, null);

            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setZipAlgorithm("DEF");

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, recipient1, recipient2, recipient3);
            fail("5.4");

        } catch (JoseException e)
        {
            assertEquals(errorMessage009, e.getMessage());
        }

        // case5.5: no Spec enc in protected
        try
        {
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader1.setAlgorithm(alg);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setAlgorithm(alg2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setAlgorithm(alg3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3, null);

            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setContentEncryptionAlgorithm(noSpecEnc);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, recipient1, recipient2, recipient3);
            fail("5.5");

        } catch (JoseException e)
        {
            assertEquals(errorMessage003, e.getMessage());
        }

        // case5.6: no Spec enc in per recipient header
        try
        {
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader1.setContentEncryptionAlgorithm(enc);
            perRecipientHeader1.setAlgorithm(alg);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setContentEncryptionAlgorithm(noSpecEnc);
            perRecipientHeader2.setAlgorithm(alg2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc);
            perRecipientHeader3.setAlgorithm(alg3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3, null);

            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setZipAlgorithm(JoseHeaders.JWE_HEADER_ZIP_ALGORITHM);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, recipient1, recipient2, recipient3);
            fail("5.6");

        } catch (JoseException e)
        {
            assertEquals(errorMessage003, e.getMessage());
        }
        
        // case5.7: enc in protected and in one per recipient header
        try
        {
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader1.setKeyId(keyId1);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setKeyId(keyId2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3, null);

            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setContentEncryptionAlgorithm(enc);
            protectedHeader.setAlgorithm(alg);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, recipient1, recipient2, recipient3);
            fail("5.7");

        } catch (JoseException e)
        {
            assertEquals(errorMessage010, e.getMessage());
        }
        
//      +------------------------+-----------+------------------+
//      | "alg" header placed in | case a)   | case b)          |
//      +------------------------+-----------+------------------+
//      | pH                     | required  | forbidden        |
//      +------------------------+-----------+------------------+
//      | pRUH1                  | forbidden | required (alg X) |
//      +------------------------+-----------+------------------+
//      | pRUH2                  | forbidden | required (alg X) |
//      +------------------------+-----------+------------------+
//      | pRUHn                  | forbidden | required (alg Y) |
//      +------------------------+-----------+------------------+       
             
        // "case5.8: alg missing"
        try
        {
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader1.setKeyId(keyId1);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setKeyId(keyId2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setKeyId(keyId3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3, null);

            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setContentEncryptionAlgorithm(enc);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, recipient1, recipient2, recipient3);
            fail("5.8");

        } catch (JoseException e)
        {
            assertEquals(errorMessage002, e.getMessage());
        }

        // case5.9: no Spec alg in protected
        try
        {
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader1.setContentEncryptionAlgorithm(enc);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setContentEncryptionAlgorithm(enc);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3, null);

            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setAlgorithm(noSpecAlg);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, recipient1, recipient2, recipient3);
            fail("5.9");

        } catch (JoseException e)
        {
            assertEquals(errorMessage004, e.getMessage());
        }

        // case5.10: no Spec alg in per recipient header
        try
        {
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader1.setAlgorithm(alg);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setAlgorithm(alg2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setAlgorithm(noSpecAlg);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3, null);

            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setContentEncryptionAlgorithm(enc);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, recipient1, recipient2, recipient3);
            fail("5.10");

        } catch (JoseException e)
        {
            assertEquals(errorMessage004, e.getMessage());
        }
        
        // case5.11: alg in protected and in one per recipient header
        try
        {
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader1.setKeyId(keyId1);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setKeyId(keyId2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setAlgorithm(alg);
            perRecipientHeader3.setKeyId(keyId3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3, null);

            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setContentEncryptionAlgorithm(enc);
            protectedHeader.setAlgorithm(alg);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, recipient1, recipient2, recipient3);
            fail("5.11");

        } catch (JoseException e)
        {
            assertEquals(errorMessage011, e.getMessage());
        }

//      +--------------------------+-----------+
//      | pH only header placed in | case a)   |
//      +--------------------------+-----------+
//      | pH                       | optional  |
//      +--------------------------+-----------+
//      | pRUH1                    | forbidden |
//      +--------------------------+-----------+
//      | pRUH2                    | forbidden |
//      +--------------------------+-----------+
//      | pRUHn                    | forbidden |
//      +--------------------------+-----------+

        // case5.12: protected only entry in per recipient
        try
        {
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader1.setContentEncryptionAlgorithm(enc);
            perRecipientHeader1.setAlgorithm(alg);
            perRecipientHeader1.setKeyId(keyId1);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setContentEncryptionAlgorithm(enc);
            perRecipientHeader2.setAlgorithm(alg);
            perRecipientHeader2.setKeyId(keyId2);
            perRecipientHeader2.setHeader(JoseHeaders.HEADER_CRITICAL, "EXP");
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc);
            perRecipientHeader3.setAlgorithm(alg);
            perRecipientHeader3.setKeyId(keyId3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3, null);

            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setZipAlgorithm(JoseHeaders.JWE_HEADER_ZIP_ALGORITHM);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, recipient1, recipient2, recipient3);
            fail("5.12");

        } catch (JoseException e)
        {
            assertEquals(errorMessage005, e.getMessage());
        }
      
//        +------------------------+-----------+--------------------+
//        | other header placed in | case a)   | case b)            |
//        +------------------------+-----------+--------------------+
//        | pH                     | optional  | forbidden          |
//        +------------------------+-----------+--------------------+
//        | pRUH1                  | forbidden | optional (other X) |
//        +------------------------+-----------+--------------------+
//        | pRUH2                  | forbidden | optional (other X) |
//        +------------------------+-----------+--------------------+
//        | pRUHn                  | forbidden | optional (other Y) |
//        +------------------------+-----------+--------------------+
        
        // case5.13: other in protected and in per recipient header
        try
        {
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader1.setContentEncryptionAlgorithm(enc);
            perRecipientHeader1.setAlgorithm(alg);
            perRecipientHeader1.setKeyId(keyId1);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setContentEncryptionAlgorithm(enc);
            perRecipientHeader2.setAlgorithm(alg2);
            perRecipientHeader2.setKeyId(keyId2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2, null);

            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc);
            perRecipientHeader3.setAlgorithm(alg3);
            perRecipientHeader3.setKeyId(keyId3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3, null);

            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setType(JoseConstants.MEDIA_TYPE_JOSE_JSON);
            protectedHeader.setKeyId(keyId1);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, recipient1, recipient2, recipient3);
            fail("5.13");

        } catch (JoseException e)
        {
            assertEquals(errorMessage012, e.getMessage());
        }
    }     
    
    /**
     * Case6/7 JWE Per-Recipient Unprotected Header/s with JWE Shared Unprotected Header
     */
    @SuppressWarnings("unused")
    @Test
  public void testCase6() {
        
        // case6.1: global enc in unprotected & global alg in unprotected
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setKeyId(keyId1);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setKeyId(keyId2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setKeyId(keyId3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
            
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentEncryptionAlgorithm(enc);
            unprotectedHeader.setAlgorithm(alg);
            
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, unprotectedHeader, recipient1, recipient2, recipient3);

        } catch (JoseException e) {
            System.out.println(e);
        }
        
        // case6.2: same enc in all recipient headers & algs in per recipient headers
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader1.setContentEncryptionAlgorithm(enc); 
            perRecipientHeader1.setAlgorithm(alg);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setContentEncryptionAlgorithm(enc);
            perRecipientHeader2.setAlgorithm(alg2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc);
            perRecipientHeader3.setAlgorithm(alg3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);   
            
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setType(JoseConstants.MEDIA_TYPE_JOSE_JSON);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, unprotectedHeader, recipient1, recipient2, recipient3);

        } catch (JoseException e) {
            System.out.println(e);
        }   
        
//      +------------------------+-----------+------------------+
//      | "enc" header placed in | case a)   | case b)          |
//      +------------------------+-----------+------------------+
//      | pH                     | required  | forbidden        |
//      +------------------------+-----------+------------------+
//      | pRUH1                  | forbidden | required (enc X) |
//      +------------------------+-----------+------------------+
//      | pRUH2                  | forbidden | required (enc X) |
//      +------------------------+-----------+------------------+
//      | pRUHn                  | forbidden | required (enc X) |
//      +------------------------+-----------+------------------+
                   
        // case6.3: enc missing
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setKeyId(keyId1);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setKeyId(keyId2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setKeyId(keyId3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
            
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setAlgorithm(alg);

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, unprotectedHeader, recipient1, recipient2, recipient3);            
            fail("6.3");
            
        } catch (JoseException e) {
            assertEquals(errorMessage001, e.getMessage());
        }   
        
        // case6.4: different enc in per recipient headers");
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader1.setContentEncryptionAlgorithm(enc);
            perRecipientHeader1.setAlgorithm(alg);    
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setContentEncryptionAlgorithm(enc);
            perRecipientHeader2.setAlgorithm(alg2);   
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc2);
            perRecipientHeader3.setAlgorithm(alg3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
       
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setType(JoseConstants.MEDIA_TYPE_JOSE_JSON);
       
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, unprotectedHeader, recipient1, recipient2, recipient3);
            fail("6.4");
            
        } catch (JoseException e) {
            assertEquals(errorMessage009, e.getMessage());
        }   
        
        // case6.5: no Spec enc in unprotected
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setAlgorithm(alg);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setAlgorithm(alg2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setAlgorithm(alg3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);

            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentEncryptionAlgorithm(noSpecEnc);
 
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, unprotectedHeader, recipient1, recipient2, recipient3);
            fail("6.5");

        } catch (JoseException e) {
            assertEquals(errorMessage003, e.getMessage());
        }
        
        // case6.6: no Spec enc in per recipient header
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setContentEncryptionAlgorithm(enc);
            perRecipientHeader1.setAlgorithm(alg);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setContentEncryptionAlgorithm(noSpecEnc);
            perRecipientHeader2.setAlgorithm(alg2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc);
            perRecipientHeader3.setAlgorithm(alg3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
            
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setType(JoseConstants.MEDIA_TYPE_JOSE_JSON);
            
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, unprotectedHeader, recipient1, recipient2, recipient3);
            fail("6.6");

        } catch (JoseException e) {
            assertEquals(errorMessage003, e.getMessage());
        }
           
        // case6.7: enc in unprotected and in one per recipient header
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setKeyId(keyId1);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setKeyId(keyId2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setKeyId(keyId3);
            perRecipientHeader3.setContentEncryptionAlgorithm(enc);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
            
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setAlgorithm(alg);
            unprotectedHeader.setContentEncryptionAlgorithm(enc);
            
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, unprotectedHeader, recipient1, recipient2, recipient3);
            fail("6.7");

        } catch (JoseException e) {
            assertEquals(errorMessage010, e.getMessage());
        }   
  
//      +------------------------+-----------+------------------+
//      | "alg" header placed in | case a)   | case b)          |
//      +------------------------+-----------+------------------+
//      | pH                     | required  | forbidden        |
//      +------------------------+-----------+------------------+
//      | pRUH1                  | forbidden | required (alg X) |
//      +------------------------+-----------+------------------+
//      | pRUH2                  | forbidden | required (alg X) |
//      +------------------------+-----------+------------------+
//      | pRUHn                  | forbidden | required (alg Y) |
//      +------------------------+-----------+------------------+ 
              
        // case6.8: alg missing
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setKeyId(keyId1);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setKeyId(keyId2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setKeyId(keyId3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
            
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentEncryptionAlgorithm(enc);
            
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, unprotectedHeader, recipient1, recipient2, recipient3);
            fail("");
            
        } catch (JoseException e) {
            assertEquals(errorMessage002, e.getMessage());
        }   
        
        // case6.9: no Spec alg in unprotected
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setKeyId(keyId1);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setKeyId(keyId1);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setKeyId(keyId1);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
            
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentEncryptionAlgorithm(enc);
            unprotectedHeader.setAlgorithm(noSpecAlg);
            
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, unprotectedHeader, recipient1, recipient2, recipient3);
            fail("6.9");

        } catch (JoseException e) {
            assertEquals(errorMessage004, e.getMessage());
        }
        
        // case6.10: no Spec alg in per recipient header");
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setContentEncryptionAlgorithm(enc);
            perRecipientHeader1.setAlgorithm(alg);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setContentEncryptionAlgorithm(enc);
            perRecipientHeader2.setAlgorithm(alg2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc);
            perRecipientHeader3.setAlgorithm(noSpecAlg);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);

            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setType(JoseConstants.MEDIA_TYPE_JOSE_JSON);
        
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, unprotectedHeader, recipient1, recipient2, recipient3);
            fail("6.10");

        } catch (JoseException e) {
            assertEquals(errorMessage004, e.getMessage());
        }
        
        // case6.11: alg in unprotected and in one per recipient header
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setKeyId(keyId1);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setKeyId(keyId1);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setKeyId(keyId1);
            perRecipientHeader3.setAlgorithm(alg);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);

            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentEncryptionAlgorithm(enc);
            unprotectedHeader.setAlgorithm(alg);
            
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, unprotectedHeader, recipient1, recipient2, recipient3);
            fail("6.11");
            
        } catch (JoseException e) {
            assertEquals(errorMessage011, e.getMessage());
        }   
        
//      +--------------------------+-----------+
//      | pH only header placed in | case a)   |
//      +--------------------------+-----------+
//      | pH                       | forbidden |
//      +--------------------------+-----------+
//      | pRUH1                    | forbidden |
//      +--------------------------+-----------+
//      | pRUH2                    | forbidden |
//      +--------------------------+-----------+
//      | pRUHn                    | forbidden |
//      +--------------------------+-----------+  
        
        // case6.12: protected only entry in per recipient
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setContentEncryptionAlgorithm(enc);
            perRecipientHeader1.setAlgorithm(alg);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setContentEncryptionAlgorithm(enc); 
            perRecipientHeader2.setAlgorithm(alg2);     
            perRecipientHeader2.setHeader(JoseHeaders.HEADER_CRITICAL, "EXP");
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc);
            perRecipientHeader3.setAlgorithm(alg3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
            
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setType(JoseConstants.MEDIA_TYPE_JOSE_JSON);
     
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, unprotectedHeader, recipient1, recipient2, recipient3);
            fail("6.12");

        } catch (JoseException e) {
            assertEquals(errorMessage005, e.getMessage());
        }
        
        // case6.13: protected only entry in unprotected");
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setContentEncryptionAlgorithm(enc);
            perRecipientHeader1.setAlgorithm(alg);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setContentEncryptionAlgorithm(enc);
            perRecipientHeader2.setAlgorithm(alg);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc);
            perRecipientHeader3.setAlgorithm(alg);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);

            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setHeader(JoseHeaders.HEADER_CRITICAL, "EXP");
            
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, unprotectedHeader, recipient1, recipient2, recipient3);
            fail("6.13");
            
        } catch (JoseException e) {
            assertEquals(errorMessage005, e.getMessage());
        }
            
//      +------------------------+-----------+--------------------+
//      | other header placed in | case a)   | case b)            |
//      +------------------------+-----------+--------------------+
//      | pH                     | optional  | forbidden          |
//      +------------------------+-----------+--------------------+
//      | pRUH1                  | forbidden | optional (other X) |
//      +------------------------+-----------+--------------------+
//      | pRUH2                  | forbidden | optional (other X) |
//      +------------------------+-----------+--------------------+
//      | pRUHn                  | forbidden | optional (other Y) |
//      +------------------------+-----------+--------------------+
        
        // case6.14: other in unprotected and in per recipient header
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setContentEncryptionAlgorithm(enc);
            perRecipientHeader1.setAlgorithm(alg);
            perRecipientHeader1.setKeyId(keyId1);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setContentEncryptionAlgorithm(enc);
            perRecipientHeader2.setAlgorithm(alg2);
            perRecipientHeader2.setKeyId(keyId2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc);
            perRecipientHeader3.setAlgorithm(alg3);
            perRecipientHeader3.setKeyId(keyId3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
            
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setKeyId(keyId2);
            
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, unprotectedHeader, recipient1, recipient2, recipient3);
            fail("6.14");

        } catch (JoseException e) {
            assertEquals(errorMessage012, e.getMessage());
        }   
    }

    /**
     * Case7/7: All Headers are used
     */
    @SuppressWarnings("unused")
    @Test
  public void testCase7() {
        
        //case7.1: global enc in protected & global alg in protected
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setKeyId(keyId1);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setKeyId(keyId2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setKeyId(keyId3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
            
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentType(JoseConstants.MEDIA_TYPE_JOSE_JSON);
                    
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setContentEncryptionAlgorithm(enc);
            protectedHeader.setAlgorithm(alg);            
            
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader, recipient1, recipient2, recipient3);
            
        } catch (JoseException e) {
            System.out.println(e);
        }
        
        // case7.2: global enc in unprotected & global alg in unprotected
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setKeyId(keyId1);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setKeyId(keyId2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setKeyId(keyId3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
            
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentEncryptionAlgorithm(enc);
            unprotectedHeader.setAlgorithm(alg);
            
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setZipAlgorithm("DEF");
 
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader, recipient1, recipient2, recipient3);

        } catch (JoseException e) {
            System.out.println(e);
        }
        
        // case7.3: same enc in all per recipient headers & algs in per recipient headers
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader1.setContentEncryptionAlgorithm(enc); 
            perRecipientHeader1.setAlgorithm(alg);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setContentEncryptionAlgorithm(enc);
            perRecipientHeader2.setAlgorithm(alg2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc);
            perRecipientHeader3.setAlgorithm(alg3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);

            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentType(JoseConstants.MEDIA_TYPE_JOSE_JSON);
            
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setZipAlgorithm("DEF");
     
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader, recipient1, recipient2, recipient3);
   
        } catch (JoseException e) {
            System.out.println(e);
        }   
        
//        +-----------------+-----------+-----------+------------------+
//        | "enc" placed in | case a)   | case b)   | case c)          |
//        +-----------------+-----------+-----------+------------------+
//        | pH              | required  | forbidden | forbidden        |
//        +-----------------+-----------+-----------+------------------+
//        | uH              | forbidden | required  | forbidden        |
//        +-----------------+-----------+-----------+------------------+
//        | pRUH1           | forbidden | forbidden | required (alg X) |
//        +-----------------+-----------+-----------+------------------+
//        | pRUH2           | forbidden | forbidden | required (alg X) |
//        +-----------------+-----------+-----------+------------------+
//        | pRUHn           | forbidden | forbidden | required (alg X) |
//        +-----------------+-----------+-----------+------------------+
        
        //case7.4: enc missing
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setKeyId(keyId1);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setKeyId(keyId1);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setKeyId(keyId1);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
            
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setAlgorithm(alg);
            
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setZipAlgorithm("DEF");
            
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader, recipient1, recipient2, recipient3);            
            fail("7.4");
            
        } catch (JoseException e) {
            assertEquals(errorMessage001, e.getMessage());
        }   
        
        // case7.5: different enc in per recipient headers
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader1.setContentEncryptionAlgorithm(enc);
            perRecipientHeader1.setAlgorithm(alg);    
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setContentEncryptionAlgorithm(enc);
            perRecipientHeader2.setAlgorithm(alg2);   
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc2);
            perRecipientHeader3.setAlgorithm(alg3);   
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
  
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentType(JoseConstants.MEDIA_TYPE_JOSE_JSON);
            
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setZipAlgorithm("DEF");
            
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader, recipient1, recipient2, recipient3);
            fail("7.5");
            
        } catch (JoseException e) {
            assertEquals(errorMessage009, e.getMessage());
        }   
        
        // case7.6: no Spec enc in protected");
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setAlgorithm(alg);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setAlgorithm(alg2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setAlgorithm(alg3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
      
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentType(JoseConstants.MEDIA_TYPE_JOSE_JSON);
            
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setContentEncryptionAlgorithm(noSpecEnc);
                
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader, recipient1, recipient2, recipient3);
            fail("7.6");

        } catch (JoseException e) {
            assertEquals(errorMessage003, e.getMessage());
        }
        
        //case7.7: no Spec enc in unprotected
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setAlgorithm(alg);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setAlgorithm(alg2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setAlgorithm(alg3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
         
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentEncryptionAlgorithm(noSpecEnc);
            
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setZipAlgorithm("DEF");
       
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader, recipient1, recipient2, recipient3);
            fail("7.7");

        } catch (JoseException e) {
            assertEquals(errorMessage003, e.getMessage());
        }
        
        // case7.8: no spec enc in one per recipient headers
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader1.setContentEncryptionAlgorithm(enc); 
            perRecipientHeader1.setAlgorithm(alg);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setContentEncryptionAlgorithm(enc);
            perRecipientHeader2.setAlgorithm(alg2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(noSpecEnc);
            perRecipientHeader3.setAlgorithm(alg3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
        
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentType(JoseConstants.MEDIA_TYPE_JOSE_JSON);
            
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setZipAlgorithm("DEF");
         
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader, recipient1, recipient2, recipient3);
            fail("7.8");

        } catch (JoseException e) {
            assertEquals(errorMessage003, e.getMessage());
        }   
        
        // case7.9: enc in protected and in unprotected
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setKeyId(keyId1);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setKeyId(keyId2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setKeyId(keyId3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
        
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentEncryptionAlgorithm(enc);
            unprotectedHeader.setAlgorithm(alg);

            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setContentEncryptionAlgorithm(enc);
    
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader, recipient1, recipient2, recipient3);
            fail("7.9");

        } catch (JoseException e) {
            assertEquals(errorMessage006, e.getMessage());
        }   
        
        // case7.10: enc in protected and in one per recipient header
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setKeyId(keyId1);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setKeyId(keyId2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc);
            perRecipientHeader3.setKeyId(keyId3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
      
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setAlgorithm(alg);
                     
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setContentEncryptionAlgorithm(enc);
                       
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader, recipient1, recipient2, recipient3);
            fail("7.10");
            
        } catch (JoseException e) {
            assertEquals(errorMessage010, e.getMessage());
        }   
        
        // case7.11: enc in unprotected and in one per recipient header
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setKeyId(keyId1);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setKeyId(keyId2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc);
            perRecipientHeader3.setKeyId(keyId3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
    
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentEncryptionAlgorithm(enc);
            
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            unprotectedHeader.setAlgorithm(alg);
            protectedHeader.setZipAlgorithm("DEF");
         
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader, recipient1, recipient2, recipient3);
            fail("7.11");

        } catch (JoseException e) {
            assertEquals(errorMessage010, e.getMessage());
        }   
        
//        +-----------------+-----------+-----------+------------------+
//        | "alg" placed in | case a)   | case b)   | case c)          |
//        +-----------------+-----------+-----------+------------------+
//        | pH              | required  | forbidden | forbidden        |
//        +-----------------+-----------+-----------+------------------+
//        | uH              | forbidden | required  | forbidden        |
//        +-----------------+-----------+-----------+------------------+
//        | pRUH1           | forbidden | forbidden | required (alg X) |
//        +-----------------+-----------+-----------+------------------+
//        | pRUH2           | forbidden | forbidden | required (alg X) |
//        +-----------------+-----------+-----------+------------------+
//        | pRUHn           | forbidden | forbidden | required (alg Y) |
//        +-----------------+-----------+-----------+------------------+
        
        // case7.12: alg missing
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setKeyId(keyId1);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setKeyId(keyId2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setKeyId(keyId3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
         
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentEncryptionAlgorithm(enc);
            
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setZipAlgorithm("DEF");
            
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader, recipient1, recipient2, recipient3);
            fail("7.12");

        } catch (JoseException e) {
            assertEquals(errorMessage002, e.getMessage());
        }   
        
        // case7.13: no Spec alg in protected
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setKeyId(keyId1);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setKeyId(keyId2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setKeyId(keyId3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
  
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentEncryptionAlgorithm(enc);
            
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setAlgorithm(noSpecAlg);
            protectedHeader.setZipAlgorithm("DEF");
           
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader, recipient1, recipient2, recipient3);
            
            fail("7.13");
        } catch (JoseException e) {
            assertEquals(errorMessage004, e.getMessage());
        }
        
        // case7.14: no Spec alg in unprotected
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setKeyId(keyId1);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setKeyId(keyId2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setKeyId(keyId3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
  
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentEncryptionAlgorithm(enc);
            unprotectedHeader.setAlgorithm(noSpecAlg);
            
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setZipAlgorithm("DEF");
       
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader, recipient1, recipient2, recipient3);
            fail("7.14");

        } catch (JoseException e) {
            assertEquals(errorMessage004, e.getMessage());
        }
    
        // case7.15: no Spec alg in per recipient header
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setContentEncryptionAlgorithm(enc);
            perRecipientHeader1.setAlgorithm(alg);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setContentEncryptionAlgorithm(enc);
            perRecipientHeader2.setAlgorithm(alg2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc);
            perRecipientHeader3.setAlgorithm(noSpecAlg);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
       
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentType(JoseConstants.MEDIA_TYPE_JOSE_JSON);
            
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setZipAlgorithm("DEF");
           
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader, recipient1, recipient2, recipient3);
            fail("7.15");

        } catch (JoseException e) {
            assertEquals(errorMessage004, e.getMessage());
        }
        
        // case7.16: alg in protected and in unprotected
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setKeyId(keyId1);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setKeyId(keyId2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setKeyId(keyId3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
          
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentEncryptionAlgorithm(enc);
            unprotectedHeader.setAlgorithm(alg);
            
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setAlgorithm(alg);
            protectedHeader.setZipAlgorithm("DEF");
                     
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader, recipient1, recipient2, recipient3);
            fail("7.16");

        } catch (JoseException e) {
            assertEquals(errorMessage007, e.getMessage());
        }   
        
        // case7.17: alg in protected and in one per recipient header
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setKeyId(keyId1);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setKeyId(keyId2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setKeyId(keyId3);
            perRecipientHeader3.setAlgorithm(alg);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
        
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentEncryptionAlgorithm(enc);
            
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setAlgorithm(alg2);
            protectedHeader.setZipAlgorithm("DEF");
         
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader, recipient1, recipient2, recipient3);            
            fail("7.17");
            
        } catch (JoseException e) {
            assertEquals(errorMessage011, e.getMessage());
        }   
        
        // case7.18: alg in unprotected and in one per recipient header
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setKeyId(keyId1);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setKeyId(keyId2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setKeyId(keyId3);
            perRecipientHeader3.setAlgorithm(alg);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
  
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentEncryptionAlgorithm(enc);
            unprotectedHeader.setAlgorithm(alg);
            
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setZipAlgorithm("DEF");
      
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader, recipient1, recipient2, recipient3);
            fail("7.18");

        } catch (JoseException e) {
            assertEquals(errorMessage011, e.getMessage());
        }   
  
//      +--------------------------+-----------+
//      | pH only header placed in | case a)   |
//      +--------------------------+-----------+
//      | pH                       | optional  |
//      +--------------------------+-----------+
//      | pRUH1                    | forbidden |
//      +--------------------------+-----------+
//      | pRUH2                    | forbidden |
//      +--------------------------+-----------+
//      | pRUHn                    | forbidden |
//      +--------------------------+-----------+
        
        // case7.19: protected only entry in per recipient
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setContentEncryptionAlgorithm(enc);
            perRecipientHeader1.setAlgorithm(alg);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setContentEncryptionAlgorithm(enc);
            perRecipientHeader2.setAlgorithm(alg2);
            perRecipientHeader2.setHeader(JoseHeaders.HEADER_CRITICAL, "EXP");
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc);
            perRecipientHeader3.setAlgorithm(alg3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
      
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentType(JoseConstants.MEDIA_TYPE_JOSE_JSON);
            
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setZipAlgorithm("DEF");
            
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader, recipient1, recipient2, recipient3);
            fail("7.19");
            
        } catch (JoseException e) {
            assertEquals(errorMessage005, e.getMessage());
        }
    
        // case7.20: protected only entry in unprotected
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setContentEncryptionAlgorithm(enc);
            perRecipientHeader1.setAlgorithm(alg);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setContentEncryptionAlgorithm(enc);
            perRecipientHeader2.setAlgorithm(alg2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc);
            perRecipientHeader3.setAlgorithm(alg3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
            
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentType(JoseConstants.MEDIA_TYPE_JOSE_JSON);
            unprotectedHeader.setHeader(JoseHeaders.HEADER_CRITICAL, "abc");
            
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setZipAlgorithm("DEF");

            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader, recipient1, recipient2, recipient3);
            fail("7.20");
        } catch (JoseException e) {
            assertEquals(errorMessage005, e.getMessage());
        }
        
//        +------------------------+-----------+-----------+--------------------+
//        | other header placed in | case a)   | case b)   | case c)            |
//        +------------------------+-----------+-----------+--------------------+
//        | pH                     | optional  | forbidden | forbidden          |
//        +------------------------+-----------+-----------+--------------------+
//        | uH                     | forbidden | optional  | forbidden          |
//        +------------------------+-----------+-----------+--------------------+
//        | pRUH1                  | forbidden | forbidden | optional (other X) |
//        +------------------------+-----------+-----------+--------------------+
//        | pRUH2                  | forbidden | forbidden | optional (other X) |
//        +------------------------+-----------+-----------+--------------------+
//        | pRUHn                  | forbidden | forbidden | optional (other Y) |
//        +------------------------+-----------+-----------+--------------------+
        
        // case7.21: other in protected and unprotected
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setContentEncryptionAlgorithm(enc);
            perRecipientHeader1.setAlgorithm(alg);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setContentEncryptionAlgorithm(enc);
            perRecipientHeader2.setAlgorithm(alg);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc);
            perRecipientHeader3.setAlgorithm(alg);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
            
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentType(JoseConstants.MEDIA_TYPE_JOSE_JSON);
            
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setZipAlgorithm("DEF");
            protectedHeader.setContentType(JoseConstants.MEDIA_TYPE_JOSE_JSON);
      
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader, recipient1, recipient2, recipient3);
            fail("7.21");

        } catch (JoseException e) {
            assertEquals(errorMessage013, e.getMessage());
        }   
        
        // case7.22: other in protected and in per recipient header
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setContentEncryptionAlgorithm(enc3);
            perRecipientHeader1.setAlgorithm(alg);
            perRecipientHeader1.setKeyId(keyId1);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setContentEncryptionAlgorithm(enc3);
            perRecipientHeader2.setAlgorithm(alg2);
            perRecipientHeader2.setKeyId(keyId2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc3);
            perRecipientHeader3.setAlgorithm(alg3);
            perRecipientHeader3.setKeyId(keyId3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
         
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setContentType(JoseConstants.MEDIA_TYPE_JOSE_JSON);
            
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setZipAlgorithm("DEF");
            protectedHeader.setKeyId(keyId1);
           
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader, recipient1, recipient2, recipient3); 
            fail("7.22");
            
        } catch (JoseException e) {
            assertEquals(errorMessage012, e.getMessage());
        }   
    
        // case7.23: other in unprotected and in per recipient header
        try {           
            JwePerRecipientUnprotectedHeader perRecipientHeader1 = new JwePerRecipientUnprotectedHeader(); 
            perRecipientHeader1.setContentEncryptionAlgorithm(enc);
            perRecipientHeader1.setAlgorithm(alg);
            perRecipientHeader1.setKeyId(keyId1);
            JweRecipient recipient1 = new JweRecipient(perRecipientHeader1,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader2 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader2.setContentEncryptionAlgorithm(enc);
            perRecipientHeader2.setAlgorithm(alg2);
            perRecipientHeader2.setKeyId(keyId2);
            JweRecipient recipient2 = new JweRecipient(perRecipientHeader2,null);
            
            JwePerRecipientUnprotectedHeader perRecipientHeader3 = new JwePerRecipientUnprotectedHeader();
            perRecipientHeader3.setContentEncryptionAlgorithm(enc);
            perRecipientHeader3.setAlgorithm(alg3);
            perRecipientHeader3.setKeyId(keyId3);
            JweRecipient recipient3 = new JweRecipient(perRecipientHeader3,null);
  
            JweSharedUnprotectedHeader unprotectedHeader = new JweSharedUnprotectedHeader();
            unprotectedHeader.setKeyId(keyId3);
            
            JweProtectedHeader protectedHeader = new JweProtectedHeader();
            protectedHeader.setZipAlgorithm("DEF");
            
            JweDocument jwe = JweMaker.generateFromMessage(PAYLOAD, CEK, protectedHeader, unprotectedHeader, recipient1, recipient2, recipient3);
            fail("7.23");

        } catch (JoseException e) {
            assertEquals(errorMessage012, e.getMessage());
        }   
    }
    
    private String readKeySet(String fileName)
    {
        InputStream is = this.getClass().getClassLoader().getResourceAsStream(fileName);
        StringBuilder sb = new StringBuilder(1024);

        try
        {
            for (int i = is.read(); i != -1; i = is.read())
            {
                sb.append((char) i);
            }
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        
        try
        {
            is.close();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        return sb.toString();
    }
    
}

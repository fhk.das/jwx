/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwa.tests;

import java.security.Key;
import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Test;

import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jwe.JweDocument;
import de.fhk.jwx.jwe.JweMaker;
import de.fhk.jwx.jwe.JweProtectedHeader;
import de.fhk.jwx.jwe.JweRecipient;
import de.fhk.jwx.jwe.crypto.JweCookbookBypass;
import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.util.JoseUtils;
import de.fhk.jwx.util.JwkUtils;

public class AppendixC {

	@Test
	public void jsonWebAlgorithmsAppendixC() {
		
		Security.addProvider(new BouncyCastleProvider());
		
			JweProtectedHeader protectedHeader = new JweProtectedHeader();
			protectedHeader.setAlgorithm(JwaAlgorithms.ECDH_ES.getSpecName());
			protectedHeader.setContentEncryptionAlgorithm(JwaAlgorithms.AES_128_GCM.getSpecName());
			
//			Alice's ephemeral key (in JWK format)
			String alicesKeyString =
				 "{\"kty\":\"EC\","
				+ "\"crv\":\"P-256\","
				+   "\"x\":\"gI0GAILBdu7T53akrFmMyGcsF3n5dO7MmwNBHKW5SV0\","
				+   "\"y\":\"SLW_xSffzlPWrHEVI30DHM_4egVwt3NQqeUD7nMFpps\","
				+   "\"d\":\"0_NxaRPUMQoAJt50Gz8YiTr8gRTwyEaCumd-MToTmIo\""
				+ "}";
			
			JwkKey alicesKeyJwk = new JwkKey(alicesKeyString);
			Key alicesPrivateKey = JwkUtils.convertJwkToECPrivateKey(alicesKeyJwk);
			Key alicesPublicKey = JwkUtils.convertJwkToECPublicKey(alicesKeyJwk);
	
			JweCookbookBypass appendixBypass = new JweCookbookBypass();
			appendixBypass.setBypassEcdhEphemeralPrivateKey(alicesPrivateKey);
			appendixBypass.setBypassEcdhEphemeralPublicKey(alicesPublicKey);
			
//			Bobs's key (in JWK format)
			String bobsKeyString =
				 "{\"kty\":\"EC\","
				+ "\"crv\":\"P-256\","
				+   "\"x\":\"weNJy2HscCSM6AEDTDg04biOvhFhyyWvOHQfeF_PxMQ\","
				+   "\"y\":\"e8lnCO-AlStT-NJVX-crhB7QRYhiix03illJOVAOyck\","
				+   "\"d\":\"VEmDZpDXXK8p8N0Cndsxs924q6nS1RXFASRl6BfUqdw\""
				+ "}";
			
			JwkKey bobsKeyJwk = new JwkKey(bobsKeyString);
			Key bobsPrivateKey = JwkUtils.convertJwkToECPrivateKey(bobsKeyJwk);
			Key bobsPublicKey = JwkUtils.convertJwkToECPublicKey(bobsKeyJwk);
		
			String apu = JoseUtils.stringToBase64URL("Alice");
			protectedHeader.setEcdhPartyUInfo(apu);
			
			String apv = JoseUtils.stringToBase64URL("Bob");			
			protectedHeader.setEcdhPartyVInfo(apv);
			
			// Z
			final int[] Z = 
				{158, 86, 217, 29, 129, 113, 53, 211, 114, 131, 66, 131, 191, 132,
			    38, 156, 251, 49, 110, 163, 218, 128, 106, 72, 246, 218, 167, 121,
			    140, 254, 144, 196};
			
			// keydatalen
			final int keydatalen = 128;

			// AlgorithmID
			final int[] AlgorithmID = {0, 0, 0, 7, 65, 49, 50, 56, 71, 67, 77};
	
			// PartyUInfo
			final int[] PartyUInfo = {0, 0, 0, 5, 65, 108, 105, 99, 101};
			
			// PartyVInfo
			final int[] PartyVInfo = {0, 0, 0, 3, 66, 111, 98};
			
			// SuppPubInfo
			final int[] SuppPubInfo = {0, 0, 0, 128};
			
			// SuppPubInfo
			final int[] SuppPrivInfo = new int[0];
			      				
			JweDocument jwe = JweMaker.generateFromMessageForJoseCookBook(
					appendixBypass, 
					"AppendixCMessage".getBytes(), 
					null, 
					bobsKeyJwk, 
					protectedHeader, 
					null);		
	}
	
	public void sysoIntArray(int[] array){
		System.out.print("[");
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + ", ");
		}
		System.out.println("]");
	}
}

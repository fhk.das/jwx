/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.cookbook;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.jwk.JwkKeySet;

public class JwkJoseCookBookTests extends Assert {
    private static final String EC_X_COORDINATE_VALUE = "AHKZLLOsCOzz5cY97ewNUajB957y-C-U88c3v13nmGZx6sYl_oJXu9"
            + "A5RkTKqjqvjyekWF-7ytDyRXYgCF5cj0Kt";
    private static final String EC_Y_COORDINATE_VALUE = "AdymlHvOiLxXkEhayXQnNCvDX4h9htZaCJN34kfmC6pV5OhQHiraVy"
            + "SsUdaQkAgDPrwQrJmbnX9cwlGfP-HqHZR1";
    private static final String EC_KID_VALUE = "bilbo.baggins@hobbiton.example";
    private static final String EC_CURVE_VALUE = "P-521";
    private static final String EC_PRIVATE_KEY_VALUE = "AAhRON2r9cqXX1hg-RoI6R1tX5p2rUAYdmpHZoC1XNM56KtscrX6zb"
            + "KipQrCW9CGZH3T4ubpnoTKLDYJ_fF3_rJt";
    private static final String RSA_MODULUS_VALUE = "n4EPtAOCc9AlkeQHPzHStgAbgs7bTZLwUBZdR8_KuKPEHLd4rHVTeT"
            + "-O-XV2jRojdNhxJWTDvNd7nqQ0VEiZQHz_AJmSCpMaJMRBSFKrKb2wqV"
            + "wGU_NsYOYL-QtiWN2lbzcEe6XC0dApr5ydQLrHqkHHig3RBordaZ6Aj-"
            + "oBHqFEHYpPe7Tpe-OfVfHd1E6cS6M1FZcD1NNLYD5lFHpPI9bTwJlsde"
            + "3uhGqC0ZCuEHg8lhzwOHrtIQbS0FVbb9k3-tVTU4fg_3L_vniUFAKwuC"
            + "LqKnS2BYwdq_mzSnbLY7h_qixoR7jig3__kRhuaxwUkRz5iaiQkqgc5g"
            + "HdrNP5zw";
    private static final String RSA_PUBLIC_EXP_VALUE = "AQAB";
    private static final String RSA_KID_VALUE = "bilbo.baggins@hobbiton.example";
    private static final String RSA_PRIVATE_EXP_VALUE = "bWUC9B-EFRIo8kpGfh0ZuyGPvMNKvYWNtB_ikiH9k20eT-O1q_I78e"
            + "iZkpXxXQ0UTEs2LsNRS-8uJbvQ-A1irkwMSMkK1J3XTGgdrhCku9gRld"
            + "Y7sNA_AKZGh-Q661_42rINLRCe8W-nZ34ui_qOfkLnK9QWDDqpaIsA-b"
            + "MwWWSDFu2MUBYwkHTMEzLYGqOe04noqeq1hExBTHBOBdkMXiuFhUq1BU"
            + "6l-DqEiWxqg82sXt2h-LMnT3046AOYJoRioz75tSUQfGCshWTBnP5uDj"
            + "d18kKhyv07lhfSJdrPdM5Plyl21hsFf4L_mHCuoFau7gdsPfHPxxjVOc"
            + "OpBrQzwQ";
    private static final String RSA_FIRST_PRIME_FACTOR_VALUE = "3Slxg_DwTXJcb6095RoXygQCAZ5RnAvZlno1yhHtnUex_fp7AZ_9nR"
            + "aO7HX_-SFfGQeutao2TDjDAWU4Vupk8rw9JR0AzZ0N2fvuIAmr_WCsmG"
            + "peNqQnev1T7IyEsnh8UMt-n5CafhkikzhEsrmndH6LxOrvRJlsPp6Zv8"
            + "bUq0k";
    private static final String RSA_SECOND_PRIME_FACTOR_VALUE = "uKE2dh-cTf6ERF4k4e_jy78GfPYUIaUyoSSJuBzp3Cubk3OCqs6grT"
            + "8bR_cu0Dm1MZwWmtdqDyI95HrUeq3MP15vMMON8lHTeZu2lmKvwqW7an"
            + "V5UzhM1iZ7z4yMkuUwFWoBvyY898EXvRD-hdqRxHlSqAZ192zB3pVFJ0"
            + "s7pFc";
    private static final String RSA_FIRST_PRIME_CRT_VALUE = "B8PVvXkvJrj2L-GYQ7v3y9r6Kw5g9SahXBwsWUzp19TVlgI-YV85q"
            + "1NIb1rxQtD-IsXXR3-TanevuRPRt5OBOdiMGQp8pbt26gljYfKU_E9xn"
            + "-RULHz0-ed9E9gXLKD4VGngpz-PfQ_q29pk5xWHoJp009Qf1HvChixRX"
            + "59ehik";
    private static final String RSA_SECOND_PRIME_CRT_VALUE = "CLDmDGduhylc9o7r84rEUVn7pzQ6PF83Y-iBZx5NT-TpnOZKF1pEr"
            + "AMVeKzFEl41DlHHqqBLSM0W1sOFbwTxYWZDm6sI6og5iTbwQGIC3gnJK"
            + "bi_7k_vJgGHwHxgPaX2PnvP-zyEkDERuf-ry4c_Z11Cq9AqC2yeL6kdK"
            + "T1cYF8";
    private static final String RSA_FIRST_CRT_COEFFICIENT_VALUE = "3PiqvXQN0zwMeE-sBvZgi289XP9XCQF3VWqPzMKnIgQp7_Tugo6-N"
            + "ZBKCQsMf3HaEGBjTVJs_jcK8-TRXvaKe-7ZMaQj8VfBdYkssbu0NKDDh"
            + "jJ-GtiseaDVWt7dcH0cfwxgFUHpQh7FoCrjFJ6h6ZEpMF6xmujs4qMpP"
            + "z8aaI4";
    private static final String SIGN_SECRET_VALUE = "hJtXIZ2uSN5kbQfbtTNWbpdmhkV8FJG-Onbc6mxCcYg";
    private static final String SIGN_KID_VALUE = "018c0ae5-4d9b-471b-bfd6-eef314bc7037";
    private static final String ENCRYPTION_SECRET_VALUE = "AAPapAv4LbFbiVawEjagUBluYqN5rhna-8nuldDvOx8";
    private static final String ENCRYPTION_KID_VALUE = "1e571774-2e08-40da-8308-e8d68773842d";

    @Test
    public void testPublicSetAsList() {
        JwkKeySet jwks = new JwkKeySet(readKeySet("cookbookPublicSet.txt"));
        List<JwkKey> keys = jwks.getKeys();
        assertEquals(5, keys.size());
        JwkKey ecKey = keys.get(0);
        assertEquals(6, ecKey.sizeOfProperties());
        validatePublicEcKey(ecKey);
        JwkKey rsaKey = keys.get(1);
        assertEquals(5, rsaKey.sizeOfProperties());
        validatePublicRsaKey(rsaKey);
    }

    @Test
    public void testPublicSetAsMap() {
        JwkKeySet jwks = new JwkKeySet(readKeySet("cookbookPublicSet.txt"));
        Map<String, List<JwkKey>> keysMap = jwks.getKeyTypeMap();
        assertEquals(2, keysMap.size());
        List<JwkKey> rsaKeys = keysMap.get("RSA");
        assertEquals(2, rsaKeys.size());
        assertEquals(5, rsaKeys.get(0).sizeOfProperties());
        validatePublicRsaKey(rsaKeys.get(0));
        List<JwkKey> ecKeys = keysMap.get("EC");
        assertEquals(3, ecKeys.size());
        assertEquals(6, ecKeys.get(0).sizeOfProperties());
        validatePublicEcKey(ecKeys.get(0));
    }

    @Test
    public void testPrivateSetAsList() {
        JwkKeySet jwks = new JwkKeySet(readKeySet("cookbookPrivateSet.txt"));
        validatePrivateSet(jwks);
    }

    @Test
    public void testSecretSetAsList() {
        JwkKeySet jwks = new JwkKeySet(readKeySet("cookbookSecretSet.txt"));
        List<JwkKey> keys = jwks.getKeys();
        assertEquals(5, keys.size());
        JwkKey signKey = keys.get(0);
        assertEquals(5, signKey.sizeOfProperties());
        validateSecretSignKey(signKey);
        JwkKey encKey = keys.get(1);
        assertEquals(5, encKey.sizeOfProperties());
        validateSecretEncKey(encKey);
    }

    private void validatePrivateSet(JwkKeySet jwks) {
        List<JwkKey> keys = jwks.getKeys();
        assertEquals(7, keys.size());
        JwkKey ecKey = keys.get(0);
        assertEquals(7, ecKey.sizeOfProperties());
        validatePrivateEcKey(ecKey);
        JwkKey rsaKey = keys.get(1);
        assertEquals(11, rsaKey.sizeOfProperties());
        validatePrivateRsaKey(rsaKey);
    }

    private void validateSecretSignKey(JwkKey key) {
        assertEquals(SIGN_SECRET_VALUE, key.getProperty(JwkKey.OCTET_KEY_VALUE));
        assertEquals(SIGN_KID_VALUE, key.getKid());
        assertEquals(JwkKey.KEY_TYPE_OCTET, key.getKeyType());
        assertEquals(JwaAlgorithms.HMAC_SHA_256.getSpecName(),
                key.getAlgorithm());
    }

    private void validateSecretEncKey(JwkKey key) {
        assertEquals(ENCRYPTION_SECRET_VALUE,
                key.getProperty(JwkKey.OCTET_KEY_VALUE));
        assertEquals(ENCRYPTION_KID_VALUE, key.getKid());
        assertEquals(JwkKey.KEY_TYPE_OCTET, key.getKeyType());
        assertEquals("A256GCM", key.getAlgorithm());
    }

    private void validatePublicRsaKey(JwkKey key) {
        assertEquals(RSA_MODULUS_VALUE, key.getProperty(JwkKey.RSA_MODULUS));
        assertEquals(RSA_PUBLIC_EXP_VALUE,
                key.getProperty(JwkKey.RSA_PUBLIC_EXP));
        assertEquals(RSA_KID_VALUE, key.getKid());
        assertEquals(JwkKey.KEY_TYPE_RSA, key.getKeyType());
    }

    private void validatePrivateRsaKey(JwkKey key) {
        validatePublicRsaKey(key);
        assertEquals(RSA_PRIVATE_EXP_VALUE,
                key.getProperty(JwkKey.RSA_PRIVATE_EXP));
        assertEquals(RSA_FIRST_PRIME_FACTOR_VALUE,
                key.getProperty(JwkKey.RSA_FIRST_PRIME_FACTOR));
        assertEquals(RSA_SECOND_PRIME_FACTOR_VALUE,
                key.getProperty(JwkKey.RSA_SECOND_PRIME_FACTOR));
        assertEquals(RSA_FIRST_PRIME_CRT_VALUE,
                key.getProperty(JwkKey.RSA_FIRST_PRIME_CRT));
        assertEquals(RSA_SECOND_PRIME_CRT_VALUE,
                key.getProperty(JwkKey.RSA_SECOND_PRIME_CRT));
        assertEquals(RSA_FIRST_CRT_COEFFICIENT_VALUE,
                key.getProperty(JwkKey.RSA_FIRST_CRT_COEFFICIENT));
    }

    private void validatePublicEcKey(JwkKey key) {
        assertEquals(EC_X_COORDINATE_VALUE,
                key.getProperty(JwkKey.EC_X_COORDINATE));
        assertEquals(EC_Y_COORDINATE_VALUE,
                key.getProperty(JwkKey.EC_Y_COORDINATE));
        assertEquals(EC_KID_VALUE, key.getKid());
        assertEquals(JwkKey.KEY_TYPE_ELLIPTIC, key.getKeyType());
        assertEquals(EC_CURVE_VALUE, key.getProperty(JwkKey.EC_CURVE));
        assertEquals(JwkKey.PUBLIC_KEY_USE_SIGN, key.getPublicKeyUse());
    }

    private void validatePrivateEcKey(JwkKey key) {
        validatePublicEcKey(key);
        assertEquals(EC_PRIVATE_KEY_VALUE,
                key.getProperty(JwkKey.EC_PRIVATE_KEY));
    }

    private String readKeySet(String fileName) {
        InputStream is = this.getClass().getClassLoader()
                .getResourceAsStream(fileName);
        StringBuilder sb = new StringBuilder(1024);

        try {
            for (int i = is.read(); i != -1; i = is.read()) {
                sb.append((char) i);
            }

            is.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return sb.toString();
    }

}

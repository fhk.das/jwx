/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.cookbook;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.security.Security;
import java.util.List;

import javax.crypto.Cipher;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Test;

import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.jwk.JwkKeySet;
import de.fhk.jwx.jws.JwsDocument;
import de.fhk.jwx.jws.JwsMaker;
import de.fhk.jwx.jws.JwsProtectedHeader;
import de.fhk.jwx.jws.JwsUnprotectedHeader;
import de.fhk.jwx.jws.crypto.EcdsaJwsSigner;
import de.fhk.jwx.util.JoseUtils;
import de.fhk.jwx.util.JwkUtils;
import de.fhk.jwx.util.JwsSignerAssigning;

public class JwsJoseCookBookTests {
    private static final String PAYLOAD = "It’s a dangerous business, Frodo, going out your door. "
            + "You step onto the road, and if you don't keep your feet, "
            + "there’s no knowing where you might be swept off to.";
    private static final String ENCODED_PAYLOAD = "SXTigJlzIGEgZGFuZ2Vyb3VzIGJ1c2luZXNzLCBGcm9kbywgZ29pbmcgb3V0IH"
            + "lvdXIgZG9vci4gWW91IHN0ZXAgb250byB0aGUgcm9hZCwgYW5kIGlmIHlvdSBk"
            + "b24ndCBrZWVwIHlvdXIgZmVldCwgdGhlcmXigJlzIG5vIGtub3dpbmcgd2hlcm"
            + "UgeW91IG1pZ2h0IGJlIHN3ZXB0IG9mZiB0by4";
    private static final String RSA_KID_VALUE = "bilbo.baggins@hobbiton.example";
    private static final String RSA_V1_5_SIGNATURE_PROTECTED_HEADER = "eyJhbGciOiJSUzI1NiIsImtpZCI6ImJpbGJvLmJhZ2dpbnNAaG9iYml0b24uZX"
            + "hhbXBsZSJ9";
    private static final String RSA_V1_5_SIGNATURE_PROTECTED_HEADER_JSON = ("{"
            + "\"alg\": \"RS256\","
            + "\"kid\": \"bilbo.baggins@hobbiton.example\"" + "}").replaceAll(
            " ", "");
    private static final String RSA_V1_5_SIGNATURE_VALUE = "MRjdkly7_-oTPTS3AXP41iQIGKa80A0ZmTuV5MEaHoxnW2e5CZ5NlKtainoFmK"
            + "ZopdHM1O2U4mwzJdQx996ivp83xuglII7PNDi84wnB-BDkoBwA78185hX-Es4J"
            + "IwmDLJK3lfWRa-XtL0RnltuYv746iYTh_qHRD68BNt1uSNCrUCTJDt5aAE6x8w"
            + "W1Kt9eRo4QPocSadnHXFxnt8Is9UzpERV0ePPQdLuW3IS_de3xyIrDaLGdjluP"
            + "xUAhb6L2aXic1U12podGU0KLUQSE_oI-ZnmKJ3F4uOZDnd6QZWJushZ41Axf_f"
            + "cIe8u9ipH84ogoree7vjbU5y18kDquDg";
    private static final String RSA_V1_5_JSON_GENERAL_SERIALIZATION = ("{"
            + "\"payload\": \"SXTigJlzIGEgZGFuZ2Vyb3VzIGJ1c2luZXNzLCBGcm9kbywg"
            + "Z29pbmcgb3V0IHlvdXIgZG9vci4gWW91IHN0ZXAgb250byB0aGUgcm9h"
            + "ZCwgYW5kIGlmIHlvdSBkb24ndCBrZWVwIHlvdXIgZmVldCwgdGhlcmXi"
            + "gJlzIG5vIGtub3dpbmcgd2hlcmUgeW91IG1pZ2h0IGJlIHN3ZXB0IG9m"
            + "ZiB0by4\"," + "\"signatures\": [" + "{"
            + "\"protected\": \"eyJhbGciOiJSUzI1NiIsImtpZCI6ImJpbGJvLmJhZ2"
            + "dpbnNAaG9iYml0b24uZXhhbXBsZSJ9\","
            + "\"signature\": \"MRjdkly7_-oTPTS3AXP41iQIGKa80A0ZmTuV5MEaHo"
            + "xnW2e5CZ5NlKtainoFmKZopdHM1O2U4mwzJdQx996ivp83xuglII"
            + "7PNDi84wnB-BDkoBwA78185hX-Es4JIwmDLJK3lfWRa-XtL0Rnlt"
            + "uYv746iYTh_qHRD68BNt1uSNCrUCTJDt5aAE6x8wW1Kt9eRo4QPo"
            + "cSadnHXFxnt8Is9UzpERV0ePPQdLuW3IS_de3xyIrDaLGdjluPxU"
            + "Ahb6L2aXic1U12podGU0KLUQSE_oI-ZnmKJ3F4uOZDnd6QZWJush"
            + "Z41Axf_fcIe8u9ipH84ogoree7vjbU5y18kDquDg\"" + "}" + "]" + "}")
            .replaceAll(" ", "");
    private static final String RSA_V1_5_JSON_FLATTENED_SERIALIZATION = ("{"
            + "\"payload\": \"SXTigJlzIGEgZGFuZ2Vyb3VzIGJ1c2luZXNzLCBGcm9kbywg"
            + "Z29pbmcgb3V0IHlvdXIgZG9vci4gWW91IHN0ZXAgb250byB0aGUgcm9h"
            + "ZCwgYW5kIGlmIHlvdSBkb24ndCBrZWVwIHlvdXIgZmVldCwgdGhlcmXi"
            + "gJlzIG5vIGtub3dpbmcgd2hlcmUgeW91IG1pZ2h0IGJlIHN3ZXB0IG9m"
            + "ZiB0by4\","
            + "\"protected\": \"eyJhbGciOiJSUzI1NiIsImtpZCI6ImJpbGJvLmJhZ2dpbn"
            + "NAaG9iYml0b24uZXhhbXBsZSJ9\","
            + "\"signature\": \"MRjdkly7_-oTPTS3AXP41iQIGKa80A0ZmTuV5MEaHoxnW2"
            + "e5CZ5NlKtainoFmKZopdHM1O2U4mwzJdQx996ivp83xuglII7PNDi84w"
            + "nB-BDkoBwA78185hX-Es4JIwmDLJK3lfWRa-XtL0RnltuYv746iYTh_q"
            + "HRD68BNt1uSNCrUCTJDt5aAE6x8wW1Kt9eRo4QPocSadnHXFxnt8Is9U"
            + "zpERV0ePPQdLuW3IS_de3xyIrDaLGdjluPxUAhb6L2aXic1U12podGU0"
            + "KLUQSE_oI-ZnmKJ3F4uOZDnd6QZWJushZ41Axf_fcIe8u9ipH84ogore"
            + "e7vjbU5y18kDquDg\"" + "}").replaceAll(" ", "");
    private static final String RSA_PSS_SIGNATURE_PROTECTED_HEADER_JSON = ("{"
            + "\"alg\": \"PS384\","
            + "\"kid\": \"bilbo.baggins@hobbiton.example\"" + "}").replaceAll(
            " ", "");
    private static final String RSA_PSS_SIGNATURE_PROTECTED_HEADER = "eyJhbGciOiJQUzM4NCIsImtpZCI6ImJpbGJvLmJhZ2dpbnNAaG9iYml0b24uZX"
            + "hhbXBsZSJ9";
    private static final String RSA_PSS_SIGNATURE_VALUE = "cu22eBqkYDKgIlTpzDXGvaFfz6WGoz7fUDcfT0kkOy42miAh2qyBzk1xEsnk2I"
            + "pN6-tPid6VrklHkqsGqDqHCdP6O8TTB5dDDItllVo6_1OLPpcbUrhiUSMxbbXU"
            + "vdvWXzg-UD8biiReQFlfz28zGWVsdiNAUf8ZnyPEgVFn442ZdNqiVJRmBqrYRX"
            + "e8P_ijQ7p8Vdz0TTrxUeT3lm8d9shnr2lfJT8ImUjvAA2Xez2Mlp8cBE5awDzT"
            + "0qI0n6uiP1aCN_2_jLAeQTlqRHtfa64QQSUmFAAjVKPbByi7xho0uTOcbH510a"
            + "6GYmJUAfmWjwZ6oD4ifKo8DYM-X72Eaw";
    private static final String RSA_PSS_JSON_GENERAL_SERIALIZATION = ("{"
            + "\"payload\": \"SXTigJlzIGEgZGFuZ2Vyb3VzIGJ1c2luZXNzLCBGcm9kbywg"
            + "Z29pbmcgb3V0IHlvdXIgZG9vci4gWW91IHN0ZXAgb250byB0aGUgcm9h"
            + "ZCwgYW5kIGlmIHlvdSBkb24ndCBrZWVwIHlvdXIgZmVldCwgdGhlcmXi"
            + "gJlzIG5vIGtub3dpbmcgd2hlcmUgeW91IG1pZ2h0IGJlIHN3ZXB0IG9m"
            + "ZiB0by4\"," + "\"signatures\": [" + "{"
            + "\"protected\": \"eyJhbGciOiJQUzM4NCIsImtpZCI6ImJpbGJvLmJhZ2"
            + "dpbnNAaG9iYml0b24uZXhhbXBsZSJ9\","
            + "\"signature\": \"cu22eBqkYDKgIlTpzDXGvaFfz6WGoz7fUDcfT0kkOy"
            + "42miAh2qyBzk1xEsnk2IpN6-tPid6VrklHkqsGqDqHCdP6O8TTB5"
            + "dDDItllVo6_1OLPpcbUrhiUSMxbbXUvdvWXzg-UD8biiReQFlfz2"
            + "8zGWVsdiNAUf8ZnyPEgVFn442ZdNqiVJRmBqrYRXe8P_ijQ7p8Vd"
            + "z0TTrxUeT3lm8d9shnr2lfJT8ImUjvAA2Xez2Mlp8cBE5awDzT0q"
            + "I0n6uiP1aCN_2_jLAeQTlqRHtfa64QQSUmFAAjVKPbByi7xho0uT"
            + "OcbH510a6GYmJUAfmWjwZ6oD4ifKo8DYM-X72Eaw\"" + "}" + "]" + "}")
            .replace(" ", "");
    private static final String RSA_PSS_JSON_FLATTENED_SERIALIZATION = ("{"
            + "\"payload\": \"SXTigJlzIGEgZGFuZ2Vyb3VzIGJ1c2luZXNzLCBGcm9kbywg"
            + "Z29pbmcgb3V0IHlvdXIgZG9vci4gWW91IHN0ZXAgb250byB0aGUgcm9h"
            + "ZCwgYW5kIGlmIHlvdSBkb24ndCBrZWVwIHlvdXIgZmVldCwgdGhlcmXi"
            + "gJlzIG5vIGtub3dpbmcgd2hlcmUgeW91IG1pZ2h0IGJlIHN3ZXB0IG9m"
            + "ZiB0by4\","
            + "\"protected\": \"eyJhbGciOiJQUzM4NCIsImtpZCI6ImJpbGJvLmJhZ2dpbn"
            + "NAaG9iYml0b24uZXhhbXBsZSJ9\","
            + "\"signature\": \"cu22eBqkYDKgIlTpzDXGvaFfz6WGoz7fUDcfT0kkOy42mi"
            + "Ah2qyBzk1xEsnk2IpN6-tPid6VrklHkqsGqDqHCdP6O8TTB5dDDItllV"
            + "o6_1OLPpcbUrhiUSMxbbXUvdvWXzg-UD8biiReQFlfz28zGWVsdiNAUf"
            + "8ZnyPEgVFn442ZdNqiVJRmBqrYRXe8P_ijQ7p8Vdz0TTrxUeT3lm8d9s"
            + "hnr2lfJT8ImUjvAA2Xez2Mlp8cBE5awDzT0qI0n6uiP1aCN_2_jLAeQT"
            + "lqRHtfa64QQSUmFAAjVKPbByi7xho0uTOcbH510a6GYmJUAfmWjwZ6oD"
            + "4ifKo8DYM-X72Eaw\"" + "}").replace(" ", "");
    private static final String ECDSA_KID_VALUE = "bilbo.baggins@hobbiton.example";
    private static final String ECDSA_SIGNATURE_PROTECTED_HEADER_JSON = ("{"
            + "\"alg\": \"ES512\","
            + "\"kid\": \"bilbo.baggins@hobbiton.example\"" + "}").replace(" ",
            "");
    private static final String ECSDA_SIGNATURE_PROTECTED_HEADER = "eyJhbGciOiJFUzUxMiIsImtpZCI6ImJpbGJvLmJhZ2dpbnNAaG9iYml0b24uZX"
            + "hhbXBsZSJ9";
    private static final String HMAC_KID_VALUE = "018c0ae5-4d9b-471b-bfd6-eef314bc7037";
    private static final String HMAC_SIGNATURE_PROTECTED_HEADER_JSON = ("{"
            + "\"alg\": \"HS256\","
            + "\"kid\": \"018c0ae5-4d9b-471b-bfd6-eef314bc7037\"" + "}")
            .replaceAll(" ", "");
    private static final String HMAC_SIGNATURE_PROTECTED_HEADER = "eyJhbGciOiJIUzI1NiIsImtpZCI6IjAxOGMwYWU1LTRkOWItNDcxYi1iZmQ2LW"
            + "VlZjMxNGJjNzAzNyJ9";
    private static final String HMAC_SIGNATURE_VALUE = "s0h6KThzkfBBBkLspW1h84VsJZFTsPPqMDA7g1Md7p0";
    private static final String HMAC_JSON_GENERAL_SERIALIZATION = ("{"
            + "\"payload\": \"SXTigJlzIGEgZGFuZ2Vyb3VzIGJ1c2luZXNzLCBGcm9kbywg"
            + "Z29pbmcgb3V0IHlvdXIgZG9vci4gWW91IHN0ZXAgb250byB0aGUgcm9h"
            + "ZCwgYW5kIGlmIHlvdSBkb24ndCBrZWVwIHlvdXIgZmVldCwgdGhlcmXi"
            + "gJlzIG5vIGtub3dpbmcgd2hlcmUgeW91IG1pZ2h0IGJlIHN3ZXB0IG9m"
            + "ZiB0by4\"," + "\"signatures\": [" + "{"
            + "\"protected\": \"eyJhbGciOiJIUzI1NiIsImtpZCI6IjAxOGMwYWU1LT"
            + "RkOWItNDcxYi1iZmQ2LWVlZjMxNGJjNzAzNyJ9\","
            + "\"signature\": \"s0h6KThzkfBBBkLspW1h84VsJZFTsPPqMDA7g1Md7p"
            + "0\"" + "}" + "]" + "}").replaceAll(" ", "");
    private static final String HMAC_JSON_FLATTENED_SERIALIZATION = ("{"
            + "\"payload\": \"SXTigJlzIGEgZGFuZ2Vyb3VzIGJ1c2luZXNzLCBGcm9kbywg"
            + "Z29pbmcgb3V0IHlvdXIgZG9vci4gWW91IHN0ZXAgb250byB0aGUgcm9h"
            + "ZCwgYW5kIGlmIHlvdSBkb24ndCBrZWVwIHlvdXIgZmVldCwgdGhlcmXi"
            + "gJlzIG5vIGtub3dpbmcgd2hlcmUgeW91IG1pZ2h0IGJlIHN3ZXB0IG9m"
            + "ZiB0by4\","
            + "\"protected\": \"eyJhbGciOiJIUzI1NiIsImtpZCI6IjAxOGMwYWU1LTRkOW"
            + "ItNDcxYi1iZmQ2LWVlZjMxNGJjNzAzNyJ9\","
            + "\"signature\": \"s0h6KThzkfBBBkLspW1h84VsJZFTsPPqMDA7g1Md7p0\""
            + "}").replaceAll(" ", "");
    private static final String DETACHED_HMAC_JsonWebSignature = ("eyJhbGciOiJIUzI1NiIsImtpZCI6IjAxOGMwYWU1LTRkOWItNDcxYi1iZmQ2LW"
            + "VlZjMxNGJjNzAzNyJ9"
            + "."
            + "."
            + "s0h6KThzkfBBBkLspW1h84VsJZFTsPPqMDA7g1Md7p0")
            .replaceAll(" ", "");
    private static final String HMAC_DETACHED_JSON_GENERAL_SERIALIZATION = ("{"
            + "\"signatures\": [" + "{"
            + "\"protected\": \"eyJhbGciOiJIUzI1NiIsImtpZCI6IjAxOGMwYWU1LT"
            + "RkOWItNDcxYi1iZmQ2LWVlZjMxNGJjNzAzNyJ9\","
            + "\"signature\": \"s0h6KThzkfBBBkLspW1h84VsJZFTsPPqMDA7g1Md7p"
            + "0\"" + "}" + "]" + "}").replaceAll(" ", "");
    private static final String HMAC_DETACHED_JSON_FLATTENED_SERIALIZATION = ("{"
            + "\"protected\": \"eyJhbGciOiJIUzI1NiIsImtpZCI6IjAxOGMwYWU1LTRkOW"
            + "ItNDcxYi1iZmQ2LWVlZjMxNGJjNzAzNyJ9\","
            + "\"signature\": \"s0h6KThzkfBBBkLspW1h84VsJZFTsPPqMDA7g1Md7p0\""
            + "}").replaceAll(" ", "");
    private static final String ECSDA_SIGNATURE_VALUE = "AE_R_YZCChjn4791jSQCrdPZCNYqHXCTZH0-JZGYNlaAjP2kqaluUIIUnC9qvb"
            + "u9Plon7KRTzoNEuT4Va2cmL1eJAQy3mtPBu_u_sDDyYjnAMDxXPn7XrT0lw-kv"
            + "AD890jl8e2puQens_IEKBpHABlsbEPX6sFY8OcGDqoRuBomu9xQ2";
    private static final String PROTECTING_SPECIFIC_HEADER_FIELDS_JSON_GENERAL_SERIALIZATION = ("{"
            + "\"payload\": \"SXTigJlzIGEgZGFuZ2Vyb3VzIGJ1c2luZXNzLCBGcm9kbywg"
            + "Z29pbmcgb3V0IHlvdXIgZG9vci4gWW91IHN0ZXAgb250byB0aGUgcm9h"
            + "ZCwgYW5kIGlmIHlvdSBkb24ndCBrZWVwIHlvdXIgZmVldCwgdGhlcmXi"
            + "gJlzIG5vIGtub3dpbmcgd2hlcmUgeW91IG1pZ2h0IGJlIHN3ZXB0IG9m"
            + "ZiB0by4\","
            + "\"signatures\": ["
            + "{"
            + "\"protected\": \"eyJhbGciOiJIUzI1NiJ9\","
            + "\"header\": {"
            + "\"kid\": \"018c0ae5-4d9b-471b-bfd6-eef314bc7037\""
            + "},"
            + "\"signature\": \"bWUSVaxorn7bEF1djytBd0kHv70Ly5pvbomzMWSOr2"
            + "0\"" + "}" + "]" + "}").replace(" ", "");
    private static final String PROTECTING_SPECIFIC_HEADER_FIELDS_JSON_FLATTENED_SERIALIZATION = ("{"
            + "\"payload\": \"SXTigJlzIGEgZGFuZ2Vyb3VzIGJ1c2luZXNzLCBGcm9kbywg"
            + "Z29pbmcgb3V0IHlvdXIgZG9vci4gWW91IHN0ZXAgb250byB0aGUgcm9h"
            + "ZCwgYW5kIGlmIHlvdSBkb24ndCBrZWVwIHlvdXIgZmVldCwgdGhlcmXi"
            + "gJlzIG5vIGtub3dpbmcgd2hlcmUgeW91IG1pZ2h0IGJlIHN3ZXB0IG9m"
            + "ZiB0by4\","
            + "\"protected\": \"eyJhbGciOiJIUzI1NiJ9\","
            + "\"header\": {"
            + "\"kid\": \"018c0ae5-4d9b-471b-bfd6-eef314bc7037\""
            + "},"
            + "\"signature\": \"bWUSVaxorn7bEF1djytBd0kHv70Ly5pvbomzMWSOr20\""
            + "}").replace(" ", "");
    private static final String PROTECTING_CONTENT_ONLY_JSON_GENERAL_SERIALIZATION = ("{"
            + "\"payload\": \"SXTigJlzIGEgZGFuZ2Vyb3VzIGJ1c2luZXNzLCBGcm9kbywg"
            + "Z29pbmcgb3V0IHlvdXIgZG9vci4gWW91IHN0ZXAgb250byB0aGUgcm9h"
            + "ZCwgYW5kIGlmIHlvdSBkb24ndCBrZWVwIHlvdXIgZmVldCwgdGhlcmXi"
            + "gJlzIG5vIGtub3dpbmcgd2hlcmUgeW91IG1pZ2h0IGJlIHN3ZXB0IG9m"
            + "ZiB0by4\","
            + "\"signatures\": ["
            + "{"
            + "\"header\": {"
            + "\"alg\": \"HS256\","
            + "\"kid\": \"018c0ae5-4d9b-471b-bfd6-eef314bc7037\""
            + "},"
            + "\"signature\": \"xuLifqLGiblpv9zBpuZczWhNj1gARaLV3UxvxhJxZu"
            + "k\"" + "}" + "]" + "}").replace(" ", "");
    private static final String PROTECTING_CONTENT_ONLY_JSON_FLATTENED_SERIALIZATION = ("{"
            + "\"payload\": \"SXTigJlzIGEgZGFuZ2Vyb3VzIGJ1c2luZXNzLCBGcm9kbywg"
            + "Z29pbmcgb3V0IHlvdXIgZG9vci4gWW91IHN0ZXAgb250byB0aGUgcm9h"
            + "ZCwgYW5kIGlmIHlvdSBkb24ndCBrZWVwIHlvdXIgZmVldCwgdGhlcmXi"
            + "gJlzIG5vIGtub3dpbmcgd2hlcmUgeW91IG1pZ2h0IGJlIHN3ZXB0IG9m"
            + "ZiB0by4\","
            + "\"header\": {"
            + "\"alg\": \"HS256\","
            + "\"kid\": \"018c0ae5-4d9b-471b-bfd6-eef314bc7037\""
            + "},"
            + "\"signature\": \"xuLifqLGiblpv9zBpuZczWhNj1gARaLV3UxvxhJxZuk\""
            + "}").replace(" ", "");
    private static final String FIRST_SIGNATURE_ENTRY_MULTIPLE_SIGNATURES = ("{"
            + "\"protected\": \"eyJhbGciOiJSUzI1NiJ9\","
            + "\"header\": {"
            + "\"kid\": \"bilbo.baggins@hobbiton.example\""
            + "},"
            + "\"signature\": \"MIsjqtVlOpa71KE-Mss8_Nq2YH4FGhiocsqrgi5NvyG53u"
            + "oimic1tcMdSg-qptrzZc7CG6Svw2Y13TDIqHzTUrL_lR2ZFcryNFiHkS"
            + "w129EghGpwkpxaTn_THJTCglNbADko1MZBCdwzJxwqZc-1RlpO2HibUY"
            + "yXSwO97BSe0_evZKdjvvKSgsIqjytKSeAMbhMBdMma622_BG5t4sdbuC"
            + "HtFjp9iJmkio47AIwqkZV1aIZsv33uPUqBBCXbYoQJwt7mxPftHmNlGo"
            + "OSMxR_3thmXTCm4US-xiNOyhbm8afKK64jU6_TPtQHiJeQJxz9G3Tx-0"
            + "83B745_AfYOnlC9w\"" + "}").replace(" ", "");
    private static final String SECOND_SIGNATURE_ENTRY_MULTIPLE_SIGNATURES = ("{"
            + "\"header\": {"
            + "\"alg\": \"ES512\","
            + "\"kid\": \"bilbo.baggins@hobbiton.example\""
            + "},"
            + "\"signature\": \"ARcVLnaJJaUWG8fG-8t5BREVAuTY8n8YHjwDO1muhcdCoF"
            + "ZFFjfISu0Cdkn9Ybdlmi54ho0x924DUz8sK7ZXkhc7AFM8ObLfTvNCrq"
            + "cI3Jkl2U5IX3utNhODH6v7xgy1Qahsn0fyb4zSAkje8bAWz4vIfj5pCM"
            + "Yxxm4fgV3q7ZYhm5eD\"" + "}").replace(" ", "");
    private static final String SECOND_SIGNATURE_UNPROTECTED_HEADER_MULTIPLE_SIGNATURES = ("{"
            + "\"alg\": \"ES512\","
            + "\"kid\": \"bilbo.baggins@hobbiton.example\"" + "}").replace(" ",
            "");
    private static final String THIRD_SIGNATURE_ENTRY_MULTIPLE_SIGNATURES = ("{"
            + "\"protected\": \"eyJhbGciOiJIUzI1NiIsImtpZCI6IjAxOGMwYWU1LTRkOW"
            + "ItNDcxYi1iZmQ2LWVlZjMxNGJjNzAzNyJ9\","
            + "\"signature\": \"s0h6KThzkfBBBkLspW1h84VsJZFTsPPqMDA7g1Md7p0\""
            + "}").replace(" ", "");
    private static final String MULTIPLE_SIGNATURES_JSON_GENERAL_SERIALIZATION = ("{"
            + "\"payload\": \"SXTigJlzIGEgZGFuZ2Vyb3VzIGJ1c2luZXNzLCBGcm9kbywg"
            + "Z29pbmcgb3V0IHlvdXIgZG9vci4gWW91IHN0ZXAgb250byB0aGUgcm9h"
            + "ZCwgYW5kIGlmIHlvdSBkb24ndCBrZWVwIHlvdXIgZmVldCwgdGhlcmXi"
            + "gJlzIG5vIGtub3dpbmcgd2hlcmUgeW91IG1pZ2h0IGJlIHN3ZXB0IG9m"
            + "ZiB0by4\","
            + "\"signatures\": ["
            + "{"
            + "\"protected\": \"eyJhbGciOiJSUzI1NiJ9\","
            + "\"header\": {"
            + "\"kid\": \"bilbo.baggins@hobbiton.example\""
            + "},"
            + "\"signature\": \"MIsjqtVlOpa71KE-Mss8_Nq2YH4FGhiocsqrgi5Nvy"
            + "G53uoimic1tcMdSg-qptrzZc7CG6Svw2Y13TDIqHzTUrL_lR2ZFc"
            + "ryNFiHkSw129EghGpwkpxaTn_THJTCglNbADko1MZBCdwzJxwqZc"
            + "-1RlpO2HibUYyXSwO97BSe0_evZKdjvvKSgsIqjytKSeAMbhMBdM"
            + "ma622_BG5t4sdbuCHtFjp9iJmkio47AIwqkZV1aIZsv33uPUqBBC"
            + "XbYoQJwt7mxPftHmNlGoOSMxR_3thmXTCm4US-xiNOyhbm8afKK6"
            + "4jU6_TPtQHiJeQJxz9G3Tx-083B745_AfYOnlC9w\""
            + "},"
            + "{"
            + "\"header\": {"
            + "\"alg\": \"ES512\","
            + "\"kid\": \"bilbo.baggins@hobbiton.example\""
            + "},"
            + "\"signature\": \"ARcVLnaJJaUWG8fG-8t5BREVAuTY8n8YHjwDO1muhc"
            + "dCoFZFFjfISu0Cdkn9Ybdlmi54ho0x924DUz8sK7ZXkhc7AFM8Ob"
            + "LfTvNCrqcI3Jkl2U5IX3utNhODH6v7xgy1Qahsn0fyb4zSAkje8b"
            + "AWz4vIfj5pCMYxxm4fgV3q7ZYhm5eD\""
            + "},"
            + "{"
            + "\"protected\": \"eyJhbGciOiJIUzI1NiIsImtpZCI6IjAxOGMwYWU1LT"
            + "RkOWItNDcxYi1iZmQ2LWVlZjMxNGJjNzAzNyJ9\","
            + "\"signature\": \"s0h6KThzkfBBBkLspW1h84VsJZFTsPPqMDA7g1Md7p"
            + "0\"" + "}" + "]" + "}").replace(" ", "");;

    @Test
    public void testEncodedPayload() {
        assertEquals(JoseUtils.stringToBase64URL(PAYLOAD), ENCODED_PAYLOAD);
    }

    @Test
    public void testRSAv15Signature() {

        JwsProtectedHeader protectedHeader = new JwsProtectedHeader();

        protectedHeader.setAlgorithm(JwaAlgorithms.RS_SHA_256.getSpecName());
        protectedHeader.setKeyId(RSA_KID_VALUE);
        assertEquals(protectedHeader.getHeaders(),
                RSA_V1_5_SIGNATURE_PROTECTED_HEADER_JSON);

        JwkKeySet jwks = new JwkKeySet(readKeySet("cookbookPrivateSet.txt"));
        List<JwkKey> keys = jwks.getKeys();
        JwkKey rsaKey = keys.get(1);
        JwsDocument jws = JwsMaker.generateFromPayload(PAYLOAD,
                protectedHeader, rsaKey);
        assertEquals(jws.getSignatureInput(),
                RSA_V1_5_SIGNATURE_PROTECTED_HEADER + "." + ENCODED_PAYLOAD);
        assertEquals(jws.getCompactSerialisation(),
                RSA_V1_5_SIGNATURE_PROTECTED_HEADER + "." + ENCODED_PAYLOAD
                        + "." + RSA_V1_5_SIGNATURE_VALUE);
        JwsDocument consumer = JwsMaker.generateFromJws(jws
                .getCompactSerialisation());
        JwkKeySet publicJwks = new JwkKeySet(
                readKeySet("cookbookPublicSet.txt"));
        List<JwkKey> publicKeys = publicJwks.getKeys();
        JwkKey rsaPublicKey = publicKeys.get(1);
        assertTrue(consumer.verifySignatureWith(rsaPublicKey,
                JwaAlgorithms.RS_SHA_256.getSpecName()));

        assertEquals(jws.getJsonSerialisation(),
                RSA_V1_5_JSON_GENERAL_SERIALIZATION);
        JwsDocument jsonConsumer = JwsMaker.generateFromJws(jws
                .getJsonSerialisation());
        assertTrue(jsonConsumer.verifySignatureWith(rsaPublicKey,
                JwaAlgorithms.RS_SHA_256.getSpecName()));

        assertEquals(jws.getJsonFlattenedSerialisation(),
                RSA_V1_5_JSON_FLATTENED_SERIALIZATION);
        jsonConsumer = JwsMaker.generateFromJws(jws
                .getJsonFlattenedSerialisation());
        assertTrue(jsonConsumer.verifySignatureWith(rsaPublicKey,
                JwaAlgorithms.RS_SHA_256.getSpecName()));
    }

    @Test
    public void testRSAPSSSignature() {
        try {
            Cipher.getInstance(JwaAlgorithms.PS_SHA_384.getJavaName());
        } catch (Throwable t) {
            Security.addProvider(new BouncyCastleProvider());
        }

        JwsProtectedHeader protectedHeader = new JwsProtectedHeader();

        protectedHeader.setAlgorithm(JwaAlgorithms.PS_SHA_384.getSpecName());
        protectedHeader.setKeyId(RSA_KID_VALUE);
        assertEquals(protectedHeader.getHeaders(),
                RSA_PSS_SIGNATURE_PROTECTED_HEADER_JSON);

        JwkKeySet jwks = new JwkKeySet(readKeySet("cookbookPrivateSet.txt"));
        List<JwkKey> keys = jwks.getKeys();
        JwkKey rsaKey = keys.get(1);
        JwsDocument producer = JwsMaker.generateFromPayload(PAYLOAD,
                protectedHeader, rsaKey);
        assertEquals(producer.getSignatureInput(),
                RSA_PSS_SIGNATURE_PROTECTED_HEADER + "." + ENCODED_PAYLOAD);
        assertEquals(producer.getCompactSerialisation().length(),
                (RSA_PSS_SIGNATURE_PROTECTED_HEADER + "." + ENCODED_PAYLOAD
                        + "." + RSA_PSS_SIGNATURE_VALUE).length());
        JwsDocument consumer = JwsMaker.generateFromJws(producer
                .getCompactSerialisation());
        JwkKeySet publicJwks = new JwkKeySet(
                readKeySet("cookbookPublicSet.txt"));
        List<JwkKey> publicKeys = publicJwks.getKeys();
        JwkKey rsaPublicKey = publicKeys.get(1);
        assertTrue(consumer.verifySignatureWith(rsaPublicKey,
                JwaAlgorithms.PS_SHA_384.getSpecName()));

        assertEquals(producer.getJsonSerialisation().length(),
                RSA_PSS_JSON_GENERAL_SERIALIZATION.length());
        consumer = JwsMaker.generateFromJws(producer.getJsonSerialisation());
        assertTrue(consumer.verifySignatureWith(rsaPublicKey,
                JwaAlgorithms.PS_SHA_384.getSpecName()));

        assertEquals(producer.getJsonFlattenedSerialisation().length(),
                RSA_PSS_JSON_FLATTENED_SERIALIZATION.length());
        consumer = JwsMaker.generateFromJws(producer
                .getJsonFlattenedSerialisation());
        assertTrue(consumer.verifySignatureWith(rsaPublicKey,
                JwaAlgorithms.PS_SHA_384.getSpecName()));
        Security.removeProvider(BouncyCastleProvider.class.getName());
    }

    @Test
    public void testECDSASignature() {

        try {
            Cipher.getInstance(JwaAlgorithms.ES_SHA_512.getJavaName());
        } catch (Throwable t) {
            Security.addProvider(new BouncyCastleProvider());
        }
        try {

            JwsProtectedHeader protectedHeader = new JwsProtectedHeader();
            protectedHeader
                    .setAlgorithm(JwaAlgorithms.ES_SHA_512.getSpecName());
            protectedHeader.setKeyId(ECDSA_KID_VALUE);
            assertEquals(protectedHeader.getHeaders(),
                    ECDSA_SIGNATURE_PROTECTED_HEADER_JSON);
            JwkKeySet jwks = new JwkKeySet(readKeySet("cookbookPrivateSet.txt"));
            List<JwkKey> keys = jwks.getKeys();
            JwkKey ecKey = keys.get(0);
            JwsDocument producer = JwsMaker.generateFromPayload(PAYLOAD,
                    protectedHeader,
                    new EcdsaJwsSigner(JwkUtils.convertJwkToECPrivateKey(ecKey)));
            assertEquals(producer.getSignatureInput(),
                    ECSDA_SIGNATURE_PROTECTED_HEADER + "." + ENCODED_PAYLOAD);

            assertEquals((ECSDA_SIGNATURE_PROTECTED_HEADER + "."
                    + ENCODED_PAYLOAD + "." + ECSDA_SIGNATURE_VALUE).length(),
                    producer.getCompactSerialisation().length());

            JwsDocument consumer = JwsMaker.generateFromJws(producer
                    .getCompactSerialisation());
            JwkKeySet publicJwks = new JwkKeySet(
                    readKeySet("cookbookPublicSet.txt"));
            List<JwkKey> publicKeys = publicJwks.getKeys();
            JwkKey ecPublicKey = publicKeys.get(0);
            assertTrue(consumer.verifySignatureWith(ecPublicKey,
                    JwaAlgorithms.ES_SHA_512.getSpecName()));
        } finally {
            Security.removeProvider(BouncyCastleProvider.class.getName());
        }
    }

    @Test
    public void testHMACSignature() {

        JwsProtectedHeader protectedHeader = new JwsProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.HMAC_SHA_256.getSpecName());
        protectedHeader.setKeyId(HMAC_KID_VALUE);
        assertEquals(protectedHeader.getHeaders(),
                HMAC_SIGNATURE_PROTECTED_HEADER_JSON);

        JwkKeySet jwks = new JwkKeySet(readKeySet("cookbookSecretSet.txt"));
        List<JwkKey> keys = jwks.getKeys();
        JwkKey key = keys.get(0);

        JwsDocument producer = JwsMaker.generateFromPayload(PAYLOAD,
                protectedHeader, key);
        assertEquals(producer.getSignatureInput(),
                HMAC_SIGNATURE_PROTECTED_HEADER + "." + ENCODED_PAYLOAD);
        assertEquals(producer.getCompactSerialisation(),
                HMAC_SIGNATURE_PROTECTED_HEADER + "." + ENCODED_PAYLOAD + "."
                        + HMAC_SIGNATURE_VALUE);
        JwsDocument consumer = JwsMaker.generateFromJws(producer
                .getCompactSerialisation());
        assertTrue(consumer.verifySignatureWith(key,
                JwaAlgorithms.HMAC_SHA_256.getSpecName()));

        assertEquals(producer.getJsonSerialisation(),
                HMAC_JSON_GENERAL_SERIALIZATION);
        consumer = JwsMaker.generateFromJws(producer.getJsonSerialisation());
        assertTrue(consumer.verifySignatureWith(key,
                JwaAlgorithms.HMAC_SHA_256.getSpecName()));

        assertEquals(producer.getJsonFlattenedSerialisation(),
                HMAC_JSON_FLATTENED_SERIALIZATION);
        consumer = JwsMaker.generateFromJws(producer
                .getJsonFlattenedSerialisation());
        assertTrue(consumer.verifySignatureWith(key,
                JwaAlgorithms.HMAC_SHA_256.getSpecName()));

    }

    @Test
    public void testDetachedHMACSignature() {
        JwsProtectedHeader protectedHeader = new JwsProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.HMAC_SHA_256.getSpecName());
        protectedHeader.setKeyId(HMAC_KID_VALUE);
        assertEquals(protectedHeader.getHeaders(),
                HMAC_SIGNATURE_PROTECTED_HEADER_JSON);

        JwkKeySet jwks = new JwkKeySet(readKeySet("cookbookSecretSet.txt"));
        List<JwkKey> keys = jwks.getKeys();
        JwkKey key = keys.get(0);
        JwsDocument producer = JwsMaker.generateFromPayload(PAYLOAD,
                protectedHeader, key);
        assertEquals(producer.getSignatureInput(),
                HMAC_SIGNATURE_PROTECTED_HEADER + "." + ENCODED_PAYLOAD);
        assertEquals(producer.getCompactDetachedSerialisation(),
                DETACHED_HMAC_JsonWebSignature);
        JwsDocument consumer = JwsMaker.generateFromJws(
                producer.getCompactDetachedSerialisation(), ENCODED_PAYLOAD);
        assertTrue(consumer.verifySignatureWith(key,
                JwaAlgorithms.HMAC_SHA_256.getSpecName()));

        assertEquals(producer.getJsonDetachedSerialisation(),
                HMAC_DETACHED_JSON_GENERAL_SERIALIZATION);
        consumer = JwsMaker.generateFromJws(
                producer.getJsonDetachedSerialisation(), ENCODED_PAYLOAD);
        assertTrue(consumer.verifySignatureWith(key,
                JwaAlgorithms.HMAC_SHA_256.getSpecName()));

        assertEquals(producer.getJsonFlattenedDetachedSerialisation(),
                HMAC_DETACHED_JSON_FLATTENED_SERIALIZATION);
        consumer = JwsMaker.generateFromJws(
                producer.getJsonFlattenedDetachedSerialisation(),
                ENCODED_PAYLOAD);
        assertTrue(consumer.verifySignatureWith(key,
                JwaAlgorithms.HMAC_SHA_256.getSpecName()));
    }

    @Test
    public void testProtectingSpecificHeaderFieldsSignature() {

        JwsProtectedHeader protectedHeader = new JwsProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.HMAC_SHA_256.getSpecName());
        JwsUnprotectedHeader unprotectedHeader = new JwsUnprotectedHeader();
        unprotectedHeader.setKeyId(HMAC_KID_VALUE);
        JwkKeySet jwks = new JwkKeySet(readKeySet("cookbookSecretSet.txt"));
        List<JwkKey> keys = jwks.getKeys();
        JwkKey key = keys.get(0);

        JwsDocument producer = JwsMaker.generateFromPayload(
                PAYLOAD,
                protectedHeader,
                unprotectedHeader,
                JwsSignerAssigning.getJwsSigner(key,
                        JwaAlgorithms.HMAC_SHA_256.getSpecName()));
        assertEquals(producer.getDecodedPayload(), PAYLOAD);
        assertEquals(producer.getEncodedPayload(), ENCODED_PAYLOAD);
        assertEquals(producer.getJsonSerialisation(),
                PROTECTING_SPECIFIC_HEADER_FIELDS_JSON_GENERAL_SERIALIZATION);
        JwsDocument consumer = JwsMaker.generateFromJws(producer
                .getJsonSerialisation());
        assertTrue(consumer.verifySignatureWith(key,
                JwaAlgorithms.HMAC_SHA_256.getSpecName()));

        assertEquals(producer.getJsonFlattenedSerialisation(),
                PROTECTING_SPECIFIC_HEADER_FIELDS_JSON_FLATTENED_SERIALIZATION);
        consumer = JwsMaker.generateFromJws(producer
                .getJsonFlattenedSerialisation());
        assertTrue(consumer.verifySignatureWith(key,
                JwaAlgorithms.HMAC_SHA_256.getSpecName()));

    }

    @Test
    public void testProtectingContentOnlySignature() {

        JwsUnprotectedHeader unprotectedHeader = new JwsUnprotectedHeader();
        unprotectedHeader
                .setAlgorithm(JwaAlgorithms.HMAC_SHA_256.getSpecName());
        unprotectedHeader.setKeyId(HMAC_KID_VALUE);
        JwkKeySet jwks = new JwkKeySet(readKeySet("cookbookSecretSet.txt"));
        List<JwkKey> keys = jwks.getKeys();
        JwkKey key = keys.get(0);
        JwsDocument producer = JwsMaker.generateFromPayload(
                PAYLOAD,
                unprotectedHeader,
                JwsSignerAssigning.getJwsSigner(key,
                        JwaAlgorithms.HMAC_SHA_256.getSpecName()));
        assertEquals(producer.getDecodedPayload(), PAYLOAD);
        assertEquals(producer.getEncodedPayload(), ENCODED_PAYLOAD);

        assertEquals(producer.getJsonSerialisation(),
                PROTECTING_CONTENT_ONLY_JSON_GENERAL_SERIALIZATION);
        JwsDocument consumer = JwsMaker.generateFromJws(producer
                .getJsonSerialisation());
        assertTrue(consumer.verifySignatureWith(key,
                JwaAlgorithms.HMAC_SHA_256.getSpecName()));

        assertEquals(producer.getJsonFlattenedSerialisation(),
                PROTECTING_CONTENT_ONLY_JSON_FLATTENED_SERIALIZATION);
        consumer = JwsMaker.generateFromJws(producer
                .getJsonFlattenedSerialisation());
        assertTrue(consumer.verifySignatureWith(key,
                JwaAlgorithms.HMAC_SHA_256.getSpecName()));
    }

    @Test
    public void testMultipleSignatures() {
        try {
            Cipher.getInstance(JwaAlgorithms.ES_SHA_512.getJavaName());
        } catch (Throwable t) {
            Security.addProvider(new BouncyCastleProvider());
        }
        try {
            JwsProtectedHeader protectedHeader = new JwsProtectedHeader();
            protectedHeader
                    .setAlgorithm(JwaAlgorithms.RS_SHA_256.getSpecName());
            JwsUnprotectedHeader unprotectedHeader = new JwsUnprotectedHeader();
            unprotectedHeader.setKeyId(RSA_KID_VALUE);

            JwkKeySet jwks = new JwkKeySet(readKeySet("cookbookPrivateSet.txt"));
            List<JwkKey> keys = jwks.getKeys();
            JwkKey rsaKey = keys.get(1);
            JwsDocument jws = JwsMaker.generateFromPayload(PAYLOAD,
                    protectedHeader, unprotectedHeader, rsaKey);

            assertEquals(jws.getDecodedPayload(), PAYLOAD);
            assertEquals(jws.getEncodedPayload(), ENCODED_PAYLOAD);
            assertEquals(jws.getJsonFlattenedDetachedSerialisation(),
                    FIRST_SIGNATURE_ENTRY_MULTIPLE_SIGNATURES);

            unprotectedHeader = new JwsUnprotectedHeader();
            unprotectedHeader.setAlgorithm(JwaAlgorithms.ES_SHA_512
                    .getSpecName());
            unprotectedHeader.setKeyId(ECDSA_KID_VALUE);
            JwkKey ecKey = keys.get(0);
            jws.additionallySignWith(JwsSignerAssigning.getJwsSigner(ecKey,
                    JwaAlgorithms.ES_SHA_512.getSpecName()), null,
                    unprotectedHeader);
            assertEquals(jws.getSignatureEntry(1).getUnprotectedHeader(),
                    SECOND_SIGNATURE_UNPROTECTED_HEADER_MULTIPLE_SIGNATURES);
            assertEquals(jws.getJwsDocument(1)
                    .getJsonFlattenedDetachedSerialisation().length(),
                    SECOND_SIGNATURE_ENTRY_MULTIPLE_SIGNATURES.length());

            protectedHeader = new JwsProtectedHeader();
            protectedHeader.setAlgorithm(JwaAlgorithms.HMAC_SHA_256
                    .getSpecName());
            protectedHeader.setKeyId(HMAC_KID_VALUE);
            JwkKeySet secretJwks = new JwkKeySet(
                    readKeySet("cookbookSecretSet.txt"));
            List<JwkKey> secretKeys = secretJwks.getKeys();
            JwkKey hmacKey = secretKeys.get(0);
            jws.additionallySignWith(JwsSignerAssigning.getJwsSigner(hmacKey,
                    JwaAlgorithms.HMAC_SHA_256.getSpecName()), protectedHeader,
                    null);
            assertEquals(jws.getJwsDocument(2)
                    .getJsonFlattenedDetachedSerialisation(),
                    THIRD_SIGNATURE_ENTRY_MULTIPLE_SIGNATURES);
            assertEquals(jws.getJsonSerialisation().length(),
                    MULTIPLE_SIGNATURES_JSON_GENERAL_SERIALIZATION.length());
            JwsDocument consumer = JwsMaker.generateFromJws(jws
                    .getJsonSerialisation());
            JwkKeySet publicJwks = new JwkKeySet(
                    readKeySet("cookbookPublicSet.txt"));
            List<JwkKey> publicKeys = publicJwks.getKeys();
            JwkKey rsaPublicKey = publicKeys.get(1);
            JwkKey ecPublicKey = publicKeys.get(0);
            assertTrue(consumer.tryToVerifySignatureWith(rsaPublicKey,
                    JwaAlgorithms.RS_SHA_256.getSpecName()));
            assertTrue(consumer.verifySignatureWith(rsaPublicKey,
                    JwaAlgorithms.RS_SHA_256.getSpecName(), 0));
            assertTrue(consumer.tryToVerifySignatureWith(ecPublicKey,
                    JwaAlgorithms.ES_SHA_512.getSpecName()));
            assertTrue(consumer.verifySignatureWith(ecPublicKey,
                    JwaAlgorithms.ES_SHA_512.getSpecName(), 1));
            assertTrue(consumer.tryToVerifySignatureWith(hmacKey,
                    JwaAlgorithms.HMAC_SHA_256.getSpecName()));
            assertTrue(consumer.verifySignatureWith(hmacKey,
                    JwaAlgorithms.HMAC_SHA_256.getSpecName(), 2));
            // System.out.println(jws.getJsonSerialisation(true));
        } finally {
            Security.removeProvider(BouncyCastleProvider.class.getName());
        }
    }

    private String readKeySet(String fileName) {
        InputStream is = this.getClass().getClassLoader()
                .getResourceAsStream(fileName);
        StringBuilder sb = new StringBuilder(1024);

        try {
            for (int i = is.read(); i != -1; i = is.read()) {
                sb.append((char) i);
            }

            is.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return sb.toString();
    }

}

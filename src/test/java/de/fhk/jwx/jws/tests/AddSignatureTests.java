/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jws.tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jws.JwsDocument;
import de.fhk.jwx.jws.JwsMaker;
import de.fhk.jwx.jws.JwsProtectedHeader;
import de.fhk.jwx.jws.JwsUnprotectedHeader;
import de.fhk.jwx.jws.crypto.HmacJwsSigner;
import de.fhk.jwx.jws.crypto.HmacJwsVerifier;
import de.fhk.jwx.jws.crypto.JwsSigner;
import de.fhk.jwx.jws.crypto.JwsVerifier;

public class AddSignatureTests {
    @Test
    public void addSignatureTest()  {
        String jwsAsString = "{"
                + "\"payload\" : \"SXTigJlzIGEgZGFuZ2Vyb3VzIGJ1c2luZXNzLCBG"
                + "cm9kbywgZ29pbmcgb3V0IHlvdXIgZG9vci4gWW91IHN0ZXAgb250byB0"
                + "aGUgcm9hZCwgYW5kIGlmIHlvdSBkb24ndCBrZWVwIHlvdXIgZmVldCwg"
                + "dGhlcmXigJlzIG5vIGtub3dpbmcgd2hlcmUgeW91IG1pZ2h0IGJlIHN3"
                + "ZXB0IG9mZiB0by4\","
                + "\"signatures\" : [ {"
                + "\"protected\" : \"eyJhbGciOiJIUzI1NiIsImtpZCI6IjAxOGMwYWU"
                + "1LTRkOWItNDcxYi1iZmQ2LWVlZjMxNGJjNzAzNyJ9\","
                + "\"signature\" : \"s0h6KThzkfBBBkLspW1h84VsJZFTsPPqMDA7g1"
                + "Md7p0\""
                + "} ]"
                + "}";

    JwsDocument jws = JwsMaker.generateFromJws(jwsAsString);
    JwsVerifier verifier = new HmacJwsVerifier("hJtXIZ2uSN5kbQfbtTNWbpdmhkV8FJG-Onbc6mxCcYg");
    
    assertTrue(jws.verifySignatureWith(verifier));
    
    JwsProtectedHeader protectedHeader = new JwsProtectedHeader();
    protectedHeader.setAlgorithm(JwaAlgorithms.HMAC_SHA_256.getSpecName());
    
    JwsUnprotectedHeader unprotectedHeader = new JwsUnprotectedHeader();
    unprotectedHeader.setKeyId("secondKeyId");
    
    JwsSigner signer = new HmacJwsSigner("dGhpc2lzdGhlc2Vjb3VuZGtleXRvc2lnbmluZ2Fqd3M");
                            
    jws.additionallySignWith(signer, protectedHeader, unprotectedHeader);
    }

}

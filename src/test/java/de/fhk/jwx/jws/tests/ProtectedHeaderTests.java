/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jws.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jws.JwsProtectedHeader;

public class ProtectedHeaderTests {
	private static final String keyId = "bilbo.baggins@hobbiton.example";
	private static final Object PROTECTED_HEADER = ("{"
	    + "\"alg\": \"RS256\","
	    + "\"kid\": \"bilbo.baggins@hobbiton.example\""
	    + "}").replaceAll(" ", "");
	private static final Object ENCODED_PROTECTED_HEADER = "eyJhbGciOiJSUzI1NiIsImtpZCI6ImJpbGJvLmJhZ2dpbnNAaG9iYml0b24uZX"
		+ "hhbXBsZSJ9";

	@Test
	public void protectedHeaderTest(){
		JwsProtectedHeader protectedHeader = new JwsProtectedHeader();
		protectedHeader.setAlgorithm(JwaAlgorithms.RS_SHA_256.getSpecName());
		protectedHeader.setKeyId(keyId);
		assertEquals(protectedHeader.getHeaders(), PROTECTED_HEADER);
		assertEquals(protectedHeader.getBase64URLEncodedHeaders(), ENCODED_PROTECTED_HEADER);
	}
}

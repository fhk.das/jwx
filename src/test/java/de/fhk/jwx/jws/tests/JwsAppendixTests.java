/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jws.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.jws.JwsDocument;
import de.fhk.jwx.jws.JwsMaker;
import de.fhk.jwx.jws.JwsProtectedHeader;
import de.fhk.jwx.jws.JwsUnprotectedHeader;
import de.fhk.jwx.jws.crypto.NoneJwsSigner;
import de.fhk.jwx.jws.crypto.NoneJwsVerifier;

public class JwsAppendixTests {

    private static final String A_PAYLOAD = "{\"iss\":\"joe\",\r\n"
            + " \"exp\":1300819380,\r\n"
            + " \"http://example.com/is_root\":true}";
    private static final String A1_JWK = "{\"kty\":\"oct\",\r\n"
            + " \"k\":\"AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75"
            + "aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow\"\r\n" + "}";
    private static final String A1_JWS = "eyJ0eXAiOiJKV1QiLA0KICJhbGciOiJIUzI1NiJ9"
            + "."
            + "eyJpc3MiOiJqb2UiLA0KICJleHAiOjEzMDA4MTkzODAsDQogImh0dHA6Ly9leGFt"
            + "cGxlLmNvbS9pc19yb290Ijp0cnVlfQ"
            + "."
            + "dBjftJeZ4CVP-mB92K27uhbUJU1p1r_wW1gFWFOEjXk";

    private static final String A2_JWK = "{\"kty\":\"RSA\","
            + "\"n\":\"ofgWCuLjybRlzo0tZWJjNiuSfb4p4fAkd_wWJcyQoTbji9k0l8W26mPddx"
            + "HmfHQp-Vaw-4qPCJrcS2mJPMEzP1Pt0Bm4d4QlL-yRT-SFd2lZS-pCgNMs"
            + "D1W_YpRPEwOWvG6b32690r2jZ47soMZo9wGzjb_7OMg0LOL-bSf63kpaSH"
            + "SXndS5z5rexMdbBYUsLA9e-KXBdQOS-UTo7WTBEMa2R2CapHg665xsmtdV"
            + "MTBQY4uDZlxvb3qCo5ZwKh9kG4LT6_I5IhlJH7aGhyxXFvUK-DWNmoudF8"
            + "NAco9_h9iaGNj8q2ethFkMLs91kzk2PAcDTW9gb54h4FRWyuXpoQ\","
            + "\"e\":\"AQAB\","
            + "\"d\":\"Eq5xpGnNCivDflJsRQBXHx1hdR1k6Ulwe2JZD50LpXyWPEAeP88vLNO97I"
            + "jlA7_GQ5sLKMgvfTeXZx9SE-7YwVol2NXOoAJe46sui395IW_GO-pWJ1O0"
            + "BkTGoVEn2bKVRUCgu-GjBVaYLU6f3l9kJfFNS3E0QbVdxzubSu3Mkqzjkn"
            + "439X0M_V51gfpRLI9JYanrC4D4qAdGcopV_0ZHHzQlBjudU2QvXt4ehNYT"
            + "CBr6XCLQUShb1juUO1ZdiYoFaFQT5Tw8bGUl_x_jTj3ccPDVZFD9pIuhLh"
            + "BOneufuBiB4cS98l2SR_RQyGWSeWjnczT0QU91p1DhOVRuOopznQ\","
            + "\"p\":\"4BzEEOtIpmVdVEZNCqS7baC4crd0pqnRH_5IB3jw3bcxGn6QLvnEtfdUdi"
            + "YrqBdss1l58BQ3KhooKeQTa9AB0Hw_Py5PJdTJNPY8cQn7ouZ2KKDcmnPG"
            + "BY5t7yLc1QlQ5xHdwW1VhvKn-nXqhJTBgIPgtldC-KDV5z-y2XDwGUc\","
            + "\"q\":\"uQPEfgmVtjL0Uyyx88GZFF1fOunH3-7cepKmtH4pxhtCoHqpWmT8YAmZxa"
            + "ewHgHAjLYsp1ZSe7zFYHj7C6ul7TjeLQeZD_YwD66t62wDmpe_HlB-TnBA"
            + "-njbglfIsRLtXlnDzQkv5dTltRJ11BKBBypeeF6689rjcJIDEz9RWdc\","
            + "\"dp\":\"BwKfV3Akq5_MFZDFZCnW-wzl-CCo83WoZvnLQwCTeDv8uzluRSnm71I3Q"
            + "CLdhrqE2e9YkxvuxdBfpT_PI7Yz-FOKnu1R6HsJeDCjn12Sk3vmAktV2zb"
            + "34MCdy7cpdTh_YVr7tss2u6vneTwrA86rZtu5Mbr1C1XsmvkxHQAdYo0\","
            + "\"dq\":\"h_96-mK1R_7glhsum81dZxjTnYynPbZpHziZjeeHcXYsXaaMwkOlODsWa"
            + "7I9xXDoRwbKgB719rrmI2oKr6N3Do9U0ajaHF-NKJnwgjMd2w9cjz3_-ky"
            + "NlxAr2v4IKhGNpmM5iIgOS1VZnOZ68m6_pbLBSp3nssTdlqvd0tIiTHU\","
            + "\"qi\":\"IYd7DHOhrWvxkwPQsRM2tOgrjbcrfvtQJipd-DlcxyVuuM9sQLdgjVk2o"
            + "y26F0EmpScGLq2MowX7fhd_QJQ3ydy5cY7YIBi87w93IKLEdfnbJtoOPLU"
            + "W0ITrJReOgo1cq9SbsxYawBgfp_gh6A5603k2-ZQwVK0JKSHuLFkuQ3U\""
            + "}";
    private static final Object A2_JWS = "eyJhbGciOiJSUzI1NiJ9"
            + "."
            + "eyJpc3MiOiJqb2UiLA0KICJleHAiOjEzMDA4MTkzODAsDQogImh0dHA6Ly9leGFt"
            + "cGxlLmNvbS9pc19yb290Ijp0cnVlfQ"
            + "."
            + "cC4hiUPoj9Eetdgtv3hF80EGrhuB__dzERat0XF9g2VtQgr9PJbu3XOiZj5RZmh7"
            + "AAuHIm4Bh-0Qc_lF5YKt_O8W2Fp5jujGbds9uJdbF9CUAr7t1dnZcAcQjbKBYNX4"
            + "BAynRFdiuB--f_nZLgrnbyTyWzO75vRK5h6xBArLIARNPvkSjtQBMHlb1L07Qe7K"
            + "0GarZRmB_eSN9383LcOLn6_dO--xi12jzDwusC-eOkHWEsqtFZESc6BfI7noOPqv"
            + "hJ1phCnvWh6IeYI2w9QOYEUipUTI8np6LbgGY9Fs98rqVt5AXLIhWkWywlVmtVrB"
            + "p0igcN_IoypGlUPQGe77Rw";
    private static final String A3_JWK = "{\"kty\":\"EC\","
            + "\"crv\":\"P-256\","
            + "\"x\":\"f83OJ3D2xF1Bg8vub9tLe1gHMzV76e8Tus9uPHvRVEU\","
            + "\"y\":\"x_FEzRu9m36HLN_tue659LNpXW6pCyStikYjKIWI5a0\","
            + "\"d\":\"jpsQnnGQmL-YBIffH1136cspYG6-0iY7X1fCE9-E9LI\"" + "}";
    private static final String A3_JWS = "eyJhbGciOiJFUzI1NiJ9"
            + "."
            + "eyJpc3MiOiJqb2UiLA0KICJleHAiOjEzMDA4MTkzODAsDQogImh0dHA6Ly9leGFt"
            + "cGxlLmNvbS9pc19yb290Ijp0cnVlfQ"
            + "."
            + "DtEhU3ljbEg8L38VWAfUAqOyKAM6-Xx-F4GawxaepmXFCgfTjDxw5djxLa8ISlSA"
            + "pmWQxfKTUJqPP3-Kg6NU1Q";
    private static final String A4_JWK = "{\"kty\":\"EC\","
            + "\"crv\":\"P-521\","
            + "\"x\":\"AekpBQ8ST8a8VcfVOTNl353vSrDCLLJXmPk06wTjxrrjcBpXp5EOnYG_NjFZ6OvLFV1jSfS9tsz4qUxcWceqwQGk\","
            + "\"y\":\"ADSmRA43Z1DSNx_RvcLI87cdL07l6jQyyBXMoxVg_l2Th-x3S1WDhjDly79ajL4Kkd0AZMaZmh9ubmf63e3kyMj2\","
            + "\"d\":\"AY5pb7A0UFiB3RELSD64fTLOSV_jazdF7fLYyuTw8lOfRhWg6Y6rUrPAxerEzgdRhajnu0ferB0d53vM9mE15j2C\""
            + "}";
    private static final String A4_JWS = "eyJhbGciOiJFUzUxMiJ9"
            + "."
            + "UGF5bG9hZA"
            + "."
            + "AdwMgeerwtHoh-l192l60hp9wAHZFVJbLfD_UxMi70cwnZOYaRI1bKPWROc-mZZq"
            + "wqT2SI-KGDKB34XO0aw_7XdtAG8GaSwFKdCAPZgoXD2YBJZCPEX3xKpRwcdOO8Kp"
            + "EHwJjyqOgzDO7iKvU8vcnwNrmxYbSW9ERBXukOXolLzeO_Jn";
    private static final String A5_JWS = "eyJhbGciOiJub25lIn0"
            + "."
            + "eyJpc3MiOiJqb2UiLA0KICJleHAiOjEzMDA4MTkzODAsDQogImh0dHA6Ly9leGFt"
            + "cGxlLmNvbS9pc19yb290Ijp0cnVlfQ" + ".";
    private static final String A6_JWS = ("{"
            + "\"payload\":"
            + "\"eyJpc3MiOiJqb2UiLA0KICJleHAiOjEzMDA4MTkzODAsDQogImh0dHA6Ly9leGF"
            + "tcGxlLmNvbS9pc19yb290Ijp0cnVlfQ\"," + "\"signatures\":["
            + "{\"protected\":\"eyJhbGciOiJSUzI1NiJ9\"," + "\"header\":"
            + "{\"kid\":\"2010-12-29\"}," + "\"signature\":"
            + "\"cC4hiUPoj9Eetdgtv3hF80EGrhuB__dzERat0XF9g2VtQgr9PJbu3XOiZj5RZ"
            + "mh7AAuHIm4Bh-0Qc_lF5YKt_O8W2Fp5jujGbds9uJdbF9CUAr7t1dnZcAcQjb"
            + "KBYNX4BAynRFdiuB--f_nZLgrnbyTyWzO75vRK5h6xBArLIARNPvkSjtQBMHl"
            + "b1L07Qe7K0GarZRmB_eSN9383LcOLn6_dO--xi12jzDwusC-eOkHWEsqtFZES"
            + "c6BfI7noOPqvhJ1phCnvWh6IeYI2w9QOYEUipUTI8np6LbgGY9Fs98rqVt5AX"
            + "LIhWkWywlVmtVrBp0igcN_IoypGlUPQGe77Rw\"},"
            + "{\"protected\":\"eyJhbGciOiJFUzI1NiJ9\"," + "\"header\":"
            + "{\"kid\":\"e9bc097a-ce51-4036-9562-d2ade882db0d\"},"
            + "\"signature\":"
            + "\"DtEhU3ljbEg8L38VWAfUAqOyKAM6-Xx-F4GawxaepmXFCgfTjDxw5djxLa8IS"
            + "lSApmWQxfKTUJqPP3-Kg6NU1Q\"}]" + "}").replace(" ", "");
    private static final String A7_JWS = "{"
            + "\"payload\":"
            + "\"eyJpc3MiOiJqb2UiLA0KICJleHAiOjEzMDA4MTkzODAsDQogImh0dHA6Ly9leGF"
            + "tcGxlLmNvbS9pc19yb290Ijp0cnVlfQ\","
            + "\"protected\":\"eyJhbGciOiJFUzI1NiJ9\"," + "\"header\":"
            + "{\"kid\":\"e9bc097a-ce51-4036-9562-d2ade882db0d\"},"
            + "\"signature\":"
            + "\"DtEhU3ljbEg8L38VWAfUAqOyKAM6-Xx-F4GawxaepmXFCgfTjDxw5djxLa8IS"
            + "lSApmWQxfKTUJqPP3-Kg6NU1Q\"" + "}";

    //@Test
    public void A1Test()  {
        JwsProtectedHeader protectedHeader = new JwsProtectedHeader();
        protectedHeader.setType("JWT");
        protectedHeader.setAlgorithm(JwaAlgorithms.HMAC_SHA_256.getSpecName());
        JwkKey jwk = new JwkKey(A1_JWK);
        JwsDocument jws = JwsMaker.generateFromPayload(A_PAYLOAD,
                protectedHeader, jwk);
        // System.out.println(jws.getCompactSerialisation());
        assertEquals(jws.getCompactSerialisation(), A1_JWS);
        assertTrue(jws.verifySignatureWith(jwk));
    }

    @Test
    public void A2Test() {
        JwsProtectedHeader protectedHeader = new JwsProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.RS_SHA_256.getSpecName());
        JwkKey jwk = new JwkKey(A2_JWK);
        JwsDocument jws = JwsMaker.generateFromPayload(A_PAYLOAD,
                protectedHeader, jwk);

        assertEquals(jws.getCompactSerialisation(), A2_JWS);
        assertTrue(jws.verifySignatureWith(jwk));
    }

    @Test
    public void A3Test() {
        JwsProtectedHeader protectedHeader = new JwsProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.ES_SHA_256.getSpecName());
        JwkKey jwk = new JwkKey(A3_JWK);
        JwsDocument jws = JwsMaker.generateFromPayload(A_PAYLOAD,
                protectedHeader, jwk);
        assertEquals(jws.getCompactSerialisation().length(), A3_JWS.length());
        assertTrue(jws.verifySignatureWith(jwk));
    }

    @Test
    public void A4Test(){
        JwsProtectedHeader protectedHeader = new JwsProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.ES_SHA_512.getSpecName());
        JwkKey jwk = new JwkKey(A4_JWK);
        JwsDocument jws = JwsMaker.generateFromPayload("Payload",
                protectedHeader, jwk);
        assertEquals(jws.getCompactSerialisation().length(), A4_JWS.length());
        assertTrue(jws.verifySignatureWith(jwk));
    }

    @Test
    public void A5Test(){
        JwsProtectedHeader protectedHeader = new JwsProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.PLAIN_TEXT.getSpecName());
        JwsDocument jws = JwsMaker.generateFromPayload(A_PAYLOAD,
                protectedHeader, new NoneJwsSigner());
        assertEquals(jws.getCompactSerialisation(), A5_JWS);
        assertTrue(jws.verifySignatureWith(new NoneJwsVerifier()));
    }

    @Test
    public void A6A7Test() {
        JwsProtectedHeader protectedHeaderSigner1 = new JwsProtectedHeader();
        protectedHeaderSigner1.setAlgorithm(JwaAlgorithms.RS_SHA_256
                .getSpecName());
        JwsUnprotectedHeader unprotectedHeaderSigner1 = new JwsUnprotectedHeader();
        unprotectedHeaderSigner1.setKeyId("2010-12-29");
        JwkKey jwkSigner1 = new JwkKey(A2_JWK);
        JwsDocument jws = JwsMaker.generateFromPayload(A_PAYLOAD,
                protectedHeaderSigner1, unprotectedHeaderSigner1, jwkSigner1);

        JwsProtectedHeader protectedHeaderSigner2 = new JwsProtectedHeader();
        protectedHeaderSigner2.setAlgorithm(JwaAlgorithms.ES_SHA_256
                .getSpecName());
        JwsUnprotectedHeader unprotectedHeaderSigner2 = new JwsUnprotectedHeader();
        unprotectedHeaderSigner2
                .setKeyId("e9bc097a-ce51-4036-9562-d2ade882db0d");
        JwkKey jwkSigner2 = new JwkKey(A3_JWK);
        jws.additionallySignWith(jwkSigner2, protectedHeaderSigner2,
                unprotectedHeaderSigner2);
        assertEquals(jws.getJsonSerialisation().length(), A6_JWS.length());

        assertTrue(jws.verifySignatureWith(jwkSigner1, 0));
        assertTrue(jws.verifySignatureWith(jwkSigner2, 1));
        assertEquals(jws.getJwsDocument(1).getJsonFlattenedSerialisation()
                .length(), A7_JWS.length());
    }

}

/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jws.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.security.InvalidAlgorithmParameterException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Security;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.junit.Test;

import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jws.JwsDocument;
import de.fhk.jwx.jws.JwsMaker;
import de.fhk.jwx.jws.JwsProtectedHeader;

public class SignJwsDirectlyWithKeyTests {
    private static final String keyId = "018c0ae5-4d9b-471b-bfd6-eef314bc7037";
    private static final String PAYLOAD = "It’s a dangerous business, Frodo, going out your door. You step onto the road, and if you don't keep your feet, there’s no knowing where you might be swept off to.";
    private static final byte[] secretKeyinBytes = { 72, 97, 115, 104, 45, 98,
            97, 115, 101, 100, 32, 77, 101, 115, 115, 97, 103, 101, 32, 65,
            117, 116, 104, 101, 110, 116, 105, 99, 97, 116, 105, 111, 110, 32,
            67, 111, 100, 101, 115, 32, 40, 72, 77, 65, 67, 115, 41, 32, 101,
            110, 97, 98, 108, 101, 32, 111, 110, 101, 32, 116, 111, 32, 117,
            115, 101, 32, 97, 32, 115, 101, 99, 114, 101, 116, 32, 112, 108,
            117, 115, 32, 97, 32, 99, 114, 121, 112, 116, 111, 103, 114, 97,
            112, 104, 105, 99, 32, 104, 97, 115, 104, 32, 102, 117, 110, 99,
            116, 105, 111, 110, 32, 116, 111, 32, 103, 101, 110, 101, 114, 97,
            116, 101, 32, 97, 32, 77, 101, 115, 115, 97, 103, 101, 32, 65, 117,
            116, 104, 101, 110, 116, 105, 99, 97, 116, 105, 111, 110, 32, 67,
            111, 100, 101, 32, 40, 77, 65, 67, 41, 46, 32, 32, 84, 104, 105,
            115, 32, 99, 97, 110, 32, 98, 101, 32, 117, 115, 101, 100, 32, 116,
            111, 32, 100, 101, 109, 111, 110, 115, 116, 114, 97, 116, 101, 32,
            116, 104, 97, 116, 32, 119, 104, 111, 101, 118, 101, 114, 32, 103,
            101, 110, 101, 114, 97, 116, 101, 100, 32, 116, 104, 101, 32, 77,
            65, 67, 32, 119, 97, 115, 32, 105, 110, 32, 112, 111, 115, 115,
            101, 115, 115, 105, 111, 110, 32, 111, 102, 32, 116, 104, 101, 32,
            77, 65, 67, 32, 107, 101, 121, 46, 32, 84, 104, 101, 32, 97, 108,
            103, 111, 114, 105, 116, 104, 109, 32, 102, 111, 114, 32, 105, 109,
            112, 108, 101, 109, 101, 110, 116, 105, 110, 103, 32, 97, 110, 100,
            32, 118, 97, 108, 105, 100, 97, 116, 105, 110, 103, 32, 72, 77, 65,
            67, 115, 32, 105, 115, 32, 112, 114, 111, 118, 105, 100, 101, 100,
            32, 105, 110, 32, 82, 70, 67, 50, 49, 48, 52, 32, 91, 82, 70, 67,
            50, 49, 48, 52, 93, 46, 32, 65, 32, 107, 101, 121, 32, 111, 102,
            32, 116, 104, 101, 32, 115, 97, 109, 101, 32, 115, 105, 122, 101,
            32, 97, 115, 32, 116, 104, 101, 32, 104, 97, 115, 104, 32, 111,
            117, 116, 112, 117, 116, 32, 40, 102, 111, 114, 32, 105, 110, 115,
            116, 97, 110, 99, 101, 44, 32, 50, 53, 54, 32, 98, 105, 116, 115,
            32, 102, 111, 114, 32, 34, 72, 83, 50, 53, 54, 34, 41, 32, 111,
            114, 32, 108, 97, 114, 103, 101, 114, 32, 77, 85, 83, 84, 32, 98,
            101, 32, 117, 115, 101, 100, 32, 119, 105, 116, 104, 32, 116, 104,
            105, 115, 32, 97, 108, 103, 111, 114, 105, 116, 104, 109, 46, 32,
            104, 116, 116, 112, 58, 47, 47, 116, 111, 111, 108, 115, 46, 105,
            101, 116, 102, 46, 111, 114, 103, 47, 104, 116, 109, 108, 47, 100,
            114, 97, 102, 116, 45, 105, 101, 116, 102, 45, 106, 111, 115, 101,
            45, 106, 115, 111, 110, 45, 119, 101, 98, 45, 97, 108, 103, 111,
            114, 105, 116, 104, 109, 115, 45, 51, 48 };
    private final static Key SECRET_KEY = new SecretKeySpec(secretKeyinBytes,
            "");
    private static final String HMAC_SHA_JWS = "eyJhbGciOiJIUzI1NiIsImtpZCI6IjAxOGMwYWU1LTRkOWItNDcxYi1iZmQ2LWVlZjMxNGJjNzAzNyJ9.SXTigJlzIGEgZGFuZ2Vyb3VzIGJ1c2luZXNzLCBGcm9kbywgZ29pbmcgb3V0IHlvdXIgZG9vci4gWW91IHN0ZXAgb250byB0aGUgcm9hZCwgYW5kIGlmIHlvdSBkb24ndCBrZWVwIHlvdXIgZmVldCwgdGhlcmXigJlzIG5vIGtub3dpbmcgd2hlcmUgeW91IG1pZ2h0IGJlIHN3ZXB0IG9mZiB0by4.AXrBAC3kz-1AbHT-31xGA8UjLS0pmf_JxK-shevhSm4";

    @Test
    public void SignWithKeyTest(){
        JwsProtectedHeader protectedHeader = new JwsProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.HMAC_SHA_256.getSpecName());
        protectedHeader.setKeyId(keyId);
        JwsDocument jws = JwsMaker.generateFromPayload(PAYLOAD,
                protectedHeader, SECRET_KEY);
        assertEquals(jws.getCompactSerialisation(), HMAC_SHA_JWS);
        assertTrue(jws.verifySignatureWith(SECRET_KEY));
    }

    @Test
    public void SignWithRSAKeyTest(){
        KeyPairGenerator keyPairGenerator;
        try {
            keyPairGenerator = KeyPairGenerator.getInstance("RSA");
 
        keyPairGenerator.initialize(2048);
        KeyPair keyPair = keyPairGenerator.genKeyPair();

        JwsProtectedHeader protectedHeader = new JwsProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.RS_SHA_512.getSpecName());
        protectedHeader.setKeyId("automatic generated rsa key pair");
        JwsDocument jws = JwsMaker.generateFromPayload(PAYLOAD,
                protectedHeader, keyPair.getPrivate());
        assertTrue(jws.verifySignatureWith(keyPair.getPublic()));
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
    @Test
    public void SignWithRSAPSSKeyTest(){
        try {
            Cipher.getInstance(JwaAlgorithms.PS_SHA_384.getJavaName());
        } catch (Throwable t) {
            Security.addProvider(new BouncyCastleProvider());
        }
        KeyPairGenerator keyPairGenerator;
        try {
            keyPairGenerator = KeyPairGenerator.getInstance("RSA");

        keyPairGenerator.initialize(2048);
        KeyPair keyPair = keyPairGenerator.genKeyPair();

        JwsProtectedHeader protectedHeader = new JwsProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.PS_SHA_512.getSpecName());
        protectedHeader.setKeyId("automatic generated rsa key pair");
        JwsDocument jws = JwsMaker.generateFromPayload(PAYLOAD,
                protectedHeader, keyPair.getPrivate());
        assertTrue(jws.verifySignatureWith(keyPair.getPublic()));
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    @Test
    public void SignWithECKeyTest(){
        try {
            Cipher.getInstance(JwaAlgorithms.ES_SHA_512.getJavaName());
        } catch (Throwable t) {
            Security.addProvider(new BouncyCastleProvider());
        }
        ECParameterSpec ecSpec = ECNamedCurveTable.getParameterSpec("P-521");
        KeyPairGenerator g;
        try {
            g = KeyPairGenerator.getInstance("ECDSA", "BC");

        g.initialize(ecSpec, new SecureRandom());
        KeyPair pair = g.generateKeyPair();

        JwsProtectedHeader protectedHeader = new JwsProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.ES_SHA_512.getSpecName());
        protectedHeader.setKeyId("automatic generated ec key pair");
        JwsDocument jws = JwsMaker.generateFromPayload(PAYLOAD,
                protectedHeader, pair.getPrivate());
        assertTrue(jws.verifySignatureWith(pair.getPublic()));
        } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidAlgorithmParameterException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    @Test
    public void SignWithNoneKeyTest(){
        Key noneKey = null;

        JwsProtectedHeader protectedHeader = new JwsProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.PLAIN_TEXT.getSpecName());
        JwsDocument jws = JwsMaker.generateFromPayload(PAYLOAD,
                protectedHeader, noneKey);
        assertTrue(jws.verifySignatureWith(noneKey));
    }

}

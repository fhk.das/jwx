/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jws.tests;

import java.util.ArrayList;
import java.util.List;

import javax.crypto.spec.SecretKeySpec;

import org.junit.Test;

import de.fhk.jwx.JoseHeaders;
import de.fhk.jwx.jwa.JwaAlgorithms;
import de.fhk.jwx.jws.JwsDocument;
import de.fhk.jwx.jws.JwsMaker;
import de.fhk.jwx.jws.JwsProtectedHeader;
import de.fhk.jwx.jws.JwsUnprotectedHeader;
import de.fhk.jwx.jws.crypto.HmacJwsSigner;
import de.fhk.jwx.jws.crypto.NoneJwsVerifier;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class JwsDestructiveTests {

    private static final byte[] secretKeyinBytes = { 72, 97, 115, 104, 45, 98,
            97, 115, 101, 100, 32, 77, 101, 115, 115, 97, 103, 101, 32, 65,
            117, 116, 104, 101, 110, 116, 105, 99, 97, 116, 105, 111, 110, 32,
            67, 111, 100, 101, 115, 32, 40, 72, 77, 65, 67, 115, 41, 32, 101,
            110, 97, 98, 108, 101, 32, 111, 110, 101, 32, 116, 111, 32, 117,
            115, 101, 32, 97, 32, 115, 101, 99, 114, 101, 116, 32, 112, 108,
            117, 115, 32, 97, 32, 99, 114, 121, 112, 116, 111, 103, 114, 97,
            112, 104, 105, 99, 32, 104, 97, 115, 104, 32, 102, 117, 110, 99,
            116, 105, 111, 110, 32, 116, 111, 32, 103, 101, 110, 101, 114, 97,
            116, 101, 32, 97, 32, 77, 101, 115, 115, 97, 103, 101, 32, 65, 117,
            116, 104, 101, 110, 116, 105, 99, 97, 116, 105, 111, 110, 32, 67,
            111, 100, 101, 32, 40, 77, 65, 67, 41, 46, 32, 32, 84, 104, 105,
            115, 32, 99, 97, 110, 32, 98, 101, 32, 117, 115, 101, 100, 32, 116,
            111, 32, 100, 101, 109, 111, 110, 115, 116, 114, 97, 116, 101, 32,
            116, 104, 97, 116, 32, 119, 104, 111, 101, 118, 101, 114, 32, 103,
            101, 110, 101, 114, 97, 116, 101, 100, 32, 116, 104, 101, 32, 77,
            65, 67, 32, 119, 97, 115, 32, 105, 110, 32, 112, 111, 115, 115,
            101, 115, 115, 105, 111, 110, 32, 111, 102, 32, 116, 104, 101, 32,
            77, 65, 67, 32, 107, 101, 121, 46, 32, 84, 104, 101, 32, 97, 108,
            103, 111, 114, 105, 116, 104, 109, 32, 102, 111, 114, 32, 105, 109,
            112, 108, 101, 109, 101, 110, 116, 105, 110, 103, 32, 97, 110, 100,
            32, 118, 97, 108, 105, 100, 97, 116, 105, 110, 103, 32, 72, 77, 65,
            67, 115, 32, 105, 115, 32, 112, 114, 111, 118, 105, 100, 101, 100,
            32, 105, 110, 32, 82, 70, 67, 50, 49, 48, 52, 32, 91, 82, 70, 67,
            50, 49, 48, 52, 93, 46, 32, 65, 32, 107, 101, 121, 32, 111, 102,
            32, 116, 104, 101, 32, 115, 97, 109, 101, 32, 115, 105, 122, 101,
            32, 97, 115, 32, 116, 104, 101, 32, 104, 97, 115, 104, 32, 111,
            117, 116, 112, 117, 116, 32, 40, 102, 111, 114, 32, 105, 110, 115,
            116, 97, 110, 99, 101, 44, 32, 50, 53, 54, 32, 98, 105, 116, 115,
            32, 102, 111, 114, 32, 34, 72, 83, 50, 53, 54, 34, 41, 32, 111,
            114, 32, 108, 97, 114, 103, 101, 114, 32, 77, 85, 83, 84, 32, 98,
            101, 32, 117, 115, 101, 100, 32, 119, 105, 116, 104, 32, 116, 104,
            105, 115, 32, 97, 108, 103, 111, 114, 105, 116, 104, 109, 46, 32,
            104, 116, 116, 112, 58, 47, 47, 116, 111, 111, 108, 115, 46, 105,
            101, 116, 102, 46, 111, 114, 103, 47, 104, 116, 109, 108, 47, 100,
            114, 97, 102, 116, 45, 105, 101, 116, 102, 45, 106, 111, 115, 101,
            45, 106, 115, 111, 110, 45, 119, 101, 98, 45, 97, 108, 103, 111,
            114, 105, 116, 104, 109, 115, 45, 51, 48 };
    private static final String JWS_TEST_CASE_2 = "{\"payload\":\"eyJwYXlsb2FkIiA6ICJ0aG"
            + "UgY29udGVudCB0aGF0IHVzZWQgdG8gc2lnbi4ifQ\",\"signatures\":[{\"protected\""
            + ":\"eyJjcml0IjpbImV4cCJdLCJleHAiOjE3Mzk3NjAyMTgxMzUsImFsZyI6IkhTNTEyIn0\","
            + "\"header\":{\"crit\":[\"exp\"]},\"signature\":\"7A61oCma-nvo5plmWx9zEqlam"
            + "OQ14kue74ZVAW_SQRjXJyZ-MMsyhR1dn9VOqopaF1PAY-Uhgp2QzZ5vXmlqmQ\"}]}";

    private static final String JWS_TEST_CASE_1 = "{\"payload\":\"eyJwYXlsb2FkIiA6ICJ0aG"
            + "UgY29udGVudCB0aGF0IHVzZWQgdG8gc2lnbi4ifQ\",\"signatures\":[{\"protected\""
            + ":\"eyJhbGciOiJIUzUxMiJ9\",\"header\":{\"alg\":\"HS512\"},\"signature\":\""
            + "qXyLBC8bcgwxd75lf9ex2FMK9uJGwuoArtkZ6aBpTHWZWDn8W2-wY3wy2Hkdoxp_Migtoyrpx"
            + "fvkGDXW5yw8aQ\"}]}";

    private static final String JWS_TEST_CASE_4 = "eyJhbGciOiJIUzUxMiIsImFsZyI6IkhTNTEyI"
            + "n0.dGhlIGNvbnRlbnQgdGhhdCB1c2VkIHRvIHNpZ24u.kSp_2hACw7ld8wHqOvpasRgM5iufE"
            + "5Fo7heLeSW0-HLw0uEJRu1t3MUtcbN--X307Jh7QhD30u6BY_rSdJuVlA";

    private static final String JWS_TEST_CASE_3 = "{\"payload\":\"eyJwYXlsb2FkIiA6ICJ0aG"
            + "UgY29udGVudCB0aGF0IHVzZWQgdG8gc2lnbi4ifQ\",\"signatures\":[{\"protected\""
            + ":\"eyJleHAiOjE3Mzk3NjAyMTgxMzUsImFsZyI6IkhTNTEyIn0\",\"header\":{\"crit\""
            + ":[\"exp\"]},\"signature\":\"inICtBn1_3OwGRoJEfKbbSqhMfaj-UOegxT2ikcHDE1cK"
            + "XgulmX1zGBgvXlBotnl-G8bg8Tslf95_Z7F8Nfhww\"}]}";

    private static final String JWS_TEST_CASE_5 = "eyJhbGciOiJIUzM4NCIsImFsZyI6IkhTNTEyI"
            + "n0.dGhlIGNvbnRlbnQgdGhhdCB1c2VkIHRvIHNpZ24u.-I2xpOIYekmSM8YvlowwkEZwgPtlh"
            + "7LxLho5wcu-8EvqsvjKArgahMgs7WBwLYbqEWRTZU5AlUGubDt_nPb8sQ";

    private static final Object JWS_TEST_CASE_6 = "{\"payload\":\"dGhlIGNvbnRlbnQgdGhhdC"
            + "B1c2VkIHRvIHNpZ24u\",\"signatures\":[{\"header\":{\"alg\":\"HS512\"},\"si"
            + "gnature\":\"Dda3DBwNNZ7rd5i9HleXJy_v-eDn7HsNniemiHUR2b3xeG_5OvSilVy-udlO4"
            + "bXHy5i-ur0zLHuhH7WBh9lGWg\"}]}";

    private static final String JWS_TEST_CASE_6_MANIPULATED = "{\"payload\":\"dGhlIGNvbn"
            + "RlbnQgdGhhdCB1c2VkIHRvIHNpZ24u\",\"signatures\":[{\"header\":{\"alg\":\"n"
            + "one\"},\"signature\":\"Dda3DBwNNZ7rd5i9HleXJy_v-eDn7HsNniemiHUR2b3xeG_5Ov"
            + "SilVy-udlO4bXHy5i-ur0zLHuhH7WBh9lGWg\"}]}";

    private static final String JWS_TEST_CASE_7 = "eyJhbGciOiJIUzUxMiJ9.dGhlIGNvbnRlbnQg"
            + "dGhhdCB1c2VkIHRvIHNpZ24u.C8agio4_3YEFarjFc_njftmHXpAX1tcyZ6XTTxpMotnDf0TB"
            + "p4cucZIyhUaWr_0esJNm5VZok0HSW0VI5exPUg";

    private static final String JWS_TEST_CASE_7_MANIPULATED = "eyJhbGciOiJub25lIn0.dGhlI"
            + "GNvbnRlbnQgdGhhdCB1c2VkIHRvIHNpZ24u.C8agio4_3YEFarjFc_njftmHXpAX1tcyZ6XTT"
            + "xpMotnDf0TBp4cucZIyhUaWr_0esJNm5VZok0HSW0VI5exPUgdGhlIGNvbnRlbnQgdGhhdCB1"
            + "c2VkIHRvIHNpZ24uX";

    private static final String JWS_TEST_CASE_8 = "{\"payload\":\"eyJwYXlsb2FkIiA6ICJ0aG"
            + "UgY29udGVudCB0aGF0IHVzZWQgdG8gc2lnbi4ifQ\",\"signatures\":[{\"protected\""
            + ":\"eyJhbGciOiJIUzUxMiJ9\",\"header\":{\"crit\":[\"exp\"],\"exp\":17397602"
            + "18135},\"signature\":\"qXyLBC8bcgwxd75lf9ex2FMK9uJGwuoArtkZ6aBpTHWZWDn8W2"
            + "-wY3wy2Hkdoxp_MigtoyrpxfvkGDXW5yw8aQ\"}]}";

    private static final String JWS_TEST_CASE_9 = "eyJhbGciOiJIUzUxMiJ9.dGhlIGNvbnRlbnQg"
            + "dGhhdCB1c2VkIHRvIHNpZ24u.C8agio4_3YEFarjFc_njftmHXpAX1tcyZ6XTTxpMotnDf0TB"
            + "p4cucZIyhUaWr_0esJNm5VZok0HSW0VI5exPUg";

    private static final String JWS_TEST_CASE_9_MANIPULATED = "eyJhbGciOiJIUzUxMiJ9.dGhl"
            + "IGNvbnRlbnQgdGhhdCB1c2VkIHRvIHNpZ24uX.C8agio4_3YEFarjFc_njftmHXpAX1tcyZ6X"
            + "TTxpMotnDf0TBp4cucZIyhUaWr_0esJNm5VZok0HSW0VI5exPUg";

    @Test(expected = SecurityException.class)
    public void JwsTestCase1() {
        JwsProtectedHeader protectedHeader = new JwsProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.HMAC_SHA_512.getSpecName());

        JwsUnprotectedHeader unprotectedHeader = new JwsUnprotectedHeader();
        unprotectedHeader
                .setAlgorithm(JwaAlgorithms.HMAC_SHA_512.getSpecName());

        SecretKeySpec secretKey = new SecretKeySpec(secretKeyinBytes,
                JwaAlgorithms.HMAC_SHA_512.getJavaName());
        String payload = "{\"payload\" : \"the content that used to sign.\"}";

        HmacJwsSigner signer = new HmacJwsSigner(secretKey);

        @SuppressWarnings("unused")
        JwsDocument jws = JwsMaker.generateFromPayload(payload,
                protectedHeader, unprotectedHeader, signer);
    }

    @Test(expected = SecurityException.class)
    public void JwsTestCase1Verifier() {

        JwsDocument jws = JwsMaker.generateFromJws(JWS_TEST_CASE_1);
        SecretKeySpec secretKey = new SecretKeySpec(secretKeyinBytes,
                JwaAlgorithms.HMAC_SHA_512.getJavaName());

        assertFalse(jws.verifySignatureWith(secretKey));

    }

    @Test(expected = SecurityException.class)
    public void JWSTestCase2() {
        List<String> crit = new ArrayList<String>();
        crit.add("exp");

        JwsProtectedHeader protectedHeader = new JwsProtectedHeader();
        protectedHeader.setCritical(crit);
        protectedHeader.setHeader("exp", 1739760218135l);
        protectedHeader.setAlgorithm(JwaAlgorithms.HMAC_SHA_512.getSpecName());

        JwsUnprotectedHeader unprotectedHeader = new JwsUnprotectedHeader();
        unprotectedHeader.setHeader(JoseHeaders.HEADER_CRITICAL, crit);

        SecretKeySpec secretKey = new SecretKeySpec(secretKeyinBytes,
                JwaAlgorithms.HMAC_SHA_512.getJavaName());
        String payload = "{\"payload\" : \"the content that used to sign.\"}";

        HmacJwsSigner signer = new HmacJwsSigner(secretKey);
        @SuppressWarnings("unused")
        JwsDocument jws = JwsMaker.generateFromPayload(payload,
                protectedHeader, unprotectedHeader, signer);
    }

    @Test(expected = SecurityException.class)
    public void JwsTestCase2Verifier() {

        JwsDocument jws = JwsMaker.generateFromJws(JWS_TEST_CASE_2);
        SecretKeySpec secretKey = new SecretKeySpec(secretKeyinBytes,
                JwaAlgorithms.HMAC_SHA_512.getJavaName());

        assertFalse(jws.verifySignatureWith(secretKey));

    }

    @Test(expected = SecurityException.class)
    public void JWSTestCase3() {
        List<String> crit = new ArrayList<String>();
        crit.add("exp");

        JwsProtectedHeader protectedHeader = new JwsProtectedHeader();
        protectedHeader.setHeader("exp", 1739760218135l);
        protectedHeader.setAlgorithm(JwaAlgorithms.HMAC_SHA_512.getSpecName());

        JwsUnprotectedHeader unprotectedHeader = new JwsUnprotectedHeader();
        unprotectedHeader.setHeader(JoseHeaders.HEADER_CRITICAL, crit);

        SecretKeySpec secretKey = new SecretKeySpec(secretKeyinBytes,
                JwaAlgorithms.HMAC_SHA_512.getJavaName());
        String payload = "{\"payload\" : \"the content that used to sign.\"}";

        HmacJwsSigner signer = new HmacJwsSigner(secretKey);
        @SuppressWarnings("unused")
        JwsDocument jws = JwsMaker.generateFromPayload(payload,
                protectedHeader, unprotectedHeader, signer);

    }

    public void JwsTestCase3Verifier() {

        JwsDocument jws = JwsMaker.generateFromJws(JWS_TEST_CASE_3);
        SecretKeySpec secretKey = new SecretKeySpec(secretKeyinBytes,
                JwaAlgorithms.HMAC_SHA_512.getJavaName());

        assertFalse(jws.verifySignatureWith(secretKey));

    }

    @Test(expected = SecurityException.class)
    public void JwsTestCase4() {
        JwsProtectedHeader protectedHeader = new JwsProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.HMAC_SHA_512.getSpecName());
        protectedHeader.setHeader(JoseHeaders.HEADER_ALGORITHM,
                JwaAlgorithms.HMAC_SHA_512.getSpecName());

        SecretKeySpec secretKey = new SecretKeySpec(secretKeyinBytes,
                JwaAlgorithms.HMAC_SHA_512.getJavaName());
        String payload = "{\"payload\" : \"the content that used to sign.\"}";

        HmacJwsSigner signer = new HmacJwsSigner(secretKey);

        JwsDocument jws = JwsMaker.generateFromPayload(payload,
                protectedHeader, signer);
        assertThat(jws.getCompactSerialisation(), not(equalTo(JWS_TEST_CASE_4)));

        jws = JwsMaker.generateFromJws(JWS_TEST_CASE_4);
    }

    @Test(expected = SecurityException.class)
    public void JwsTestCase5() {
        JwsProtectedHeader protectedHeader = new JwsProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.HMAC_SHA_384.getSpecName());
        protectedHeader.setHeader(JoseHeaders.HEADER_ALGORITHM,
                JwaAlgorithms.HMAC_SHA_512.getSpecName());

        SecretKeySpec secretKey = new SecretKeySpec(secretKeyinBytes,
                JwaAlgorithms.HMAC_SHA_512.getJavaName());
        String payload = "{\"payload\" : \"the content that used to sign.\"}";

        HmacJwsSigner signer = new HmacJwsSigner(secretKey);

        JwsDocument jws = JwsMaker.generateFromPayload(payload,
                protectedHeader, signer);
        assertThat(jws.getCompactSerialisation(), not(equalTo(JWS_TEST_CASE_5)));

        jws = JwsMaker.generateFromJws(JWS_TEST_CASE_5);
    }

    @Test
    public void JwsTestCase6() {
        JwsUnprotectedHeader unprotectedHeader = new JwsUnprotectedHeader();
        unprotectedHeader
                .setAlgorithm(JwaAlgorithms.HMAC_SHA_512.getSpecName());

        SecretKeySpec secretKey = new SecretKeySpec(secretKeyinBytes,
                JwaAlgorithms.HMAC_SHA_512.getJavaName());
        String payload = "the content that used to sign.";

        HmacJwsSigner signer = new HmacJwsSigner(secretKey);

        JwsDocument jws = JwsMaker.generateFromPayload(payload,
                unprotectedHeader, signer);
        assertEquals(jws.getJsonSerialisation(), JWS_TEST_CASE_6);
        jws = JwsMaker.generateFromJws(JWS_TEST_CASE_6_MANIPULATED);
        assertFalse(jws.verifySignatureWith(new NoneJwsVerifier()));
    }

    @Test
    public void JwsTestCase7() {
        JwsProtectedHeader protectedHeader = new JwsProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.HMAC_SHA_512.getSpecName());

        SecretKeySpec secretKey = new SecretKeySpec(secretKeyinBytes,
                JwaAlgorithms.HMAC_SHA_512.getJavaName());
        String payload = "the content that used to sign.";

        HmacJwsSigner signer = new HmacJwsSigner(secretKey);

        JwsDocument jws = JwsMaker.generateFromPayload(payload,
                protectedHeader, signer);
        assertEquals(jws.getCompactSerialisation(), JWS_TEST_CASE_7);

        jws = JwsMaker.generateFromJws(JWS_TEST_CASE_7_MANIPULATED);
        assertFalse(jws.verifySignatureWith(new NoneJwsVerifier()));
    }

    @Test(expected = SecurityException.class)
    public void JWSTestCase8() {
        List<String> crit = new ArrayList<String>();
        crit.add("exp");

        JwsProtectedHeader protectedHeader = new JwsProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.HMAC_SHA_512.getSpecName());

        JwsUnprotectedHeader unprotectedHeader = new JwsUnprotectedHeader();
        unprotectedHeader.setHeader(JoseHeaders.HEADER_CRITICAL, crit);
        unprotectedHeader.setHeader("exp", 1739760218135l);

        SecretKeySpec secretKey = new SecretKeySpec(secretKeyinBytes,
                JwaAlgorithms.HMAC_SHA_512.getJavaName());
        String payload = "{\"payload\" : \"the content that used to sign.\"}";

        HmacJwsSigner signer = new HmacJwsSigner(secretKey);
        @SuppressWarnings("unused")
        JwsDocument jws = JwsMaker.generateFromPayload(payload,
                protectedHeader, unprotectedHeader, signer);
    }

    public void JwsTestCase8Verifier() {

        JwsDocument jws = JwsMaker.generateFromJws(JWS_TEST_CASE_8);
        SecretKeySpec secretKey = new SecretKeySpec(secretKeyinBytes,
                JwaAlgorithms.HMAC_SHA_512.getJavaName());

        assertFalse(jws.verifySignatureWith(secretKey));

    }

  //  @Test(expected = IllegalArgumentException.class)
    public void JwsTestCase9(){
        JwsProtectedHeader protectedHeader = new JwsProtectedHeader();
        protectedHeader.setAlgorithm(JwaAlgorithms.HMAC_SHA_512.getSpecName());

        SecretKeySpec secretKey = new SecretKeySpec(secretKeyinBytes,
                JwaAlgorithms.HMAC_SHA_512.getJavaName());
        String payload = "the content that used to sign.";

        HmacJwsSigner signer = new HmacJwsSigner(secretKey);

        JwsDocument jws = JwsMaker.generateFromPayload(payload,
                protectedHeader, signer);
        assertEquals(jws.getCompactSerialisation(), JWS_TEST_CASE_9);

        jws = JwsMaker.generateFromJws(JWS_TEST_CASE_9_MANIPULATED);
        System.out.println(JWS_TEST_CASE_7_MANIPULATED);
        assertFalse(jws.verifySignatureWith(secretKey));
    }

}

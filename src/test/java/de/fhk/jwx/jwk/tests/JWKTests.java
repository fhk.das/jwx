/*
 * Copyright 2015 Peter Leo Gorski, Luigi Lo Iacono, Daniel Behnam Torkian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.fhk.jwx.jwk.tests;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import de.fhk.jwx.jwk.JwkKey;
import de.fhk.jwx.jwk.JwkKeySet;

public class JWKTests extends Assert {

    @Test
    public void testPublicSetAsList() {
        JwkKeySet jwks = new JwkKeySet(readKeySet("JwkAppendixB.txt"));
        List<JwkKey> keys = jwks.getKeys();
        assertEquals(1, keys.size());
        JwkKey rsa = keys.get(0);
        assertEquals(6, rsa.sizeOfProperties());
        assertEquals(1, rsa.getX509Chain().size());

    }

    private String readKeySet(String fileName) {
        InputStream is = this.getClass().getClassLoader()
                .getResourceAsStream(fileName);
        StringBuilder sb = new StringBuilder(1024);

        try {
            for (int i = is.read(); i != -1; i = is.read()) {
                sb.append((char) i);
            }

            is.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return sb.toString();
    }

}
